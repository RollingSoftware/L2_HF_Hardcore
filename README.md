# L2J Hardcore
L2J Hardcore edition - player drops, extra hard mobs, subclasses can go olympiad

## Quickstart

1. ./gradlew build
2. Extract contents of build zips from **l2j_gameserver/build/distributions**, **l2j_loginserver/build/distributions**, **l2j_datapack/build/distributions** into a single folder
3. Setup MariaDB and create two databases, one for gameserver, other one for loginserver, **l2jgs** and **l2jls** for example
4. In extracted root folder, find **tools** folder and use **dbinst_gs.jar** and **dbinst_ls.jar** to create database schemas
5. Update **login/config/database.yml** and **game/config/database.yml** to match your database credentials
6. Use game server registerting script in **login/** folder **RegisterGameServer(.sh|.exe)**
7. Copy hexid.txt to **game/config/server** folder
8. Copy geodata files to **game/data/geodata**, you can download it from [here](https://gitlab.com/RollingSoftware/l2_geo_hf)

All set, you can now start login server with **login/startLoginServer(.sh|.bat)** and after that start the game server **game/startGameServer(
.sh|.bat)**

Monitor your log files **log/stdout.log** for both login and game servers, when they will be ready, you will see message stating that they are
listening on a particular port

## Dev mode runners are available in test directories