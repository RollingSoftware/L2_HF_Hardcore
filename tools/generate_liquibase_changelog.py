import sys
import os

script, workdir = sys.argv

with open('changelog.yml', 'w') as f:
    f.write('databaseChangeLog:' + '\n')
    for root, dirs, files in os.walk(workdir):        
        for file in files:
            if file.endswith(".sql"):
                sql_file = os.path.join(root, file)
                print(sql_file)
                f.write('   - include:' + '\n')
                f.write('       file: ' + sql_file + '\n')
      