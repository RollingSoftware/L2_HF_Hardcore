plugins {
    `java-library`
}

version = "1.2.0"

java {
    sourceCompatibility = JavaVersion.VERSION_15
    targetCompatibility = JavaVersion.VERSION_15
}