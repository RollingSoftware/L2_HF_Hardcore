package com.l2jserver.mmocore;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;

public interface MMOConnectionInterface<T extends MMOClient<?>> {
    void setClient(T client);

    T getClient();

    void sendPacket(SendablePacket<T> sp);

    SelectionKey getSelectionKey();

    InetAddress getInetAddress();

    int getPort();

    void close() throws IOException;

    int read(ByteBuffer buf) throws IOException;

    int write(ByteBuffer buf) throws IOException;

    void createWriteBuffer(ByteBuffer buf);

    boolean hasPendingWriteBuffer();

    void movePendingWriteBufferTo(ByteBuffer dest);

    void setReadBuffer(ByteBuffer buf);

    ByteBuffer getReadBuffer();

    boolean isClosed();

    NioNetStackList<SendablePacket<T>> getSendQueue();

    @SuppressWarnings("unchecked")
    void close(SendablePacket<T> sp);

    void close(SendablePacket<T>[] closeList);

    void releaseBuffers();
}
