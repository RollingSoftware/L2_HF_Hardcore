package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.common.util.StringUtil;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.CommunityBuffList;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.statement.UnableToExecuteStatementException;

import java.sql.SQLIntegrityConstraintViolationException;

public class CreatePresetAction implements BoardAction {

    private static final Logger LOG = LogManager.getLogger();

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.isEmpty()) {
            LOG.warn("Invalid create preset request from {} with args {}", player, args);
            return ProcessResult.failure(player.string("invalid_preset_buff_request"));
        }
        String presetName = args.getArgs().get(0);
        if (StringUtil.isBlank(presetName) || StringUtil.hasWhitespaces(presetName)) {
            return ProcessResult.failure(player.string("preset_name_cannot_be_empty_or_contain_whitespaces"));
        }

        CommunityBuffList communityBuffList = new CommunityBuffList(player.getObjectId(), presetName);
        try {
            DAOFactory.getInstance().getCommunityBuffListDao().createCommunityBuffList(communityBuffList);
        } catch (UnableToExecuteStatementException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException) {
                return ProcessResult.failure(player.string("you_already_have_a_preset_named_n").replace("$n", presetName));
            } else {
                return ProcessResult.failure(player.string("error_occurred_could_not_retrieve_buff_preset"));
            }
        }
        CommunityBoardHandler.getInstance().handleParseCommand("_bbs_buff update_preset " + presetName, player);
        return ProcessResult.success();
    }

}
