package com.l2j.datapack.custom.bots.farmzones

import com.fasterxml.jackson.core.type.TypeReference
import com.l2j.datapack.helper.ScriptResourceResolver.Companion.resolve
import com.l2jserver.common.resources.YamlResource
import com.l2jserver.gameserver.model.actor.L2Character
import org.apache.logging.log4j.LogManager
import java.util.stream.Collectors

class FarmZonesTable {
    private val log = LogManager.getLogger()

    private val farmzones: List<FarmZone> = loadFarmZones()

    private fun loadFarmZones(): List<FarmZone> {
        val farmZonesFromFile =
            YamlResource.loadFile(
                resolve(javaClass, "./farmzones.yml"),
                object : TypeReference<List<FarmZone>>() {}
            )
        log.info("Loaded '{}' farm zones", farmZonesFromFile.size)
        return farmZonesFromFile
    }

    fun findClosestForLevel(character: L2Character): List<FarmZone> {
        return farmzones.stream().filter { (_, _, levels) -> levels.isWithin(character.level) }
            .collect(Collectors.toList())
    }

}