package com.l2j.datapack.custom.bots.equipment.types;

import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.WeaponType;

public class CrossbowType implements EquippableWeaponType {

    public static final CrossbowType INSTANCE = new CrossbowType();

    @Override
    public boolean typeOf(L2Weapon weapon) {
        return weapon.getItemType() == weaponType();
    }

    @Override
    public String name() {
        return "Crossbow";
    }

    @Override
    public WeaponType weaponType() {
        return WeaponType.CROSSBOW;
    }
}