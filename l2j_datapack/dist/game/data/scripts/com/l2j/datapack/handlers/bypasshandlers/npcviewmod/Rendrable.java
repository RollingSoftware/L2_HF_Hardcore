package com.l2j.datapack.handlers.bypasshandlers.npcviewmod;

public interface Rendrable {

    String render();

}
