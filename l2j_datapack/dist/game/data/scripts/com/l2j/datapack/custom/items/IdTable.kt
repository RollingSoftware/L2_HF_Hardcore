package com.l2j.datapack.custom.items

object IdTable {
    val manaDrugId = 726
    val orangeGreaterHealingPotionId = 1061
    val whiteGreaterHealingPotionId = 1539
}