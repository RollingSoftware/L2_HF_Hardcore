package com.l2j.datapack.custom.bots.manager

import com.l2jserver.gameserver.messaging.MessageContainer
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance

object BotCommands {
    @Volatile
    var botManager: BotManager? = null

    val events = MessageContainer<BotCommand>()

    init {
        events.register { botCommand ->
            when (botCommand) {
                is BotCommand.RestartManager -> botManager?.reload()
                is BotCommand.StartManager -> botManager?.load()
                is BotCommand.StopManager -> botManager?.unload()
                is BotCommand.SetAmount -> botManager?.setTargetBotCount(botCommand.targetAmount)
                is BotCommand.InsertBot -> botManager?.makeTargetABot(botCommand.player)
            }
        }
    }
}

sealed class BotCommand {
    object StopManager : BotCommand()
    object StartManager : BotCommand()
    object RestartManager : BotCommand()
    data class InsertBot(val player: L2PcInstance) : BotCommand()
    data class SetAmount(val targetAmount: Int) : BotCommand()
}