package com.l2j.datapack.custom.bots.states

import com.l2j.datapack.custom.bots.farmzones.FarmZone
import com.l2j.datapack.handlers.communityboard.custom.boards.teleport.BoardTeleportTable
import com.l2j.datapack.handlers.communityboard.custom.boards.teleport.TeleportBoard
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.util.Util

class CommunityTeleportToState(
    val player: L2PcInstance,
    val targetZone: FarmZone
) {
    @Volatile
    private var startedTeleporting = 0L

    fun teleportTo(): Boolean {
        val teleportInitiated = TeleportBoard.handleTeleport(player, targetZone.communityTeleport)
        if (teleportInitiated && (player.isCastingNow || player.isTeleporting)) {
            startedTeleporting = System.currentTimeMillis()
            return true
        }
        return false
    }

    fun isDone(): Boolean {
        val currentTime = System.currentTimeMillis()
        val delta = currentTime - startedTeleporting
        if (player.isTeleporting) {
            return false
        }

        if (startedTeleporting > 0 && delta > 5000) {
            // Timeout, assume teleport cancelled or succeeded
            return true
        }

        return BoardTeleportTable.getInstance().findByCode(targetZone.communityTeleport)
            .map {
                Util.calculateDistance(player.location, it.coordinates, true, false) < 500
            }
            .orElseThrow {
                IllegalArgumentException("This community teleport does not exist ${targetZone.communityTeleport}")
            }
    }
}