package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2j.datapack.handlers.communityboard.custom.BoardCondition;
import com.l2j.datapack.handlers.communityboard.custom.CommunityPaymentHelper;
import com.l2jserver.Config;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.target.TargetHolder;
import com.l2jserver.gameserver.model.zone.ZoneId;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BoardRestoreAction implements BoardAction {

    private final static Logger LOG = LogManager.getLogger();

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (!Config.COMMUNITY_RESTORE_ENABLED) {
            return ProcessResult.failure(player.string("restore_is_disabled"));
        }

        if (args.getArgs().size() != 1) {
            LOG.warn("{} is trying to use restore with invalid args number {}", player, args.getArgs().size());
            return ProcessResult.failure(player.string("invalid_restore_request"));
        }

        String targetString = args.getArgs().get(0);
        TargetHolder targetHolder = TargetHolder.parseTarget(player, targetString);

        ProcessResult checkResult = BoardCondition.checkCondition(player);
        if (checkResult.isFailure()) {
            return checkResult;
        }

        if (!player.isInsideZone(ZoneId.PEACE)) {
            return ProcessResult.failure(player.string("can_only_restore_inside_peace_zone"));
        }

        ProcessResult paymentResult = CommunityPaymentHelper.payForService(player, Config.COMMUNITY_RESTORE_PRICE);
        if (paymentResult.isFailure()) {
            return paymentResult;
        }

        int delay = GameTimeController.TICKS_PER_SECOND * GameTimeController.MILLIS_IN_TICK;
        SkillHolder greaterHeal = new SkillHolder(1217, 1);

        if (targetHolder.isSummonTarget()) {
            targetHolder.getMaster().fakeCast(greaterHeal, delay, () -> setVitalsToMax(targetHolder.getSummon()), targetHolder.getSummon());
        } else {
            targetHolder.getMaster().fakeCast(greaterHeal, delay, () -> setVitalsToMax(targetHolder.getMaster()), targetHolder.getMaster());
        }

        return ProcessResult.success();
    }

    private void setVitalsToMax(L2Character target) {
        target.setCurrentCp(target.getMaxRecoverableCp());
        target.setCurrentHp(target.getMaxRecoverableHp());
        target.setCurrentMp(target.getMaxRecoverableMp());
    }

}
