package com.l2j.datapack.custom.bots.equipment.types;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.WeaponType;

import java.util.Arrays;
import java.util.List;

public interface EquippableWeaponType {

    List<EquippableWeaponType> ALL_TYPES = Arrays.asList(
            AncientSwordType.INSTANCE,
            BluntOneHandedType.INSTANCE,
            BluntTwoHandedType.INSTANCE,
            BowType.INSTANCE,
            CrossbowType.INSTANCE,
            DaggerType.INSTANCE,
            DualDaggerType.INSTANCE,
            DualFistType.INSTANCE,
            DualType.INSTANCE,
            PoleType.INSTANCE,
            RapierType.INSTANCE,
            SwordOneHandedType.INSTANCE,
            SwordTwoHandedType.INSTANCE,
            EtcType.INSTANCE
    );

    @JsonCreator
    static EquippableWeaponType fromString(String raw) {
        return ALL_TYPES.stream()
                .filter(it -> it.name().equalsIgnoreCase(raw))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Illegal Weapon Type: " + raw));
    }

    boolean typeOf(L2Weapon weapon);

    String name();

    WeaponType weaponType();

}
