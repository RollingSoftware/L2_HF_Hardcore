package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.CommunityBuffList;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2j.datapack.handlers.communityboard.custom.CustomHomeBoard;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.BuffList;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.renderers.BuffRowRender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ShowAddToPresetBuffAction implements BoardAction {

    private final static Logger LOG = LogManager.getLogger();

    private final BuffList buffList;

    public ShowAddToPresetBuffAction(BuffList buffList) {
        this.buffList = buffList;
    }

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.isEmpty()) {
            LOG.warn("Could not show buff list because preset name is missing");
            return ProcessResult.failure(player.string("could_not_show_available_preset_buff_list"));
        }
        String presetName = args.getArgs().get(0);

        Optional<CommunityBuffList> communityBuffList = DAOFactory.getInstance().getCommunityBuffListDao().findSingleCommunityBuffSet(player.getObjectId(), presetName);
        if (communityBuffList.isEmpty()) {
            LOG.warn("Player {} tried to access list {} that does not belong to him", player, presetName);
            return ProcessResult.failure(player.string("could_not_show_available_preset_buff_list"));
        }

        Set<SkillHolder> currentPresetBuffs = new HashSet<>(communityBuffList.get().getSkills());

        List<Skill> buffSkills = buffList
                .getBuffs()
                .stream()
                .filter(skillHolder -> !currentPresetBuffs.contains(skillHolder))
                .map(SkillHolder::getSkill)
                .collect(Collectors.toList());

        String html = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), CustomHomeBoard.HTML_ROOT + "/buff/buff_add_list.html");

        html = html.replace("%preset_name%", presetName);

        String buffRows = BuffRowRender.render("bypass -h _bbs_buff add_preset_buff " + args.getActionName() + " " + presetName + " %buff_id%", buffSkills);
        html = html.replace("%buff_list%", buffRows);

        CommunityBoardHandler.separateAndSend(html, player);
        return ProcessResult.success();
    }

}
