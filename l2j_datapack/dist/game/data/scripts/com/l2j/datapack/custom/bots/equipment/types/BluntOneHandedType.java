package com.l2j.datapack.custom.bots.equipment.types;

import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.WeaponType;

public class BluntOneHandedType implements EquippableWeaponType {

    public static final BluntOneHandedType INSTANCE = new BluntOneHandedType();

    @Override
    public boolean typeOf(L2Weapon weapon) {
        return weapon.getItemType() == weaponType() && weapon.isOneHanded();
    }

    @Override
    public String name() {
        return "BluntOneHanded";
    }

    @Override
    public WeaponType weaponType() {
        return WeaponType.BLUNT;
    }
}
