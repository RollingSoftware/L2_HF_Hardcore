package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2j.datapack.handlers.communityboard.custom.CustomHomeBoard;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.BuffList;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.renderers.BuffRowRender;

import java.util.List;
import java.util.stream.Collectors;

public class ShowBuffListAction implements BoardAction {

    private final String buffListName;
    private final BuffList buffList;

    public ShowBuffListAction(String buffListName, BuffList buffList) {
        this.buffListName = buffListName;
        this.buffList = buffList;
    }

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        List<Skill> buffSkills = buffList
                .getBuffs()
                .stream()
                .map(SkillHolder::getSkill)
                .collect(Collectors.toList());

        String html = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), CustomHomeBoard.HTML_ROOT + "/buff/buff_list.html");

        String buffRows = BuffRowRender.render("bypass -h _bbs_buff buff " + buffListName + " %buff_id% $buff_target", buffSkills);
        html = html.replace("%buff_list%", buffRows);
        CommunityBoardHandler.separateAndSend(html, player);
        return ProcessResult.success();
    }

}
