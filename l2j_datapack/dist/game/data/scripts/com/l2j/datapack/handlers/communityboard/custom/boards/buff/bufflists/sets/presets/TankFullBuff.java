package com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.sets.presets;

import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.BuffFilter;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.BuffList;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TankFullBuff implements BuffList {

    public static final String NAME = "tank_full_buff";

    public final List<SkillHolder> buffs = Arrays.asList(
            new SkillHolder(1519, 1), // Chant of Blood Awakening
            new SkillHolder(1363, 1), // Chant of Victory
            new SkillHolder(1078, 6), // Concentration
            new SkillHolder(1035, 4), // Mental Shield
            new SkillHolder(1259, 4), // Resist Shock
            new SkillHolder(1503, 1), // Improved Shield Defense
            new SkillHolder(1504, 1), // Improved Movement
            new SkillHolder(1397, 3), // Clarity
            new SkillHolder(1354, 1), // Arcane Protection
            new SkillHolder(1085, 3), // Acumen
            new SkillHolder(1499, 1), // Improved Combat
            new SkillHolder(1501, 1), // Improved Condition
            new SkillHolder(1389, 3), // Greater Shield
            new SkillHolder(1542, 1), // Counter Critical
            new SkillHolder(1502, 1), // Improved Critical Attack
            new SkillHolder(1500, 1), // Improved Magic

            new SkillHolder(310, 1), // Dance of the Vampire
            new SkillHolder(275, 1), // Dance of Fury
            new SkillHolder(274, 1), // Dance of Fire
            new SkillHolder(271, 1), // Dance of the Warrior
            new SkillHolder(272, 1), // Dance of Inspiration
            new SkillHolder(304, 1), // Song of Vitality
            new SkillHolder(268, 1), // Song of Wind
            new SkillHolder(269, 1), // Song of Hunter
            new SkillHolder(267, 1), // Song of Warding
            new SkillHolder(264, 1), // Song of Earth
            new SkillHolder(349, 1), // Song of Renewal
            new SkillHolder(364, 1) // Song of Champion
    );

    @Override
    public List<SkillHolder> getBuffs() {
        return buffs;
    }

    @Override
    public Optional<SkillHolder> findBySkillId(int skillId) {
        return BuffFilter.findBySkillId(buffs, skillId);
    }

}
