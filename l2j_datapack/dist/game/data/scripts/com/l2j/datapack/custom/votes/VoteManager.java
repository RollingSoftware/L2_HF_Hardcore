package com.l2j.datapack.custom.votes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l2j.datapack.ai.SpawnManager;
import com.l2j.datapack.ai.npc.AbstractNpcAI;
import com.l2j.datapack.custom.votes.config.VoteConfig;
import com.l2jserver.common.mappers.Yaml;
import com.l2jserver.common.monad.ProcessResultCarrier;
import com.l2jserver.common.npc.ShortNpc;
import com.l2jserver.common.resources.YamlResource;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.xml.impl.MultisellData;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.NpcLocation;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.templates.L2NpcTemplate;
import com.l2jserver.gameserver.model.entity.Message;
import com.l2jserver.gameserver.model.holders.ItemHolder;
import com.l2jserver.gameserver.model.multisell.ListContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VoteManager extends AbstractNpcAI {

    private static final Logger LOG = LogManager.getLogger();
    private final SpawnManager spawnManager = new SpawnManager();
    private final VoteRepository voteRepository = new VoteRepository();
    private final Map<Integer, Future<?>> rewardClaimTasks = new ConcurrentHashMap<>();
    private Map<String, ListContainer> multisells = new ConcurrentHashMap<>();
    private VoteConfig config;
    private L2NpcTemplate voteManagerTemplate;
    private VoteChecker voteChecker;

    private VoteManager() {
        super(VoteManager.class.getSimpleName(), "Vote Manager");
        load();
    }

    public void load() {
        LOG.info("Loading Vote Manager ...");
        if (voteRepository.createTableIfNotExist()) {
            LOG.info("Created vote history table");
        }
        var result = loadConfig();
        if (result.isEmpty()) {
            return;
        }
        loadVoteChecker();
        loadNpc();
        loadRewardMultisells();
        spawnNpcs();
    }

    private void loadVoteChecker() {
        voteChecker = new VoteChecker(voteRepository, config.getSources());
        LOG.info("Vote Checker loaded");
    }

    private void loadNpc() {
        ShortNpc npc = config.getNpc();
        voteManagerTemplate = new L2NpcTemplate(npc.getId(), npc.getDisplayId(), npc.getName(), npc.getTitle(), npc.getCollisionRadius(), npc.getCollisionHeight());
        voteManagerTemplate.setUsingServerSideName(true);
        voteManagerTemplate.setUsingServerSideTitle(true);
        NpcData.getInstance().addNpc(voteManagerTemplate);
        addFirstTalkId(npc.getId());
        addTalkId(npc.getId());
        addStartNpc(npc.getId());
        LOG.info("Loaded NPC {}", npc);
    }

    private Optional<VoteConfig> loadConfig() {
        String voteConfigFilePath = "./config/vote_config.yml";
        if (!new File(voteConfigFilePath).exists()) {
            LOG.info("VoteManager will not be loaded, you need to provide vote_config.yml first");
            return Optional.empty();
        }
        config = YamlResource.loadFromClassPathFile(getClass(), voteConfigFilePath, VoteConfig.class);
        LOG.info("Config loaded");
        return Optional.of(config);
    }

    private void spawnNpcs() {
        String spawnlistFilename = "./config/vote_manager_spawnlist.yml";
        List<Location> spawnlist;
        try {
            spawnlist = Yaml.INSTANCE.getYaml().readValue(getClass().getResourceAsStream(spawnlistFilename), new TypeReference<>() {
            });
        } catch (IOException e) {
            throw new IllegalStateException("Could not load spawnlist from " + spawnlistFilename, e);
        }
        LOG.info("Loaded {} spawns", spawnlist.size());

        spawnlist.stream()
                .map(spawn -> new NpcLocation(config.getNpc().getId(), spawn))
                .map(spawn -> addSpawn(spawn.getNpcId(), spawn.getLocation()))
                .forEach(spawnManager::registerSpawn);
        LOG.info("Spawned {} npcs", spawnManager.size());
    }

    private void loadRewardMultisells() {
        String rewardFolderPath = "./rewards/";
        try (Stream<Path> files = Files.list(Path.of(getClass().getResource(rewardFolderPath).toURI()))) {
            files.filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            ListContainer multisell = Yaml.INSTANCE.getYaml().readValue(file.toFile(), new TypeReference<>() {
                            });
                            multisell.allowNpc(config.getNpc().getId());
                            multisells.put(file.toFile().getName().replace(".yml", ""), multisell);
                        } catch (IOException e) {
                            throw new IllegalStateException("Could not read multisell file " + file.getFileName().toString(), e);
                        }
                    });
        } catch (IOException | URISyntaxException e) {
            throw new IllegalStateException("Could not read reward files from " + rewardFolderPath, e);
        }

        LOG.info("Loaded {} reward multisells", multisells.keySet().size());
    }

    @Override
    public boolean unload() {
        LOG.info("Unloading script");
        despawnNpcs();
        unloadRewardMultisells();
        NpcData.getInstance().removeNpc(voteManagerTemplate);
        voteManagerTemplate = null;
        voteChecker = null;
        config = null;
        return super.unload();
    }

    private void despawnNpcs() {
        spawnManager.deleteAll();
    }

    private void unloadRewardMultisells() {
        multisells = new ConcurrentHashMap<>();
    }

    @Override
    public String onFirstTalk(L2Npc npc, L2PcInstance player) {
        return onTalk(npc, player);
    }

    @Override
    public String onTalk(L2Npc npc, L2PcInstance player) {
        return HtmCache.getInstance().getHtm(player.getHtmlPrefix(), "data/scripts/com/l2j/datapack/custom/votes/html/vote_manager.html");
    }

    private List<ProcessResultCarrier<List<ItemHolder>>> rewards(L2PcInstance player, List<VoteEntry> votes) {
        List<ProcessResultCarrier<List<ItemHolder>>> results = new ArrayList<>();
        for (VoteEntry entry : votes) {
            try {
                voteRepository.saveVoteHistory(player.getObjectId(), entry);
                results.add(ProcessResultCarrier.success(config.getRewards(), player.string("reward_for_vote_from_n").replace("$n", entry.getSourceCode())));
            } catch (RuntimeException e) {
                LOG.error("Failed to process reward {}", entry, e);
                results.add(ProcessResultCarrier.failure(player.string("reward_failed_to_process_n").replace("$n", entry.getSourceCode())));
            }
        }
        return results;
    }

    private void claimRewards(L2PcInstance player) {
        List<VoteEntry> votes = voteChecker.checkNewVotes(player);
        if (votes.isEmpty()) {
            player.sendScreenMessage(player.string("no_new_votes"));
            return;
        }

        List<ProcessResultCarrier<List<ItemHolder>>> results = rewards(player, votes);

        String content = results.stream().map(ProcessResultCarrier::getMessage).collect(Collectors.joining("\n"));
        Message rewardMessage = new Message(player.getObjectId(),
                player.string("vote_rewards"),
                content,
                Message.SendBySystem.NONE);

        List<ProcessResultCarrier<List<ItemHolder>>> successfulResults = results.stream().filter(ProcessResultCarrier::isSuccess).collect(Collectors.toList());

        if (!successfulResults.isEmpty()) {
            rewardMessage.createAttachments();
            for (ProcessResultCarrier<List<ItemHolder>> items : successfulResults) {
                items.getResult().forEach(reward -> Objects.requireNonNull(rewardMessage.getAttachments()).addItem("Vote Reward", reward.getId(), reward.getCount(), null, true));
            }
        }

        MailManager.getInstance().sendMessage(rewardMessage);
    }

    @Override
    public String onAdvEvent(String event, L2Npc npc, L2PcInstance player) {
        if (event.equals("claim_rewards")) {
            Future<?> rewardClaimTask = rewardClaimTasks.get(player.getObjectId());
            if (rewardClaimTask != null && !rewardClaimTask.isDone()) {
                player.sendScreenMessage(player.string("vote_reward_claim_in_progress"));
            } else {
                rewardClaimTasks.put(player.getObjectId(), ThreadPoolManager.getInstance().scheduleGeneral(() -> claimRewards(player), 10L));
                player.sendScreenMessage(player.string("vote_reward_claim_task_started"));
            }
            return null;
        } else if (event.contains("exchange")) {
            MultisellData.getInstance().separateAndSend(multisells.get(event), player, npc);
        }
        return super.onAdvEvent(event, npc, player);
    }
}
