package com.l2j.datapack.gameplay.deathrecap;

import com.l2j.datapack.helper.ScriptResourceResolver;
import com.l2jserver.common.file.FileHelper;
import com.l2jserver.common.renderer.IconRender;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;
import java.util.stream.Collectors;

public class AttackerDamageRecapRender {

    private String deathRecapHtml;
    private String attackDamageRecapHtml;
    private String attackDamageRowHtml;

    private NumberFormat numberFormat;

    public AttackerDamageRecapRender() {
        numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(0);
        numberFormat.setRoundingMode(RoundingMode.HALF_UP);

        deathRecapHtml = FileHelper.loadFileContents(
                AttackerDamageRecap.class,
                ScriptResourceResolver.resolve(AttackerDamageRecap.class, "./html/death_recap.html")
        );
        attackDamageRecapHtml = FileHelper.loadFileContents(
                AttackerDamageRecap.class,
                ScriptResourceResolver.resolve(AttackerDamageRecap.class, "./html/attack_damage_recap.html")
        );
        attackDamageRowHtml = FileHelper.loadFileContents(
                AttackerDamageRecap.class,
                ScriptResourceResolver.resolve(AttackerDamageRecap.class, "./html/attack_damage_row.html")
        );
    }

    public String render(List<AttackerDamageRecap> attackerDamageRecap) {
        var content = attackerDamageRecap.stream()
                .map(this::renderAttacker)
                .collect(Collectors.joining("</br>\n"));
        return deathRecapHtml.replace("{{DEATH_RECAP}}", content);
    }

    public String renderAttacker(AttackerDamageRecap attackerDamageRecap) {
        var attackerDamageContainer = attackDamageRecapHtml
                .replace("{{ATTACKER_NAME}}", attackerDamageRecap.getAttacker().getName());

        var attackerDamageRows = attackerDamageRecap.getDamageBySkill().entrySet().stream()
                .limit(25)
                .map(
                        (entry) -> {
                            var icon = IconRender.render(entry.getKey().getIcon());
                            return attackDamageRowHtml
                                    .replace("{{ATTACK_NAME}}", entry.getKey().getName())
                                    .replace("{{ATTACK_ICON}}", icon)
                                    .replace("{{ATTACK_TOTAL_DAMAGE}}", numberFormat.format(entry.getValue()));
                        }).collect(Collectors.joining("\n"));
        return attackerDamageContainer.replace("{{DAMAGE_ROWS}}", attackerDamageRows);
    }

}
