package com.l2j.datapack.custom.bots.states

import com.l2jserver.gameserver.model.actor.instance.L2ClassMasterInstance
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.base.ClassId
import com.l2jserver.gameserver.model.base.levelRequirement
import com.l2jserver.gameserver.model.base.nextClassId
import org.apache.logging.log4j.LogManager

object ClassChangingHelper {
    private val logger = LogManager.getLogger()

    fun checkAndChange(desiredNextClassIds: List<Int>, player: L2PcInstance) {
        if (desiredNextClassIds.isEmpty()) {
            logger.debug("Player {} has no desired class ids", player)
            return
        }

        val desiredClass = desiredNextClassIds.random()
        if (desiredClass > 0) {
            val nextClassId = player.classId.nextClassId(ClassId.getClassId(desiredClass))
            if (nextClassId != player.classId && nextClassId.levelRequirement() <= player.level) {
                L2ClassMasterInstance.checkAndChangeClass(player, nextClassId.id)
            }
        }
    }
}