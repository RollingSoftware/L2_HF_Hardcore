package com.l2j.datapack.custom.bots.states

import com.l2j.datapack.custom.bots.behaviour.ClassBehaviour
import com.l2j.datapack.handlers.communityboard.custom.CommunityPaymentHelper
import com.l2jserver.Config
import com.l2jserver.gameserver.handler.CommunityBoardHandler
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance

class BuffHelper(val player: L2PcInstance, val classBehaviour: ClassBehaviour) {

    private fun needToBuffAndCanPay(): Boolean {
        return (isBuffFree() || player.adena > Config.COMMUNITY_DEFAULT_PRESET_PRICE)
                && !player.effectList.hasBuffs()
                && !player.isInCombat
    }

    fun needsBuff() = !player.effectList.hasBuffs()

    private fun isBuffFree(): Boolean = CommunityPaymentHelper.checkFreeService(player)

    fun checkAndBuff(): Boolean {
        if (needToBuffAndCanPay()) {
            val bypass = "_bbs_buff " + classBehaviour.buffPreference + " PLAYER"
            CommunityBoardHandler.getInstance().handleParseCommand(bypass, player)
            return true
        }
        return false
    }

}