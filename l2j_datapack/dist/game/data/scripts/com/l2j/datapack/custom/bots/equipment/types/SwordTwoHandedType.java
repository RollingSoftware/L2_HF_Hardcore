package com.l2j.datapack.custom.bots.equipment.types;

import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.WeaponType;

public class SwordTwoHandedType implements EquippableWeaponType {

    public static final SwordTwoHandedType INSTANCE = new SwordTwoHandedType();

    @Override
    public boolean typeOf(L2Weapon weapon) {
        return weapon.getItemType() == weaponType() && weapon.isTwoHanded();
    }

    @Override
    public String name() {
        return "SwordTwoHanded";
    }

    @Override
    public WeaponType weaponType() {
        return WeaponType.SWORD;
    }
}