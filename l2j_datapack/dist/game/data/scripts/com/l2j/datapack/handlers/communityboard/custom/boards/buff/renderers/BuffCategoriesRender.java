package com.l2j.datapack.handlers.communityboard.custom.boards.buff.renderers;

import com.l2j.datapack.handlers.communityboard.custom.CustomHomeBoard;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

public class BuffCategoriesRender {

    public static String renderBuffCategoriesList(String bypassActionName, L2PcInstance player) {
        return renderBuffCategoriesList(bypassActionName, "", player);
    }

    public static String renderBuffCategoriesList(String bypassActionName, String arguments, L2PcInstance player) {
        return HtmCache.getInstance()
                .getHtm(player.getHtmlPrefix(), CustomHomeBoard.HTML_ROOT + "/buff/buff_categories.html")
                .replace("%bypass_action%", bypassActionName)
                .replace("%arguments%", arguments);
    }

}
