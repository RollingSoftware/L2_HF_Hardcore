package com.l2j.datapack.custom.metrics

import com.l2j.datapack.ai.npc.AbstractNpcAI
import com.l2jserver.common.monitoring.MetricsManager
import com.l2jserver.gameserver.model.events.Containers
import com.l2jserver.gameserver.model.events.EventType
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogin
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogout
import com.l2jserver.gameserver.model.events.listeners.AbstractEventListener
import com.l2jserver.gameserver.model.events.listeners.ConsumerEventListener
import io.micrometer.core.instrument.Counter
import java.util.concurrent.atomic.AtomicInteger
import java.util.function.Consumer

class OnlineMetrics : AbstractNpcAI("OnlineMetrics", "Online Metrics") {
    private val onlineGauge: AtomicInteger = MetricsManager.instance.metricsRegistry.gauge("l2j_gs_current_online", AtomicInteger())!!
    private val onlinePlayersGauge: AtomicInteger = MetricsManager.instance.metricsRegistry.gauge("l2j_gs_current_online_players", AtomicInteger())!!
    private val loginCounter: Counter = MetricsManager.instance.metricsRegistry.counter("l2j_gs_enter_world", "domain", "login_stats")
    private val logoutCounter: Counter = MetricsManager.instance.metricsRegistry.counter("l2j_gs_exit_world", "domain", "login_stats")

    lateinit var loginListener: AbstractEventListener
    lateinit var logoutListener: AbstractEventListener

    init {
        load()
    }

    fun load() {
        loginListener = Containers.Players().addListener(
            ConsumerEventListener(
                Containers.Players(),
                EventType.ON_PLAYER_LOGIN,
                Consumer { onPlayerLogin: OnPlayerLogin ->
                    onlineGauge.incrementAndGet()
                    loginCounter.increment()
                    if (!onPlayerLogin.activeChar.isBot) {
                        onlinePlayersGauge.incrementAndGet()
                    }
                },
                this
            )
        )

        logoutListener = Containers.Players().addListener(
            ConsumerEventListener(
                Containers.Players(),
                EventType.ON_PLAYER_LOGOUT,
                Consumer { onPlayerLogout: OnPlayerLogout ->
                    onlineGauge.decrementAndGet()
                    logoutCounter.increment()
                    if (!onPlayerLogout.activeChar.isBot) {
                        onlinePlayersGauge.decrementAndGet()
                    }
                },
                this
            )
        )
    }

    fun reload() {
        unload()
        load()
    }

    override fun unload(): Boolean {
        Containers.Players().removeListener(loginListener)
        Containers.Players().removeListener(logoutListener)
        return super.unload()
    }
}