package com.l2j.datapack.ai.individual.Frintezza;

import com.l2j.datapack.ai.Status;

import java.util.Arrays;
import java.util.List;

public enum FrintezzaStatuses implements Status {

    ALIVE, FIRST_ROOM, SECOND_ROOM, REGULAR_HALISHA, FOUR_LEG_HALISHA, FINISH, DEAD;

    public static List<Status> asList() {
        return Arrays.asList(values());
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public String getName() {
        return name();
    }

}
