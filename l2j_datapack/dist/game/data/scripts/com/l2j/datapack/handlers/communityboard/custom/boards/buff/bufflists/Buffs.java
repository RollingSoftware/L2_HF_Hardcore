package com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists;

import com.l2jserver.gameserver.model.holders.SkillHolder;

import java.util.List;

public interface Buffs {

    List<SkillHolder> getBuffs();
    
}
