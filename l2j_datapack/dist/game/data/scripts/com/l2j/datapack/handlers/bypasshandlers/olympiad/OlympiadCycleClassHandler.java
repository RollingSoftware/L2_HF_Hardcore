package com.l2j.datapack.handlers.bypasshandlers.olympiad;

import com.l2jserver.gameserver.handler.IBypassHandler;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.olympiad.Olympiad;
import com.l2jserver.gameserver.model.olympiad.subclass.OlympiadCycleClassManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.StringTokenizer;

public class OlympiadCycleClassHandler implements IBypassHandler {

    private static final String[] COMMANDS = {
            "olympiad_select_cycle_class"
    };
    private final Logger LOG = LogManager.getLogger();

    @Override
    public boolean useBypass(String command, L2PcInstance activeChar, L2Character bypassOrigin) {
        StringTokenizer st = new StringTokenizer(command, " ");
        // Skip command name
        st.nextToken();

        if (st.countTokens() != 1) {
            LOG.warn(
                    "Active={}, origin={} called command {} with incorrect arguments",
                    activeChar, bypassOrigin, command
            );
            return false;
        }

        var classIndex = Integer.parseInt(st.nextToken());
        var classId = activeChar.getClassIdByIndex(classIndex);

        var result = OlympiadCycleClassManager.with().registerCycleClass(
                activeChar,
                Olympiad.getInstance().getCurrentCycle(),
                classId
        );
        // bypassOrigin.onAction(activeChar);
        return result;
    }

    @Override
    public String[] getBypassList() {
        return COMMANDS;
    }
}
