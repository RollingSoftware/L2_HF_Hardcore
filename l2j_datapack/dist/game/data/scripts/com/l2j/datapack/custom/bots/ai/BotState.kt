package com.l2j.datapack.custom.bots.ai

enum class BotState {
    IDLE,
    SEARCHING_FOR_TARGET,
    ATTACKING,
    CASTING,
    MANAGING,
    BUFFING,
    TELEPORTING,
    THINKING,
    RESTING,
    RESPONDING_TO_ATTACKERS,
    MOVING_TO_TARGET,
    NO_MONEY,
    STUCK,
    ROAMING
}