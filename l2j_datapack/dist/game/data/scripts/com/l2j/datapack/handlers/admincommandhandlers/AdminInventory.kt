package com.l2j.datapack.handlers.admincommandhandlers

import com.l2j.datapack.custom.bots.equipment.EquipmentManager
import com.l2j.datapack.custom.bots.equipment.EquipmentTable
import com.l2j.datapack.handlers.communityboard.custom.ActionArgs
import com.l2jserver.gameserver.handler.IAdminCommandHandler
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.network.serverpackets.ItemList
import org.apache.logging.log4j.LogManager

class AdminInventory : IAdminCommandHandler {
    private val log = LogManager.getLogger()
    val equipmentTable: EquipmentTable = EquipmentTable()

    override fun useAdminCommand(command: String, activeChar: L2PcInstance): Boolean {
        if (!command.startsWith("admin_inventory")) {
            return false
        }
        return ActionArgs.parse(command)
            .map { action ->
                when (action.actionName) {
                    "clear" -> {
                        activeChar.inventory.destroyAllItems("gm-cleanup", activeChar, null)
                        true
                    }
                    "equip" -> {
                        if (action.args.size != 1) {
                            false
                        } else {
                            val equipmentPreferences = when (action.args.first()) {
                                "mage" -> equipmentTable.findPreference("mage")
                                "kamael" -> equipmentTable.findPreference("kamael")
                                else -> equipmentTable.findPreference("fighter")
                            }
                            EquipmentManager(activeChar, equipmentPreferences).manageEquipment()
                            activeChar.sendPacket(ItemList(activeChar, false))
                            true
                        }
                    }
                    else -> false
                }
            }.orElseGet {
                log.warn("Player {} used '{}' with wrong arguments", activeChar, command)
                false
            }
    }

    override fun getAdminCommandList(): Array<String> = arrayOf(
        "admin_inventory"
    )
}