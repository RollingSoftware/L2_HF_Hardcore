package com.l2j.datapack.custom.clan.territory

import com.l2jserver.common.noarg.NoArg

@NoArg
data class TerritoryOwningRewardData(
    val castles: List<TerritoryOwningReward>,
    val fortresses: List<TerritoryOwningReward>
)