package com.l2j.datapack.custom.bots.states

import com.l2j.datapack.custom.bots.decisiontree.Leaf
import com.l2j.datapack.custom.bots.equipment.EquipmentManager
import com.l2j.datapack.custom.bots.shopping.ShopBoardHelper
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance

class ShoppingSequence(val player: L2PcInstance, val equipmentManager: EquipmentManager) {

    private val leafs = listOf(
        Leaf({ ShopBoardHelper.sellUnused(player) }),
        Leaf({ ShopBoardHelper.buyBetterWeapon(player, equipmentManager)?.let { player.inventory.equipItem(it) } }),
        Leaf({
            if (ShopBoardHelper.hasSpareMoney(player)) {
                ShopBoardHelper.replenishPotions(player)
            }
        }),
        Leaf({
            val preferredSet = equipmentManager.findNonEquippedPreferredSets().firstOrNull()
            if (preferredSet != null) {
                ShopBoardHelper.buySetIfEnoughAdena(player, preferredSet.armorSet.id)
            }
        })
    )

    private var lastActionTime = 0L
    private val actionDelay = 100L

    fun useShop(): Boolean {
        val actionDelta = System.currentTimeMillis() - lastActionTime
        if (actionDelta > actionDelay) {
            lastActionTime = System.currentTimeMillis()
            val incompleteActions = leafs.filter { !it.complete }
            if (incompleteActions.isEmpty()) {
                return true
            }
            val leaf = incompleteActions.first()
            leaf.invoke()
            return !leafs.any { !it.complete }
        }
        return false
    }

    fun reset() {
        leafs.forEach { it.complete = false }
    }

}