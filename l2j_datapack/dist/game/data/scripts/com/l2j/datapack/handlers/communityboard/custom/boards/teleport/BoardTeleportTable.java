package com.l2j.datapack.handlers.communityboard.custom.boards.teleport;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l2j.datapack.helper.ScriptResourceResolver;
import com.l2jserver.common.resources.YamlResource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BoardTeleportTable {
    private Map<String, TeleportDestination> teleportsByCode = new HashMap<>();

    public BoardTeleportTable() {
        load();
    }

    public static BoardTeleportTable getInstance() {
        return BoardTeleportTable.SingletonHolder.INSTANCE;
    }

    public void load() {
        String file = "board_teleports.yml";
        List<TeleportDestination> teleportDestinations = YamlResource.loadFile(
                ScriptResourceResolver.resolve(getClass(), file), new TypeReference<>() {
                });

        teleportsByCode = teleportDestinations.stream()
                .collect(Collectors.toMap(TeleportDestination::getCode, Function.identity()));
    }

    public Optional<TeleportDestination> findByCode(String teleportDestinationCode) {
        return Optional.ofNullable(teleportsByCode.get(teleportDestinationCode));
    }

    private static class SingletonHolder {
        protected static final BoardTeleportTable INSTANCE = new BoardTeleportTable();
    }
}
