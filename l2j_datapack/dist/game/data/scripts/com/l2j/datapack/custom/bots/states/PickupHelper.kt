package com.l2j.datapack.custom.bots.states

import com.l2j.datapack.custom.bots.ai.BotAi
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance
import java.util.Optional

object PickupHelper {

    fun pickup(player: L2PcInstance, ai: BotAi): Boolean {
        val pickupObject: Optional<L2ItemInstance> = player.knownList
            .getKnownItemsInRadius(1200)
            .stream()
            .filter { obj: L2ItemInstance -> obj.isItem }
            .min(Comparator.comparing {
                player.calculateDistance(
                    it,
                    false,
                    false
                )
            })
        if (pickupObject.isPresent) {
            val target = pickupObject.get()
            if (ai.maybeMoveToPawn(target, 36)) {
                return true
            }
            player.doPickupItem(target)
            return true
        }
        return false
    }

}