package com.l2j.datapack.custom.bots.states

import com.l2jserver.gameserver.model.Location
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance

class WalkAroundState(val player: L2PcInstance) {

    private var lastWalkAroundTime = 0L
    private val walkAroundDelayRange = (1000..3000)

    private val arrivedLimit = 50

    fun walkAround(offset: Int = 300): Boolean {
        val walkAroundDelta = System.currentTimeMillis() - lastWalkAroundTime
        if (walkAroundDelta > walkAroundDelayRange.random()) {
            lastWalkAroundTime = System.currentTimeMillis()
            player.ai.maybeMoveToPosition(Location.closeTo(player.location, offset), arrivedLimit)
            return true
        }
        return false
    }

}