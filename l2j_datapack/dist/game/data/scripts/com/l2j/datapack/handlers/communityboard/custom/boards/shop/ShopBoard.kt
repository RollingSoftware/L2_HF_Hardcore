package com.l2j.datapack.handlers.communityboard.custom.boards.shop

import com.l2jserver.common.DecimalFormatStandard
import com.l2jserver.gameserver.cache.HtmCache
import com.l2jserver.gameserver.data.xml.impl.BuyListData
import com.l2jserver.gameserver.datatables.MerchantPriceConfigTable
import com.l2jserver.gameserver.handler.CommunityBoardHandler
import com.l2jserver.gameserver.handler.IParseBoardHandler
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.network.serverpackets.ActionFailed
import com.l2jserver.gameserver.network.serverpackets.BuyList
import com.l2jserver.gameserver.network.serverpackets.ExBuySellList
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage
import com.l2jserver.gameserver.transfer.bulk.BulkItemType
import com.l2jserver.gameserver.transfer.bulk.shop.BulkSellService
import com.l2j.datapack.handlers.communityboard.custom.ActionArgs
import com.l2j.datapack.handlers.communityboard.custom.BoardCondition
import com.l2j.datapack.handlers.communityboard.custom.CustomHomeBoard
import com.l2j.datapack.handlers.communityboard.custom.NotificationHelper
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class ShopBoard : IParseBoardHandler {

    val log: Logger = LogManager.getLogger()

    override fun parseCommunityBoardCommand(command: String, player: L2PcInstance): Boolean {
        if (!command.startsWith("_bbs_shop_board")) {
            return false
        }

        val result = BoardCondition.checkCondition(player)
        if (result.isFailure) {
            NotificationHelper.notify(player, result.result)
            return false
        }

        if (player.karma > 0) {
            NotificationHelper.notify(player, player.string("karma_players_cannot_use_the_shop"))
            return false
        }

        return ActionArgs.parse(command).map {
            when (it.actionName) {
                "buy" -> showBuyWindow(player, it.args.first().toInt())
                "bulk_sell" -> bulkSell(player, it)
                else -> false
            }
        }.orElseGet {
            val html =
                HtmCache.getInstance().getHtm(player.htmlPrefix, CustomHomeBoard.HTML_ROOT + "/shop/shop.html")

            val menuTemplate = HtmCache.getInstance().getHtm(
                player.htmlPrefix,
                CustomHomeBoard.HTML_ROOT + "/menu_template.html"
            )
            CommunityBoardHandler.separateAndSend(menuTemplate.replace("%page_content%", html), player)
            true
        }
    }

    override fun getCommunityBoardCommands(): Array<String> = arrayOf(
        "_bbs_shop_board"
    )

    private fun showBuyWindow(player: L2PcInstance, buyListId: Int): Boolean {
        if (buyListId !in knownBuyLists) {
            log.warn("Player {} requested a buyListId {} that is not specified in Shop Board", player, buyListId)
            player.sendPacket(ActionFailed.STATIC_PACKET)
            return false
        }

        val buyList = BuyListData.getInstance().getBuyList(buyListId)
        if (buyList == null) {
            log.warn("Trying to use incorrect buy list for board player={}, buyListId={}", player, buyListId)
            player.sendPacket(ActionFailed.STATIC_PACKET)
            return false
        }

        val regionalPriceConfig = MerchantPriceConfigTable.getInstance().getMerchantPriceConfig(player)

        val taxRate: Double = regionalPriceConfig.totalTaxRate
        player.setInventoryBlockingStatus(true)
        if (player.isGM) {
            player.sendMessage("Buy List [" + buyList.listId + "]")
        }
        player.sendPacket(BuyList(buyList, player.adena, taxRate))
        player.sendPacket(ExBuySellList(player, false))
        player.sendPacket(ActionFailed.STATIC_PACKET)
        return true
    }

    private fun bulkSell(player: L2PcInstance, actionArgs: ActionArgs): Boolean {
        if (actionArgs.actionName == "bulk_sell" && actionArgs.args.isEmpty()) {
            val htmlText =
                HtmCache.getInstance().getHtm(
                    player.htmlPrefix,
                    CustomHomeBoard.HTML_ROOT + "/shop/bulksell/shop_bulk_sell_options.html"
                )
            player.sendPacket(NpcHtmlMessage(htmlText))
            return true
        } else if (actionArgs.actionName.startsWith("bulk_sell") && actionArgs.args.size == 1) {
            val type: String = actionArgs.args.first()
            val itemType = BulkItemType.of(type)
            val price = BulkSellService.getInstance().sellAllByTypePrice(itemType, player)
            if (price <= 0L) {
                val htmlText = HtmCache.getInstance()
                    .getHtm(
                        player.htmlPrefix,
                        CustomHomeBoard.HTML_ROOT + "/shop/bulksell/shop_bulk_nothing_to_sell.html"
                    )
                player.sendPacket(NpcHtmlMessage(htmlText))
                return true
            }
            var htmlText =
                HtmCache.getInstance()
                    .getHtm(player.htmlPrefix, CustomHomeBoard.HTML_ROOT + "/shop/bulksell/shop_bulk_sell.html")
            htmlText = htmlText.replace("%item_group_name%".toRegex(), type)
            htmlText = htmlText.replace(
                "%item_total_count%".toRegex(),
                BulkSellService.getInstance().sellAllByTypeCount(itemType, player).toString()
            )
            htmlText = htmlText.replace("%item_total_sum%".toRegex(), DecimalFormatStandard.moneyFormat().format(price))
            htmlText = htmlText.replace("%item_group_type%".toRegex(), type)
            player.sendPacket(NpcHtmlMessage(htmlText))
            return true
        } else if (actionArgs.actionName.startsWith("bulk_sell") && actionArgs.args.size == 2) {
            val type: String = actionArgs.args.first()
            val itemType = BulkItemType.of(type)
            val price = BulkSellService.getInstance().sellAllByTypePrice(itemType, player)
            if (price <= 0L) {
                val htmlText = HtmCache.getInstance()
                    .getHtm(
                        player.htmlPrefix,
                        CustomHomeBoard.HTML_ROOT + "/shop/bulksell/shop_bulk_nothing_to_sell.html"
                    )
                player.sendPacket(NpcHtmlMessage(htmlText))
                return true
            }
            BulkSellService.getInstance().sellAllByTypeCommunity(BulkItemType.of(type), player)
            return true
        }

        player.sendMessage(player.string("bulk_sell_failed"))
        log.warn("Player {} is trying to use bulk sell with incorrect arguments: {}", player, actionArgs)
        return false
    }

    companion object {
        val knownBuyLists = listOf(
            4100001,
            4100002,
            4100003,
            4100011,
            4100012,
            4100013,
            4100021,
            4100022,
            4100023,
            4100031,
            4100032,
            4100033,
            4100041,
            4100042,
            4100043,
            4100051,
            4100052,
            4100053,
            4100100,
            4100101,
            4100102,
            4100103,
            4100104,
            4000000,
            4100105
        )
    }
}