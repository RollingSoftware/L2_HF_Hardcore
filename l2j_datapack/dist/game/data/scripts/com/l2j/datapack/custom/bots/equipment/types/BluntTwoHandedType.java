package com.l2j.datapack.custom.bots.equipment.types;

import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.WeaponType;

public class BluntTwoHandedType implements EquippableWeaponType {

    public static final BluntTwoHandedType INSTANCE = new BluntTwoHandedType();

    @Override
    public boolean typeOf(L2Weapon weapon) {
        return weapon.getItemType() == weaponType() && weapon.isTwoHanded();
    }

    @Override
    public String name() {
        return "BluntTwoHanded";
    }

    @Override
    public WeaponType weaponType() {
        return WeaponType.BLUNT;
    }
}