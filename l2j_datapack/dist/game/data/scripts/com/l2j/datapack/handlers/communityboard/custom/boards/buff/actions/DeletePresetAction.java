package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DeletePresetAction implements BoardAction {

    private static final Logger LOG = LogManager.getLogger();

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.isEmpty() || args.getArgs().size() != 1) {
            LOG.warn("{} is trying to delete buff preset with invalid args number {}", player, args.getArgs().size());
            return ProcessResult.failure(player.string("invalid_preset_buff_request"));
        }

        try {
            DAOFactory.getInstance().getCommunityBuffListDao().removeCommunityBuffList(player.getObjectId(), args.getArgs().get(0));
        } catch (RuntimeException e) {
            LOG.warn(player + " could not delete his preset " + args.getArgs().get(0), e);
            return ProcessResult.failure(player.string("could_not_delete_preset_n").replace("$n", args.getArgs().get(0)));
        }

        // Redirect to home buff list, to update it
        CommunityBoardHandler.getInstance().handleParseCommand("_bbs_buff_board", player);
        return ProcessResult.success();
    }

}
