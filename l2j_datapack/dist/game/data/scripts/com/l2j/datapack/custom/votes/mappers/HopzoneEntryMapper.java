package com.l2j.datapack.custom.votes.mappers;

import com.l2j.datapack.custom.votes.VoteEntry;
import com.l2jserver.common.mappers.Json;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class HopzoneEntryMapper implements VoteEntryMapper {

    private static final Logger LOG = LogManager.getLogger();

    private final String sourceCode;

    public HopzoneEntryMapper(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    @Override
    public List<VoteEntry> convert(L2PcInstance player, String contents) {
        try {
            HopzoneVote hopzoneVote = Json.INSTANCE.getJson().readValue(contents, HopzoneVote.class);
            if (hopzoneVote.isVoted()) {
                return Collections.singletonList(new VoteEntry(sourceCode, player.getName(), hopzoneVote.getVoteTime()));
            } else {
                return Collections.emptyList();
            }
        } catch (IOException e) {
            LOG.error("Could not retrieve vote for {} from {} because of: {}", player, sourceCode, e.getMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public String getSourceCode() {
        return sourceCode;
    }

}
