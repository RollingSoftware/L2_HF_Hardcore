package com.l2j.datapack.custom.bots.states

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance

object CanAct {
    @JvmStatic
    fun check(player: L2PcInstance): Boolean =
        !(player.isAllSkillsDisabled || player.isCoreAIDisabled || player.isCastingNow || player.isTeleporting)
}