package com.l2j.datapack.gameplay.deathrecap;

import com.l2jserver.gameserver.gameplay.damage.SkillHistory;
import com.l2jserver.gameserver.model.actor.L2Character;

import java.util.Map;

public class AttackerDamageRecap {
    private L2Character attacker;
    private double totalDamage;
    private Map<SkillHistory, Double> damageBySkill;

    public AttackerDamageRecap(L2Character attacker, double totalDamage, Map<SkillHistory, Double> damageBySkill) {
        this.attacker = attacker;
        this.totalDamage = totalDamage;
        this.damageBySkill = damageBySkill;
    }

    public L2Character getAttacker() {
        return attacker;
    }

    public double getTotalDamage() {
        return totalDamage;
    }

    public Map<SkillHistory, Double> getDamageBySkill() {
        return damageBySkill;
    }
}