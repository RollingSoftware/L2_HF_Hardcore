package com.l2j.datapack.custom.bots.manager

import com.l2jserver.common.pool.impl.ConnectionFactory
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.EmptyHandling
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.util.stream.Collectors

class BotDao {
    private val jdbi: Jdbi = Jdbi.create(ConnectionFactory.getInstance().dataSource)

    init {
        jdbi.registerRowMapper(BotEntryMapper())
    }

    fun createTableIfNotExist(): Boolean {
        return jdbi.withHandle<Int, RuntimeException> { h: Handle ->
            h.execute(
                "CREATE TABLE IF NOT EXISTS bots (character_id INT(6) UNSIGNED NOT NULL)"
            )
        } > 0
    }

    fun findAll(): List<BotEntry> {
        return jdbi.withHandle<List<BotEntry>, RuntimeException> { h: Handle ->
            h.createQuery("SELECT character_id FROM bots")
                .mapTo(BotEntry::class.java)
                .collect(Collectors.toList())
        }
    }

    fun create(characterIds: List<Int>): Boolean {
        return jdbi.withHandle<Boolean, RuntimeException> { handle ->
            val batch = handle.prepareBatch("INSERT INTO bots (character_id) VALUES (:characterId)")

            characterIds.forEach { characterId ->
                batch.bind("characterId", characterId)
            }

            batch.execute().all { it > 0 }
        }
    }

    fun delete(characterIds: List<Int>): Boolean {
        return jdbi.withHandle<Boolean, RuntimeException> { handle ->
            handle.createUpdate("DELETE FROM bots WHERE character_id IN (<characterIds>)")
                .bindList(EmptyHandling.BLANK, "characterIds", characterIds)
                .execute() > 0
        }
    }

    fun findAllExceptForIdsLimit(characterIds: List<Int>, limit: Int): List<BotEntry> {
        return jdbi.withHandle<List<BotEntry>, RuntimeException> { h: Handle ->
            val query =
                if (characterIds.isEmpty()) {
                    "SELECT character_id FROM bots ORDER BY character_id DESC LIMIT :limit"
                } else {
                    "SELECT character_id FROM bots WHERE character_id NOT IN (<characterIds>) ORDER BY character_id DESC LIMIT :limit"
                }
            h.createQuery(query)
                .bindList(EmptyHandling.BLANK, "characterIds", characterIds)
                .bind("limit", limit)
                .mapTo(BotEntry::class.java)
                .collect(Collectors.toList())
        }
    }
}

data class BotEntry(val characterId: Int)

class BotEntryMapper : RowMapper<BotEntry> {
    override fun map(rs: ResultSet, ctx: StatementContext): BotEntry {
        return BotEntry(rs.getInt("character_id"))
    }
}