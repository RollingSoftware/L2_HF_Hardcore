package com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.sets.presets;

import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.BuffFilter;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.BuffList;
import com.l2jserver.gameserver.model.holders.SkillHolder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MageFullBuff implements BuffList {

    public static final String NAME = "mage_full_buff";

    public final List<SkillHolder> buffs = Arrays.asList(
            new SkillHolder(1413, 1), // Magnus' Chant
            new SkillHolder(1078, 6), // Concentration
            new SkillHolder(1035, 4), // Mental Shield
            new SkillHolder(1303, 2), // Wild Magic
            new SkillHolder(1259, 4), // Resist Shock
            new SkillHolder(1503, 1), // Improved Shield Defense
            new SkillHolder(1504, 1), // Improved Movement
            new SkillHolder(1397, 3), // Clarity
            new SkillHolder(1460, 1), // Mana Gain
            new SkillHolder(1354, 1), // Arcane Protection
            new SkillHolder(1085, 3), // Acumen
            new SkillHolder(1062, 2), // Berserker Spirit
            new SkillHolder(1499, 1), // Improved Combat
            new SkillHolder(1501, 1), // Improved Condition
            new SkillHolder(1389, 3), // Greater Shield
            new SkillHolder(1542, 1), // Counter Critical
            new SkillHolder(1500, 1), // Improved Magic

            new SkillHolder(276, 1), // Dance of Concentration
            new SkillHolder(273, 1), // Dance of the Mystic
            new SkillHolder(915, 1), // Dance of Berserker
            new SkillHolder(365, 1), // Siren's Dance
            new SkillHolder(304, 1), // Song of Vitality
            new SkillHolder(268, 1), // Song of Wind
            new SkillHolder(267, 1), // Song of Warding
            new SkillHolder(264, 1), // Song of Earth
            new SkillHolder(349, 1), // Song of Renewal
            new SkillHolder(363, 1) // Song of Meditation
    );

    @Override
    public List<SkillHolder> getBuffs() {
        return buffs;
    }

    @Override
    public Optional<SkillHolder> findBySkillId(int skillId) {
        return BuffFilter.findBySkillId(buffs, skillId);
    }

}
