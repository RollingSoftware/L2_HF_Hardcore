package com.l2j.datapack.quests.Q00999_T1Tutorial;

import com.l2jserver.Config;
import com.l2jserver.common.util.StringUtil;
import com.l2jserver.gameserver.enums.audio.Sound;
import com.l2jserver.gameserver.enums.audio.Voice;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.State;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Q00999_T1Tutorial extends Quest {

    public static final int RECOMMENDATION_01 = 1067;
    public static final int RECOMMENDATION_02 = 1068;
    public static final int LEAF_OF_MOTHERTREE = 1069;
    public static final int BLOOD_OF_JUNDIN = 1070;
    public static final int LICENSE_OF_MINER = 1498;
    public static final int VOUCHER_OF_FLAME = 1496;
    public static final int SOULSHOT_NOVICE = 5789;
    public static final int SPIRITSHOT_NOVICE = 5790;
    public static final int BLUE_GEM = 6353;
    public static final int TOKEN = 8542;
    public static final int SCROLL = 8594;
    public static final int DIPLOMA = 9881;

    public static final String DEPENDENT_QUEST_NAME = "Q00255_Tutorial";

    public static class TutorialEvent {
        String htmlfile;
        int radarX;
        int radarY;
        int radarZ;
        int item;
        int classId1;
        int gift1;
        int count1;
        int classId2;
        int gift2;
        int count2;

        public TutorialEvent(String htmlfile,
                             int radarX, int radarY, int radarZ,
                             int item,
                             int classId1, int gift1, int count1,
                             int classId2, int gift2, int count2) {
            this.htmlfile = htmlfile;
            this.radarX = radarX;
            this.radarY = radarY;
            this.radarZ = radarZ;
            this.item = item;
            this.classId1 = classId1;
            this.gift1 = gift1;
            this.count1 = count1;
            this.classId2 = classId2;
            this.gift2 = gift2;
            this.count2 = count2;
        }
    }

    // # event, new TalkData(htmlfile,radarX,radarY,radarZ,item,classId1,gift1,count1,classId2,gift2,count2]
    private static final Map<String, TutorialEvent> TUTORIAL_EVENTS = createTutorialEventsData();

    public static Map<String, TutorialEvent> createTutorialEventsData() {
        var map = new HashMap<String, TutorialEvent>();
        map.put("32133_02", new TutorialEvent("32133-03.htm", -119692, 44504, 380, DIPLOMA, 0x7b, SOULSHOT_NOVICE, 200, 0x7c, SOULSHOT_NOVICE, 200));
        map.put("30008_02", new TutorialEvent("30008-03.htm", 0, 0, 0, RECOMMENDATION_01, 0x00, SOULSHOT_NOVICE, 200, 0x00, 0, 0));
        map.put("30008_04", new TutorialEvent("30008-04.htm", -84058, 243239, -3730, 0, 0x00, 0, 0, 0, 0, 0));
        map.put("30017_02", new TutorialEvent("30017-03.htm", 0, 0, 0, RECOMMENDATION_02, 0x0a, SPIRITSHOT_NOVICE, 100, 0x00, 0, 0));
        map.put("30017_04", new TutorialEvent("30017-04.htm", -84058, 243239, -3730, 0, 0x0a, 0, 0, 0x00, 0, 0));
        map.put("30370_02", new TutorialEvent("30370-03.htm", 0, 0, 0, LEAF_OF_MOTHERTREE, 0x19, SPIRITSHOT_NOVICE, 100, 0x12, SOULSHOT_NOVICE, 200));
        map.put("30370_04", new TutorialEvent("30370-04.htm", 45491, 48359, -3086, 0, 0x19, 0, 0, 0x12, 0, 0));
        map.put("30129_02", new TutorialEvent("30129-03.htm", 0, 0, 0, BLOOD_OF_JUNDIN, 0x26, SPIRITSHOT_NOVICE, 100, 0x1f, SOULSHOT_NOVICE, 200));
        map.put("30129_04", new TutorialEvent("30129-04.htm", 12116, 16666, -4610, 0, 0x26, 0, 0, 0x1f, 0, 0));
        map.put("30528_02", new TutorialEvent("30528-03.htm", 0, 0, 0, LICENSE_OF_MINER, 0x35, SOULSHOT_NOVICE, 200, 0x00, 0, 0));
        map.put("30528_04", new TutorialEvent("30528-04.htm", 115642, -178046, -941, 0, 0x35, 0, 0, 0x00, 0, 0));
        map.put("30573_02", new TutorialEvent("30573-03.htm", 0, 0, 0, VOUCHER_OF_FLAME, 0x31, SPIRITSHOT_NOVICE, 100, 0x2c, SOULSHOT_NOVICE, 200));
        map.put("30573_04", new TutorialEvent("30573-04.htm", -45067, -113549, -235, 0, 0x31, 0, 0, 0x2c, 0, 0));
        return map;
    }


    private static class TalkData {
        int raceId;
        List<String> htmlfiles;
        int npcType;
        int item;

        public TalkData(int raceId, List<String> htmlfiles, int npcType, int item) {
            this.raceId = raceId;
            this.htmlfiles = htmlfiles;
            this.npcType = npcType;
            this.item = item;
        }
    }

    private static final Map<Integer, TalkData> TALKS = createTalksData();

    public static Map<Integer, TalkData> createTalksData() {
        var map = new HashMap<Integer, TalkData>();
        map.put(30017, new TalkData(0, List.of("30017-01.htm", "30017-02.htm", "30017-04.htm"), 0, 0));
        map.put(30008, new TalkData(0, List.of("30008-01.htm", "30008-02.htm", "30008-04.htm"), 0, 0));
        map.put(30370, new TalkData(1, List.of("30370-01.htm", "30370-02.htm", "30370-04.htm"), 0, 0));
        map.put(30129, new TalkData(2, List.of("30129-01.htm", "30129-02.htm", "30129-04.htm"), 0, 0));
        map.put(30573, new TalkData(3, List.of("30573-01.htm", "30573-02.htm", "30573-04.htm"), 0, 0));
        map.put(30528, new TalkData(4, List.of("30528-01.htm", "30528-02.htm", "30528-04.htm"), 0, 0));
        map.put(30018, new TalkData(0, List.of("30131-01.htm", "0", "30019-03a.htm", "30019-04.htm"), 1, RECOMMENDATION_02));
        map.put(30019, new TalkData(0, List.of("30131-01.htm", "0", "30019-03a.htm", "30019-04.htm"), 1, RECOMMENDATION_02));
        map.put(30020, new TalkData(0, List.of("30131-01.htm", "0", "30019-03a.htm", "30019-04.htm"), 1, RECOMMENDATION_02));
        map.put(30021, new TalkData(0, List.of("30131-01.htm", "0", "30019-03a.htm", "30019-04.htm"), 1, RECOMMENDATION_02));
        map.put(30009, new TalkData(0, List.of("30530-01.htm", "30009-03.htm", "0", "30009-04.htm"), 1, RECOMMENDATION_01));
        map.put(30011, new TalkData(0, List.of("30530-01.htm", "30009-03.htm", "0", "30009-04.htm"), 1, RECOMMENDATION_01));
        map.put(30012, new TalkData(0, List.of("30530-01.htm", "30009-03.htm", "0", "30009-04.htm"), 1, RECOMMENDATION_01));
        map.put(30056, new TalkData(0, List.of("30530-01.htm", "30009-03.htm", "0", "30009-04.htm"), 1, RECOMMENDATION_01));
        map.put(30400, new TalkData(1, List.of("30131-01.htm", "30400-03.htm", "30400-03a.htm", "30400-04.htm"), 1, LEAF_OF_MOTHERTREE));
        map.put(30401, new TalkData(1, List.of("30131-01.htm", "30400-03.htm", "30400-03a.htm", "30400-04.htm"), 1, LEAF_OF_MOTHERTREE));
        map.put(30402, new TalkData(1, List.of("30131-01.htm", "30400-03.htm", "30400-03a.htm", "30400-04.htm"), 1, LEAF_OF_MOTHERTREE));
        map.put(30403, new TalkData(1, List.of("30131-01.htm", "30400-03.htm", "30400-03a.htm", "30400-04.htm"), 1, LEAF_OF_MOTHERTREE));
        map.put(30131, new TalkData(2, List.of("30131-01.htm", "30131-03.htm", "30131-03a.htm", "30131-04.htm"), 1, BLOOD_OF_JUNDIN));
        map.put(30404, new TalkData(2, List.of("30131-01.htm", "30131-03.htm", "30131-03a.htm", "30131-04.htm"), 1, BLOOD_OF_JUNDIN));
        map.put(30574, new TalkData(3, List.of("30575-01.htm", "30575-03.htm", "30575-03a.htm", "30575-04.htm"), 1, VOUCHER_OF_FLAME));
        map.put(30575, new TalkData(3, List.of("30575-01.htm", "30575-03.htm", "30575-03a.htm", "30575-04.htm"), 1, VOUCHER_OF_FLAME));
        map.put(30530, new TalkData(4, List.of("30530-01.htm", "30530-03.htm", "0", "30530-04.htm"), 1, LICENSE_OF_MINER));
        map.put(32133, new TalkData(5, List.of("32133-01.htm", "32133-02.htm", "32133-04.htm"), 0, 0));
        map.put(32134, new TalkData(5, List.of("32134-01.htm", "32134-03.htm", "0", "32134-04.htm"), 1, DIPLOMA));
        return map;
    }

    @Override
    public String onAdvEvent(String event, L2Npc npc, L2PcInstance player) {
        if (Config.DISABLE_TUTORIAL) {
            return null;
        }
        var st = getQuestState(player, false);
        if (st == null) {
            return null;
        }

        var htmltext = event;
        var qs = st.getPlayer().getQuestState(DEPENDENT_QUEST_NAME);

        if (qs == null) {
            return null;
        }

        var Ex = qs.getInt("Ex");
        var classId = st.getPlayer().getClassId();
        switch (event) {
            case "TimerEx_NewbieHelper":
                if (Ex == 0) {
                    if (classId.isMage()) {
                        st.playSound(Voice.TUTORIAL_VOICE_009B);
                    } else {
                        st.playSound(Voice.TUTORIAL_VOICE_009A);
                        qs.set("Ex", "1");
                    }
                } else if (Ex == 3) {
                    st.playSound(Voice.TUTORIAL_VOICE_025_1000);
                    qs.set("Ex", "4");

                    return null;
                }
                break;
            case "TimerEx_GrandMaster":
                if (Ex >= 4) {
                    st.showQuestionMark(7);
                    st.playSound(Sound.ITEMSOUND_QUEST_TUTORIAL);
                    st.playSound(Voice.TUTORIAL_VOICE_025_1000);
                    return null;
                }
                break;
            case "isle":
                st.addRadar(-119692, 44504, 380);
                st.getPlayer().teleToLocation(-120050, 44500, 360);
                htmltext = "<html><body>" + npc.getName() + ":<br>Go to the <font color=\"LEVEL\">Isle of Souls</font> and meet the <font color=\"LEVEL\">Newbie Guide</font> there to learn a number of important tips. He will also give you an item to assist your development. <br>Follow the direction arrow above your head and it will lead you to the Newbie Guide. Good luck!</body></html>";
                break;
            default:
                var eventData = TUTORIAL_EVENTS.get(event);
                if (eventData.radarX != 0) {
                    st.addRadar(eventData.radarX, eventData.radarY, eventData.radarZ);
                }
                htmltext = eventData.htmlfile;
                if (st.getQuestItemsCount(eventData.item) > 0 && st.getInt("onlyone") == 0) {
                    st.addExpAndSp(0, 50);
                    st.startQuestTimer("TimerEx_GrandMaster", 60000);
                    st.takeItems(eventData.item, 1);
                    if (Ex <= 3) {
                        qs.set("Ex", "4");
                    }
                    if (classId.getId() == eventData.classId1) {
                        st.giveItems(eventData.gift1, eventData.count1);
                    }
                    if (eventData.gift1 == SPIRITSHOT_NOVICE) {
                        st.playSound(Voice.TUTORIAL_VOICE_027_1000);
                    } else {
                        st.playSound(Voice.TUTORIAL_VOICE_026_1000);
                    }
                } else if (classId.getId() == eventData.classId2 && eventData.gift2 > 0) {
                    st.giveItems(eventData.gift2, eventData.count2);
                    st.playSound(Voice.TUTORIAL_VOICE_026_1000);
                    st.unset("step");
                    st.set("onlyone", "1");
                }
                break;
        }

        return htmltext;
    }

    @Override
    public String onFirstTalk(L2Npc npc, L2PcInstance player) {
        if (Config.DISABLE_TUTORIAL) {
            npc.showChatWindow(player);
            return null;
        }
        var qs = player.getQuestState(DEPENDENT_QUEST_NAME);
        if (qs == null) {
            npc.showChatWindow(player);
            return null;
        }
        var st = getQuestState(player, true);
        var htmltext = "";
        var Ex = qs.getInt("Ex");
        var npcId = npc.getId();
        var step = st.getInt("step");
        var onlyone = st.getInt("onlyone");
        var level = player.getLevel();
        var isMage = player.getClassId().isMage();

        var npcIdData = TALKS.get(npcId);

        int raceId = 0;
        List<String> htmlfiles = List.of();
        int npcTyp = 0;
        int item = 0;
        if (npcIdData != null) {
            raceId = npcIdData.raceId;
            htmlfiles = npcIdData.htmlfiles;
            npcTyp = npcIdData.npcType;
            item = npcIdData.item;
        }

        if ((level >= 10 || onlyone > 0) && npcTyp == 1) {
            htmltext = "30575-05.htm";
        } else if (List.of(30600, 30601, 30602, 30598, 30599, 32135).contains(npcId)) {
            var reward = qs.getInt("reward");
            if (reward == 0) {
                if (isMage) {
                    st.playSound(Voice.TUTORIAL_VOICE_027_1000);
                    st.giveItems(SPIRITSHOT_NOVICE, 100);
                } else {
                    st.playSound(Voice.TUTORIAL_VOICE_026_1000);
                    st.giveItems(SOULSHOT_NOVICE, 200);
                }
            }

            st.giveItems(TOKEN, 12);
            st.giveItems(SCROLL, 2);
            qs.set("reward", "1");
            st.exitQuest(true);
            npc.showChatWindow(player);
            return null;
        } else if (onlyone == 0 && level < 10) {
            if (player.getRace().ordinal() == raceId) {
                htmltext = htmlfiles.get(0);
            }
            if (npcTyp == 1) {
                if (step == 0 && Ex < 0) {
                    qs.set("Ex", "0");
                    st.startQuestTimer("TimerEx_NewbieHelper", 30000);
                    if (isMage) {
                        st.set("step", "1");
                        st.setState(State.STARTED);
                    } else {
                        htmltext = "30530-01.htm";
                        st.set("step", "1");
                        st.setState(State.STARTED);
                    }
                }

                if (step == 1 && st.getQuestItemsCount(item) == 0 && List.of(0, 1, 2).contains(Ex)) {
                    if (st.getQuestItemsCount(BLUE_GEM) > 0) {
                        st.takeItems(BLUE_GEM, st.getQuestItemsCount(BLUE_GEM));
                        st.giveItems(item, 1);
                        st.set("step", "2");
                        qs.set("Ex", "3");
                        st.startQuestTimer("TimerEx_NewbieHelper", 30000);
                        qs.set("ucMemo", "3");

                        if (isMage) {
                            st.playSound(Voice.TUTORIAL_VOICE_027_1000);
                            st.giveItems(SPIRITSHOT_NOVICE, 100);
                            htmltext = htmlfiles.get(2);
                            if (htmltext.equals("0")) {
                                htmltext = "<html><body>I am sorry.  I only help warriors.  Please go to another Newbie Helper who may assist you.</body></html>";
                            }
                        } else {
                            st.playSound(Voice.TUTORIAL_VOICE_026_1000);
                            st.giveItems(SOULSHOT_NOVICE, 200);
                            htmltext = htmlfiles.get(1);
                            if (htmltext.equals("0")) {
                                htmltext = "<html><body>I am sorry.  I only help mystics.  Please go to another Newbie Helper who may assist you.</body></html>";
                            }
                        }
                    } else {
                        if (isMage) {
                            htmltext = "30131-02.htm";
                            if (player.getRace().ordinal() == 3) {
                                htmltext = "30575-02.htm";
                            }
                        } else {
                            htmltext = "30530-02.htm";
                        }
                    }
                } else if (step == 2) {
                    htmltext = htmlfiles.get(3);
                }
            } else if (npcTyp == 0) {
                if (step == 1) {
                    htmltext = htmlfiles.get(0);
                } else if (step == 2) {
                    htmltext = htmlfiles.get(1);
                } else if (step == 3) {
                    htmltext = htmlfiles.get(2);
                }
            }
        } else if (st.getState() == State.COMPLETED && npcTyp == 0) {
            htmltext = npc.getId() + "-04.htm";
        }

        if (StringUtil.isBlank(htmltext)) {
            npc.showChatWindow(player);
        }
        return htmltext;
    }

    @Override
    public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon) {
        if (Config.DISABLE_TUTORIAL) {
            return null;
        }
        var st = getQuestState(killer, false);
        if (st == null) {
            return null;
        }
        var qs = st.getPlayer().getQuestState(DEPENDENT_QUEST_NAME);

        if (qs == null) {
            return null;
        }
        var ex = qs.get("Ex");
        if ("0".equals(ex) || "1".equals(ex)) {
            st.playSound(Voice.TUTORIAL_VOICE_011_1000);
            st.showQuestionMark(3);
            qs.set("Ex", "2");
        }

        if (List.of("0", "1", "2").contains(ex) && st.getQuestItemsCount(BLUE_GEM) < 1) {
            npc.dropItem(killer, BLUE_GEM, 1);
            st.playSound(Sound.ITEMSOUND_QUEST_TUTORIAL);
        }
        return null;
    }

    public Q00999_T1Tutorial() {
        super(999, "Q00999_T1Tutorial", "Kamael Tutorial");

        for (var startNpc : List.of(
                30008, 30009, 30017, 30019, 30129,
                30131, 30573, 30575, 30370, 30528,
                30530, 30400, 30401, 30402, 30403,
                30404, 30600, 30601, 30602, 30598,
                30599, 32133, 32134, 32135)) {
            addStartNpc(startNpc);
            addFirstTalkId(startNpc);
            addTalkId(startNpc);
        }

        addKillId(18342);
        addKillId(20001);
    }
}
