package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.common.renderer.ButtonRender;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

public class CreatePresetDialogAction implements BoardAction {

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        player.sendPacket(new NpcHtmlMessage("<center><table><tr><td valign=middle>" +
                player.string("preset_name") + ": <edit var=\"preset_name\" width=150>" +
                ButtonRender.render(player.string("create"), "bypass -h _bbs_buff create_preset $preset_name") +
                ButtonRender.render(player.string("cancel"), "bypass -h _bbshome") +
                "</td></tr></table></center>"));
        return ProcessResult.success();
    }

}
