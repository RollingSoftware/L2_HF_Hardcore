package com.l2j.datapack.handlers.bypasshandlers.npcviewmod;

import com.l2jserver.common.primitives.Range;

public interface PageRenderable {

    String render(Range range);

}
