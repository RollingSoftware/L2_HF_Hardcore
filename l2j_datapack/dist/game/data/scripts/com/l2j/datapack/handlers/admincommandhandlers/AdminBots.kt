package com.l2j.datapack.handlers.admincommandhandlers

import com.l2j.datapack.custom.bots.manager.BotCommand
import com.l2j.datapack.custom.bots.manager.BotCommands
import com.l2j.datapack.handlers.communityboard.custom.ActionArgs
import com.l2jserver.gameserver.handler.IAdminCommandHandler
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import org.apache.logging.log4j.LogManager

class AdminBots : IAdminCommandHandler {
    private val log = LogManager.getLogger()

    override fun useAdminCommand(command: String, activeChar: L2PcInstance): Boolean {
        if (!command.startsWith("admin_bots")) {
            return false
        }
        return ActionArgs.parse(command)
            .map { action ->
                when (action.actionName) {
                    "stop-manager" -> {
                        BotCommands.events.notifyAll(BotCommand.StopManager)
                        true
                    }
                    "start-manager" -> {
                        BotCommands.events.notifyAll(BotCommand.StartManager)
                        true
                    }
                    "restart-manager" -> {
                        BotCommands.events.notifyAll(BotCommand.RestartManager)
                        true
                    }
                    "insert-me" -> {
                        BotCommands.events.notifyAll(BotCommand.InsertBot(activeChar))
                        true
                    }
                    "insert-him" -> {
                        BotCommands.events.notifyAll(BotCommand.InsertBot(activeChar.target.actingPlayer))
                        true
                    }
                    "set-count" -> {
                        if (action.args.isEmpty()) {
                            false
                        } else {
                            BotCommands.events.notifyAll(BotCommand.SetAmount(action.args.first().toInt()))
                            true
                        }
                    }
                    else -> false
                }
            }.orElseGet {
                log.warn("Player {} used '{}' with wrong arguments", activeChar, command)
                false
            }
    }

    override fun getAdminCommandList(): Array<String> = arrayOf(
        "admin_bots"
    )
}