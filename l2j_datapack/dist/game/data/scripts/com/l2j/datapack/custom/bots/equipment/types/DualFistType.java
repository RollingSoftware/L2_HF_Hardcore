package com.l2j.datapack.custom.bots.equipment.types;

import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.WeaponType;

public class DualFistType implements EquippableWeaponType {

    public static final DualFistType INSTANCE = new DualFistType();

    @Override
    public boolean typeOf(L2Weapon weapon) {
        return weapon.getItemType() == weaponType();
    }

    @Override
    public String name() {
        return "DualFist";
    }

    @Override
    public WeaponType weaponType() {
        return WeaponType.DUALFIST;
    }
}