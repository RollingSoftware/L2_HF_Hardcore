package com.l2j.datapack.custom.bots.farmzones

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance

class FarmZoneManager(
    private val farmZonesTable: FarmZonesTable,
    private var lastActiveFarmZone: FarmZone? = null
) {
    fun findClosestForLevel(player: L2PcInstance): FarmZone? {
        return farmZonesTable.findClosestForLevel(player)
            .filter {
                lastActiveFarmZone != it
            }.randomOrNull()
    }

    fun findNewZoneForNewLevels(player: L2PcInstance): FarmZone? {
        return farmZonesTable.findClosestForLevel(player)
            .filter {
                it.levels > lastActiveFarmZone?.levels
            }.randomOrNull()
    }

    fun setCurrent(farmZone: FarmZone) {
        lastActiveFarmZone = farmZone
    }

    fun forgetCurrent() {
        lastActiveFarmZone = null
    }
}