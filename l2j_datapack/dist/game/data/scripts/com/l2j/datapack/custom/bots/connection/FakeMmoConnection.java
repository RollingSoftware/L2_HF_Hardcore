package com.l2j.datapack.custom.bots.connection;

import com.l2jserver.mmocore.MMOClient;
import com.l2jserver.mmocore.MMOConnectionInterface;
import com.l2jserver.mmocore.NioNetStackList;
import com.l2jserver.mmocore.SendablePacket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;

public class FakeMmoConnection<T extends MMOClient<?>> implements MMOConnectionInterface<T> {

    private T client;

    @Override
    public T getClient() {
        return client;
    }

    @Override
    public void setClient(T client) {
        this.client = client;
    }

    @Override
    public void sendPacket(SendablePacket<T> sp) {
        sp.setClient(client);
    }

    @Override
    public SelectionKey getSelectionKey() {
        return null;
    }

    @Override
    public InetAddress getInetAddress() {
        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public int getPort() {
        return 7777;
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public int read(ByteBuffer buf) throws IOException {
        return 0;
    }

    @Override
    public int write(ByteBuffer buf) throws IOException {
        return 0;
    }

    @Override
    public void createWriteBuffer(ByteBuffer buf) {

    }

    @Override
    public boolean hasPendingWriteBuffer() {
        return false;
    }

    @Override
    public void movePendingWriteBufferTo(ByteBuffer dest) {

    }

    @Override
    public ByteBuffer getReadBuffer() {
        return null;
    }

    @Override
    public void setReadBuffer(ByteBuffer buf) {

    }

    @Override
    public boolean isClosed() {
        return false;
    }

    @Override
    public NioNetStackList<SendablePacket<T>> getSendQueue() {
        return new NioNetStackList<>();
    }

    @Override
    public void close(SendablePacket<T> sp) {

    }

    @Override
    public void close(SendablePacket<T>[] closeList) {

    }

    @Override
    public void releaseBuffers() {

    }
}
