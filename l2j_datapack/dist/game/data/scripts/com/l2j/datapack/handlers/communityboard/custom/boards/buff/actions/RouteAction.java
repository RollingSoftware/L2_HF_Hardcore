package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;

import java.util.Map;

public class RouteAction implements BoardAction {

    private final Map<String, BoardAction> actions;

    public RouteAction(Map<String, BoardAction> actions) {
        this.actions = actions;
    }

    public Map<String, BoardAction> getActions() {
        return actions;
    }

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.getArgs().isEmpty()) {
            return ProcessResult.failure(player.string("no_action_route_specified"));
        }

        String actionName = args.getArgs().get(0);
        BoardAction boardAction = actions.get(actionName);
        if (boardAction == null) {
            return ProcessResult.failure(player.string("no_action_named_n_is_available").replace("$n", actionName));
        }

        return boardAction.process(player, ActionArgs.subActionArgs(args));
    }
}
