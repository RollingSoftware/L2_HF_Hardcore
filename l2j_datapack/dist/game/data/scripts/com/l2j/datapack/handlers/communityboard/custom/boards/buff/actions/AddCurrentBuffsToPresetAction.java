package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.sets.AllBuffs;
import com.l2jserver.Config;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.common.util.StringUtil;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.CommunityBuffList;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

public class AddCurrentBuffsToPresetAction implements BoardAction {

    private static final Logger LOG = LogManager.getLogger();

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.isEmpty() || args.getArgs().size() != 1) {
            LOG.warn("Invalid add current buffs to preset request from {} with args {}", player, args);
            return ProcessResult.failure(player.string("invalid_request"));
        }

        String presetName = args.getArgs().get(0);
        if (StringUtil.isBlank(presetName)) {
            return ProcessResult.failure(player.string("preset_id_cannot_be_empty"));
        }

        Optional<CommunityBuffList> buffList = DAOFactory.getInstance().getCommunityBuffListDao().findSingleCommunityBuffSet(player.getObjectId(), presetName);
        if (buffList.isEmpty()) {
            LOG.warn("Player {} is trying to add buffs to list that does not exist or does not belong to him {}", player, presetName);
            return ProcessResult.success();
        }

        List<BuffInfo> currentPlayerBuffs = new ArrayList<>(player.getEffectList().getBuffs());
        List<BuffInfo> currentPlayerDances = new ArrayList<>(player.getEffectList().getDances());

        Set<SkillHolder> currentPlayerBuffSkills = currentPlayerBuffs.stream().map(buff -> new SkillHolder(buff.getSkill().getId(), buff.getSkill().getLevel())).collect(Collectors.toCollection(LinkedHashSet::new));
        Set<SkillHolder> currentPlayerDanceSkills = currentPlayerDances.stream().map(buff -> new SkillHolder(buff.getSkill().getId(), buff.getSkill().getLevel())).collect(Collectors.toCollection(LinkedHashSet::new));

        Set<SkillHolder> allCurrentPlayerBuffs = new LinkedHashSet<>();
        allCurrentPlayerBuffs.addAll(currentPlayerBuffSkills);
        allCurrentPlayerBuffs.addAll(currentPlayerDanceSkills);

        Set<SkillHolder> availableBuffs = new LinkedHashSet<>(new AllBuffs().getBuffs());
        allCurrentPlayerBuffs.retainAll(availableBuffs);

        Set<SkillHolder> buffsAlreadyInList = new LinkedHashSet<>(buffList.get().getSkills());
        allCurrentPlayerBuffs.removeAll(buffsAlreadyInList);

        for (SkillHolder buff : allCurrentPlayerBuffs) {
            if (buffList.get().getSkills().size() < Config.MAX_CUSTOM_PRESET_BUFFS) {
                DAOFactory.getInstance().getCommunityBuffListDao().addToCommunityBuffList(buffList.get().getId(), buff);
            } else {
                break;
            }
        }

        CommunityBoardHandler.getInstance().handleParseCommand("_bbs_buff update_preset " + buffList.get().getName(), player);
        return ProcessResult.success();
    }

}
