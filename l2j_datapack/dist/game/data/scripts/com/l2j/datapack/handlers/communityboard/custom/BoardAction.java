package com.l2j.datapack.handlers.communityboard.custom;

import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

public interface BoardAction {

    ProcessResult process(L2PcInstance player, ActionArgs args);

}
