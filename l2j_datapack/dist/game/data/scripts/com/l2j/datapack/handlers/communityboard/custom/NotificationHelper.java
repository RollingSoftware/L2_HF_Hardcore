package com.l2j.datapack.handlers.communityboard.custom;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.util.Util;

public class NotificationHelper {

    public static void notify(L2PcInstance player, String message) {
        var notificationHtml = HtmCache.getInstance().getHtm(
                player.getHtmlPrefix(),
                "data/html/CommunityBoard/notification.html"
        );
        Util.sendHtml(
                player,
                notificationHtml.replace("{{MESSAGE}}", message)
        );
    }

}
