package com.l2j.datapack.custom.bots.equipment

import com.l2j.datapack.custom.bots.equipment.types.EquippableWeaponType
import com.l2jserver.common.noarg.NoArg
import com.l2jserver.gameserver.data.xml.impl.ArmorSetsData
import com.l2jserver.gameserver.model.GradedArmorSet
import com.l2jserver.gameserver.model.items.graded.Grade
import com.l2jserver.gameserver.model.items.type.ArmorType
import com.l2jserver.gameserver.model.stats.Stats
import java.util.NavigableMap
import java.util.TreeMap

@NoArg
data class EquipmentPreferences(
    val preferredWeaponTypes: List<EquippableWeaponType>,
    val preferredWeaponStats: List<Stats>,

    val preferredArmorTypes: List<ArmorType>,
    val preferredArmorStats: List<Stats>,

    val preferredShieldTypes: List<ArmorType> = listOf(ArmorType.NONE),

    val preferredSetsByGrade: NavigableMap<Grade, List<Int>> = TreeMap()
) {
    fun getAllPreferredSets(level: Int): List<GradedArmorSet> {
        val currentGrade = Grade.resolveGrade(level)
        if (preferredSetsByGrade.isEmpty()) {
            return emptyList()
        }

        return preferredSetsByGrade
            .filter {
                currentGrade >= it.key
            }
            .entries
            .fold<Map.Entry<Grade, List<Int>>, MutableList<GradedArmorSet>>(mutableListOf()) { acc, entry ->
                acc.addAll(entry.value.map { GradedArmorSet(entry.key, ArmorSetsData.getInstance().getSetById(it)) })
                acc
            }.sortedByDescending { it.grade }
    }
}
