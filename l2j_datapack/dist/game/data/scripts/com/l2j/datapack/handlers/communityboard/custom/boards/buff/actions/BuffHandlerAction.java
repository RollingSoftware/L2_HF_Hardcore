package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2j.datapack.handlers.communityboard.custom.BoardCondition;
import com.l2j.datapack.handlers.communityboard.custom.CommunityPaymentHelper;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.BuffList;
import com.l2jserver.Config;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.target.TargetHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.Optional;


public class BuffHandlerAction implements BoardAction {

    private static final Logger LOG = LogManager.getLogger();

    private final Map<String, BuffList> buffMap;

    public BuffHandlerAction(Map<String, BuffList> buffMap) {
        this.buffMap = buffMap;
    }

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.getArgs().size() != 3) {
            LOG.warn("Player {} sent and incorrect buff request", player);
            return ProcessResult.failure(player.string("invalid_buff_request"));
        }

        ProcessResult checkResult = BoardCondition.checkCondition(player);
        if (checkResult.isFailure()) {
            return checkResult;
        }

        String buffCategory = args.getArgs().get(0);
        int skillId = Integer.parseInt(args.getArgs().get(1));

        String targetString = args.getArgs().get(2);

        TargetHolder targetHolder = TargetHolder.parseTarget(player, targetString);

        BuffList categoryBuffs = buffMap.get(buffCategory);
        Optional<SkillHolder> buffHolder = categoryBuffs.findBySkillId(skillId);
        if (buffHolder.isEmpty()) {
            return ProcessResult.failure(player.string("this_buff_is_not_available"));
        }

        ProcessResult paymentResult = CommunityPaymentHelper.payForService(player, Config.COMMUNITY_SINGLE_BUFF_PRICE);
        if (paymentResult.isFailure()) {
            return paymentResult;
        }

        int delay = (int) (GameTimeController.TICKS_PER_SECOND * GameTimeController.MILLIS_IN_TICK * Config.COMMUNITY_BUFF_SPEED);
        if (targetHolder.isSummonTarget()) {
            targetHolder.getMaster().fakeCast(
                    buffHolder.get(),
                    delay,
                    () -> buffHolder.get().getSkill().applyEffects(targetHolder.getMaster(), targetHolder.getSummon()),
                    targetHolder.getSummon());
        } else {
            targetHolder.getMaster().fakeCast(
                    buffHolder.get(),
                    delay,
                    () -> buffHolder.get().getSkill().applyEffects(targetHolder.getMaster(), targetHolder.getMaster()),
                    targetHolder.getMaster());
        }

        return ProcessResult.success();
    }

}
