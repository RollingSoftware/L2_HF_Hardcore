package com.l2j.datapack.gameplay.deathrecap;

import com.l2j.datapack.ai.npc.AbstractNpcAI;
import com.l2jserver.common.util.Pair;
import com.l2jserver.gameserver.gameplay.damage.DamageRecord;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.events.Containers;
import com.l2jserver.gameserver.model.events.EventType;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerDeath;
import com.l2jserver.gameserver.model.events.listeners.ConsumerEventListener;
import com.l2jserver.gameserver.util.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class DeathRecap extends AbstractNpcAI {

    private static final Logger LOG = LogManager.getLogger();

    private final AttackerDamageRecapRender attackerDamageRecapRender = new AttackerDamageRecapRender();

    public DeathRecap() {
        super(DeathRecap.class.getSimpleName(), "Death Recap");
        Containers.Players().addListener(new ConsumerEventListener(
                Containers.Players(),
                EventType.ON_PLAYER_DEATH,
                (Consumer<OnPlayerDeath>) this::displayDeathRecap,
                this)
        );
    }

    public void displayDeathRecap(OnPlayerDeath event) {
        if (event.getCombatHistory().getDamageRecords().isEmpty()) {
            LOG.debug("Death Recap was empty, nothing to show");
            return;
        }

        Map<L2Character, List<DamageRecord>> attackersRecords = event
                .getCombatHistory().getDamageRecords().stream()
                .collect(groupingBy(DamageRecord::getAttacker));

        var attackerDamageRecapsSorted = attackersRecords.entrySet().stream().map(entry -> {
                    var skillsDamage =
                            entry.getValue().stream().collect(groupingBy(DamageRecord::getSkill));

                    var damageBySkill = skillsDamage.entrySet().stream().map(skillDamageEntry -> {
                        var skillDamageSum = skillDamageEntry.getValue().stream().mapToDouble(DamageRecord::getAmount).sum();
                        return Pair.of(skillDamageEntry.getKey(), skillDamageSum);
                    }).collect(Collectors.toMap(Pair::getLeft, Pair::getRight));

                    double totalDamage = damageBySkill.values().stream().mapToDouble(Double::doubleValue).sum();

                    return new AttackerDamageRecap(
                            entry.getKey(),
                            totalDamage,
                            damageBySkill
                    );
                }
        ).sorted(Comparator.comparingDouble(AttackerDamageRecap::getTotalDamage).reversed())
                .limit(20)
                .collect(Collectors.toList());

        var html = attackerDamageRecapRender.render(attackerDamageRecapsSorted);
        Util.sendHtml(event.getPlayer(), html);
    }

}
