package com.l2j.datapack.custom.bots.equipment

data class EquipmentSlot(
    val slotOnCharacter: Int,
    val itemBodyPart: Int
)