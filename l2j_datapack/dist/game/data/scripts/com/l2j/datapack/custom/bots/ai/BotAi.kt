package com.l2j.datapack.custom.bots.ai

import com.google.common.collect.Sets
import com.l2j.datapack.custom.bots.behaviour.ClassBehaviourTable
import com.l2j.datapack.custom.bots.equipment.EquipmentManager
import com.l2j.datapack.custom.bots.equipment.EquipmentTable
import com.l2j.datapack.custom.bots.farmzones.FarmZone
import com.l2j.datapack.custom.bots.farmzones.FarmZoneManager
import com.l2j.datapack.custom.bots.farmzones.FarmZonesTable
import com.l2j.datapack.custom.bots.goals.ReachLevelGoal
import com.l2j.datapack.custom.bots.skills.AiSkillHelper
import com.l2j.datapack.custom.bots.states.BuffHelper
import com.l2j.datapack.custom.bots.states.CanAct
import com.l2j.datapack.custom.bots.states.ClassChangingHelper
import com.l2j.datapack.custom.bots.states.CommunityTeleportToState
import com.l2j.datapack.custom.bots.states.EquipmentManagementState
import com.l2j.datapack.custom.bots.states.KamaelChangeWeaponHelper
import com.l2j.datapack.custom.bots.states.PickupHelper
import com.l2j.datapack.custom.bots.states.ResurrectionHelper
import com.l2j.datapack.custom.bots.states.ShoppingSequence
import com.l2j.datapack.custom.bots.states.StuckMonitoring
import com.l2j.datapack.custom.bots.states.TargetFinder
import com.l2j.datapack.custom.bots.states.WalkAroundState
import com.l2j.datapack.custom.items.IdTable
import com.l2j.datapack.handlers.communityboard.custom.CommunityPaymentHelper
import com.l2jserver.Config
import com.l2jserver.common.monitoring.MetricsManager
import com.l2jserver.common.util.Rnd
import com.l2jserver.gameserver.GeoData
import com.l2jserver.gameserver.ThreadPoolManager
import com.l2jserver.gameserver.ai.CtrlEvent
import com.l2jserver.gameserver.ai.CtrlIntention
import com.l2jserver.gameserver.ai.L2CharacterAI
import com.l2jserver.gameserver.data.xml.impl.ClassListData
import com.l2jserver.gameserver.instancemanager.TownManager
import com.l2jserver.gameserver.model.actor.L2Character
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.events.EventType
import com.l2jserver.gameserver.model.events.impl.IBaseEvent
import com.l2jserver.gameserver.model.events.impl.character.OnCreatureAttacked
import com.l2jserver.gameserver.model.events.listeners.AbstractEventListener
import com.l2jserver.gameserver.model.events.listeners.ConsumerEventListener
import com.l2jserver.gameserver.model.skills.Skill
import com.l2jserver.gameserver.model.stats.Formulas
import com.l2jserver.gameserver.network.clientpackets.UseItem
import com.l2jserver.gameserver.util.Util
import io.micrometer.core.instrument.Timer
import org.apache.logging.log4j.LogManager
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.atomic.AtomicBoolean

class BotAi(
    val player: L2PcInstance,
    farmZonesTable: FarmZonesTable,
    private val classBehaviourTable: ClassBehaviourTable,
    private val equipmentTable: EquipmentTable
) : L2CharacterAI(player), Runnable {

    private val logger = LogManager.getLogger()

    private val botTickTimer: Timer =
        Timer.builder("l2j-gs-bot-ai-latency").tag("method", "onEvtThink").register(MetricsManager.instance.metricsRegistry)

    private var lastKnownClassId = -1

    private var onAttackedListener: AbstractEventListener? = null
    private var aiTask: ScheduledFuture<*>? = null

    private var classBehaviour = classBehaviourTable.findBehaviour(player.classId)
    private var aiSkillHelper = AiSkillHelper(player, classBehaviour.skillPreferences)
    private var equipmentPreferences = equipmentTable.findPreference(classBehaviour.equipmentGroup)
    private val equipmentManager = EquipmentManager(
        player,
        equipmentPreferences
    )

    private var equipmentManagementState = EquipmentManagementState(player, equipmentManager)

    private val resurrectionHelper = ResurrectionHelper(player)
    private val buffHelper = BuffHelper(player, classBehaviour)
    private val farmZoneManager = FarmZoneManager(farmZonesTable)

    private val attackers: MutableSet<L2Character> = Sets.newConcurrentHashSet()
    private val targetFinder = TargetFinder(player)
    private val stuckMonitoring = StuckMonitoring(player)
    private val walkAroundState = WalkAroundState(player)
    private val shoppingSequence = ShoppingSequence(player, equipmentManager)

    private val botSightThreshold = 4000
    private val botTargetLevelRange = (40..81)

    val botGoal = ReachLevelGoal(player, targetLevel = botTargetLevelRange.random())

    private fun onAttackedEvent(onCreatureAttacked: OnCreatureAttacked) {
        if (onCreatureAttacked.target.equals(player)) {
            notifyEvent(CtrlEvent.EVT_ATTACKED, onCreatureAttacked.attacker)
        }
    }

    override fun run() {
        onEvtThink()
    }

    fun startAi() {
        if (aiTask == null || aiTask?.isCancelled == true || aiTask?.isDone == true) {
            aiTask = ThreadPoolManager.getInstance().scheduleBotAiAtFixedRate(this, 1000, 1000)
        }
        if (onAttackedListener == null) {
            onAttackedListener = player.addListener(
                ConsumerEventListener(
                    player,
                    EventType.ON_CREATURE_ATTACKED,
                    { event: IBaseEvent -> if (event is OnCreatureAttacked) onAttackedEvent(event) },
                    this
                )
            )
        }
    }

    fun stopAi() {
        aiTask?.cancel(true)
        onAttackedListener?.unregisterMe()
        onAttackedListener = null
        stopAITask()
    }

    private val managementDelay = 4 * 60 * 1000

    val thinking = AtomicBoolean()

    @Volatile
    var pendingTask: ScheduledFuture<*>? = null

    @Volatile
    private var lastManagementTime = 0L

    @Volatile
    private var lastTimeSinceSeenMobs = 0L

    private val noMobsLocationThreshold = 3 * 60 * 1000

    @Volatile
    var botState = BotState.IDLE

    @Volatile
    private var communityTeleportToState: CommunityTeleportToState? = null

    fun scheduleThink(delay: Long = 50) {
        if (pendingTask == null || pendingTask?.isDone == true || pendingTask?.isCancelled == true) {
            pendingTask = ThreadPoolManager.getInstance().scheduleAi(this, delay)
        }
    }

    override fun onEvtThink() {
        botTickTimer.record { think() }
    }

    fun think() {
        try {
            if (!player.isOnline) {
                LOG.debug("Player $player is offline, stopping AI task")
                stopAi()
                return
            }
            if (thinking.get()) {
                scheduleThink()
                return
            }
            thinking.set(true)

            if (!CanAct.check(player)) {
                return
            }

            if (stuckMonitoring.check()) {
                botState = BotState.STUCK
                return
            }

            if (resurrectionHelper.isDead()) {
                if (resurrectionHelper.checkAndResurrect()) {
                    farmZoneManager.forgetCurrent()
                }
                return
            }

            val classChanged = player.currentClassId != lastKnownClassId
            if (classChanged) {
                lastKnownClassId = player.currentClassId
                updateClassSkillAndEquipmentBehaviours()
            }

            val isBeingAttacked = attackers.isNotEmpty()

            if (currentTargetLost() && isBeingAttacked) {
                val attackersIterator = attackers.iterator()
                while (attackersIterator.hasNext()) {
                    val attacker = attackersIterator.next()
                    attackersIterator.remove()
                    botState = BotState.RESPONDING_TO_ATTACKERS
                    setTargets(attacker)
                    break
                }
            }

            val pk = targetFinder.scanForPk()
            if (currentTargetLost() && !isBeingAttacked && !player.isAttackingNow && pk.isNotEmpty()) {
                setTargets(pk.first())
            }

            equipmentManagementState.manageEquipment()

            if (botState == BotState.RESTING || player.isSitting) {
                val rested = player.currentHpPercentage > 70 && player.currentMpPercentage > 70
                if (rested) {
                    botState = BotState.THINKING
                    if (player.isSitting) {
                        player.standUp()
                    }
                }
            }

            if (isNotFighting(currentTargetLost(), isBeingAttacked)) {
                val hasToMoveToDrop = PickupHelper.pickup(player, this)
                if (hasToMoveToDrop) {
                    return
                }

                val needsToManage = (System.currentTimeMillis() - lastManagementTime) > managementDelay
                val needsToBuff = buffHelper.needsBuff()
                val minimumFreeSlots = 30L
                val needsToFreeUpInventory = !player.inventory.validateCapacity(minimumFreeSlots)
                val needsToRest = player.currentHpPercentage < 30 || player.currentMpPercentage < 30

                if (player.isInCombat && (needsToManage || needsToBuff || needsToFreeUpInventory || needsToRest)) {
                    // Walk around and wait to get out of combat
                    if (walkAroundState.walkAround(150)) {
                        botState = BotState.ROAMING
                    }
                    // TODO At each return, spawn an async EVT think to keep the bot brains working
                    return
                }

                if (!player.isInCombat && botState != BotState.NO_MONEY) {
                    lastManagementTime = System.currentTimeMillis()
                    botState = BotState.MANAGING

                    ClassChangingHelper.checkAndChange(classBehaviour.desiredNextClassIds, player)

                    if (buffHelper.checkAndBuff()) {
                        botState = BotState.BUFFING
                        return
                    }

                    if (KamaelChangeWeaponHelper.changeWeapon(player)) {
                        return
                    }

                    if (needsToRest) {
                        if (!player.isSitting) {
                            player.sitDown()
                        }
                        botState = BotState.RESTING
                        return
                    }

                    val shoppingComplete = shoppingSequence.useShop() // Need to throttle shop board usage, because anti-flood bans bots
                    if (!shoppingComplete) {
                        return // Return to continue shopping
                    } else {
                        shoppingSequence.reset() // For next use, and continue
                    }
                }
            }

            if (currentTargetLost()) {
                botState = BotState.SEARCHING_FOR_TARGET

                val visibleTargets =
                    targetFinder.scan()
                        .take(3)
                        .filter { GeoData.getInstance().canMove(player, it) }

                if (visibleTargets.isEmpty()) {
                    if (walkAroundState.walkAround(800)) {
                        botState = BotState.ROAMING
                    }

                    if (isInTown()) {
                        teleportToFarmZone(farmZoneManager.findClosestForLevel(player))
                        return
                    } else {
                        val lastTimeSeenMobsDelta = System.currentTimeMillis() - lastTimeSinceSeenMobs
                        if (lastTimeSeenMobsDelta > noMobsLocationThreshold) {
                            teleportToFarmZone(farmZoneManager.findClosestForLevel(player))
                            return
                        } else {
                            val higherLevelFarmZone = farmZoneManager.findNewZoneForNewLevels(player)
                            if (higherLevelFarmZone != null) {
                                teleportToFarmZone(higherLevelFarmZone)
                                return
                            }
                        }
                    }
                    return
                } else {
                    lastTimeSinceSeenMobs = System.currentTimeMillis()
                    val playerTarget = visibleTargets.firstOrNull { it.isPlayer }
                    val newTarget = playerTarget ?: visibleTargets.random()
                    setTargets(newTarget)
                    aggressiveIntention(newTarget)
                    return
                }
            }

            usePotionsIfNeeded()
            thinkAttack()
        } catch (e: Exception) {
            logger.warn("onEvtThink(): {} for {} failed!", intention, player, e)
        } finally {
            thinking.set(false)
        }
    }

    fun aggressiveIntention(target: L2Character) {
        if (target.isAutoAttackable(player) && !target.isAlikeDead) {
            if (GeoData.getInstance().canSeeTarget(player, target)) {
                player.ai.setIntention(CtrlIntention.AI_INTENTION_ATTACK, target)
            } else {
                val destination = GeoData.getInstance().moveCheck(player, target)
                player.ai.setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, destination)
            }
        }
    }

    fun teleportToFarmZone(farmZone: FarmZone?) {
        val couldPay = teleportIfPossible(farmZone)
        if (!couldPay) {
            botState = BotState.NO_MONEY
        } else {
            lastTimeSinceSeenMobs = System.currentTimeMillis()
        }
    }

    fun isInTown(): Boolean = TownManager.getTown(player.x, player.y, player.z) != null

    // isInCombat - checks the combat timer as well
    fun isNotFighting(targetLost: Boolean = currentTargetLost(), isBeingAttacked: Boolean = attackers.isNotEmpty()): Boolean {
        return !player.isAttackingNow && targetLost && !isBeingAttacked
    }

    private fun teleportIfPossible(farmZone: FarmZone?): Boolean {
        if (CommunityPaymentHelper.checkFreeService(player) || player.adena > Config.COMMUNITY_TELEPORT_PRICE) {
            if (farmZone != null) {
                communityTeleportToState = CommunityTeleportToState(player, farmZone)
                communityTeleportToState?.let {
                    val teleportInitiated = it.teleportTo()
                    if (teleportInitiated) {
                        farmZoneManager.setCurrent(farmZone)
                        botState = BotState.TELEPORTING
                    } else {
                        communityTeleportToState = null
                    }
                }
            }
            return true
        }
        return false
    }

    private fun updateClassSkillAndEquipmentBehaviours() {
        classBehaviour = classBehaviourTable.findBehaviour(
            ClassListData.getInstance().getClass(lastKnownClassId).classId
        )
        aiSkillHelper = AiSkillHelper(player, classBehaviour.skillPreferences)
        equipmentPreferences = equipmentTable.findPreference(classBehaviour.equipmentGroup)
        equipmentManager.equipmentPreferences = equipmentPreferences
    }

    override fun onEvtAttacked(attacker: L2Character?) {
        if (attacker == null) {
            return
        }
        if (!attacker.isInvisible && attacker != aiTarget && attacker != player) {
            attackers.add(attacker)
        }
        super.onEvtAttacked(attacker)
    }

    private fun currentTargetLost(): Boolean {
        if (aiTarget == null ||
            aiTarget!!.isDead ||
            aiTarget!!.currentHp <= 0 ||
            aiTarget!!.isInvisible ||
            Util.calculateDistance(player, aiTarget, true, false) > botSightThreshold ||
            !player.knownList.knowsObject(aiTarget) ||
            targetLineOfSightFailures > 5
        ) {
            clearTargets()
            targetLineOfSightFailures = 0
            return true
        }
        return false
    }

    @Volatile
    private var targetLineOfSightFailures = 0

    private fun maybeCast(playerSkill: Skill): Boolean {
        if (currentTargetLost()) {
            player.setIsCastingNow(false)
            return false
        }

        if (!canCast(playerSkill)) {
            player.setIsCastingNow(false)
            return false
        }

        if (castTarget == player) {
            targetLineOfSightFailures = 0
            cast(playerSkill)
            return true
        }

        if (maybeMoveToPawn(aiTarget, player.getMagicalAttackRange(playerSkill))) {
            botState = BotState.MOVING_TO_TARGET
            player.setIsCastingNow(false)
            if (!player.isMoving) {
                targetLineOfSightFailures += 1
            }
            return false
        }

        if (!playerSkill.isSimultaneousCast) {
            clientStopMoving()
            stopFollow()
        }

        targetLineOfSightFailures = 0
        cast(playerSkill)
        return true
    }

    // WARNING Have to set the 'target' field in AI before using this method
    private fun canCast(skill: Skill) =
        player.checkUseMagicConditions(skill, true, false) && player.checkDoCastConditions(skill)

    private fun cast(skill: Skill) {
        player.setCurrentSkill(_skill, true, false)
        player.doCast(skill)
        val castTime = Formulas.calcCastTime(player, skill)
        scheduleThink(castTime.toLong())
    }

    private fun usePotionsIfNeeded() {
        if (player.currentHpPercentage < 70) {
            player.inventory
                .getFirstOrNull(IdTable.whiteGreaterHealingPotionId)
                ?.let {
                    val useItem = UseItem(it.objectId, it.itemId)
                    useItem.client = player.client
                    useItem.run()
                }
        }
        if (player.currentMpPercentage < 50) {
            player.inventory
                .getAllItemsByItemId(IdTable.manaDrugId)
                .firstOrNull()
                ?.let {
                    val useItem = UseItem(it.objectId, it.itemId)
                    useItem.client = player.client
                    useItem.run()
                }
        }
    }

    @Volatile
    var aiTarget: L2Character? = null

    fun setTargets(botTarget: L2Character) {
        aiTarget = botTarget
        target = botTarget
        attackTarget = botTarget
        castTarget = botTarget
        player.target = botTarget
    }

    fun clearTargets() {
        aiTarget = null
        target = null
        attackTarget = null
        castTarget = null
        player.target = null
        stopFollow()
    }

    private fun thinkAttack() {
        val appropriateSkillOption = aiSkillHelper.findAppropriateSkill()
        if (appropriateSkillOption.isPresent) {
            val appropriateSkill = appropriateSkillOption.get()
            val skillParameter = appropriateSkill.left
            val playerSkill = appropriateSkill.right

            val randomSuccess = skillParameter.useFrequency > Rnd.get(100)
            if (randomSuccess && canCast(playerSkill)) {
                botState = BotState.CASTING
                val couldCast = maybeCast(playerSkill)
                if (couldCast) {
                    return
                }
            }
        }

        if (maybeMoveToPawn(aiTarget, player.physicalAttackRange)) {
            botState = BotState.MOVING_TO_TARGET
            if (!player.isMoving) {
                targetLineOfSightFailures += 1
            }
            return
        }

        targetLineOfSightFailures = 0
        botState = BotState.ATTACKING
        clientStopMoving()
        player.doAttack(aiTarget)
    }
}
