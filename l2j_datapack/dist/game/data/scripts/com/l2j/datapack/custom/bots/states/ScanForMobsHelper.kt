package com.l2j.datapack.custom.bots.states

import com.l2jserver.gameserver.model.actor.L2Character
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.util.Util

class TargetFinder(val player: L2PcInstance) {

    private fun searchRadius(): Int = 3000
    private fun pkSearchRadius(): Int = 4000

    fun scan(scanRadius: Int = searchRadius()): List<L2Character> {
        val preferredTargets = player.knownList
            .getKnownCharactersInRadius(scanRadius.toLong())
            .filter { isPreferredTarget(it) }
        return preferredTargets
            .sortedWith(compareBy({ Util.calculateDistance(player, it.location, true, false) }, { it.isPlayer }))
    }

    fun scanForPk(scanRadius: Int = pkSearchRadius()): List<L2Character> {
        val preferredTargets = player.knownList
            .getKnownCharactersInRadius(scanRadius.toLong())
            .filter { isPkTarget(it) && it.isAutoAttackable(player) }
        return preferredTargets
            .sortedBy { Util.calculateDistance(player, it.location, true, false) }
    }

    fun isPkTarget(target: L2Character): Boolean {
        if (target.currentHp <= 0 || target.isDead) {
            return false
        }
        if (target.isInvisible) {
            return false
        }
        return target.isPlayer && (target as L2PcInstance).karma > 0
    }

    fun isPreferredTarget(target: L2Character): Boolean {
        if (target.currentHp <= 0 || target.isDead) {
            return false
        }
        if (target.isInvisible) {
            return false
        }
        if (target.isRaid || target.isRaidMinion) {
            return false
        }
        if (target.isPlayer && (target as L2PcInstance).karma > 0) {
            return true
        }
        val levelDiff = target.level - player.level
        return target.isMonster && levelDiff < 9 && levelDiff > -10
    }

}