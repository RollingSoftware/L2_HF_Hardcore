package com.l2j.datapack.custom.bots.manager

import java.time.Duration

data class BotManagerConfig(
    val botsPerRun: Int = 1,
    @Volatile
    var targetBotCount: Int = -1,
    var botSchedule: List<BotSchedule> = BotScheduleHelper.default,
    val runInterval: Duration = Duration.ofSeconds(2)
)