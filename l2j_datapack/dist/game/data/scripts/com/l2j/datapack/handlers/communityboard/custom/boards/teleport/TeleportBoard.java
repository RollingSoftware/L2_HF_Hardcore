package com.l2j.datapack.handlers.communityboard.custom.boards.teleport;

import com.l2jserver.Config;
import com.l2jserver.common.DecimalFormatStandard;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2j.datapack.handlers.communityboard.custom.*;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TeleportBoard implements IParseBoardHandler {

    private static final Logger LOG = LogManager.getLogger();

    public static boolean handleTeleport(L2PcInstance player, String teleportDestinationCode) {
        Optional<TeleportDestination> teleportDestination = BoardTeleportTable.getInstance().findByCode(teleportDestinationCode);
        if (teleportDestination.isEmpty()) {
            LOG.warn("Player {} requested teleport to unknown location '{}'", player, teleportDestinationCode);
            return false;
        }

        ProcessResult result = BoardCondition.checkCondition(player);
        if (result.isFailure()) {
            NotificationHelper.notify(player, result.getResult());
            return false;
        }

        if (!Config.ALT_GAME_KARMA_PLAYER_CAN_USE_GK && player.getKarma() > 0) {
            NotificationHelper.notify(player, player.string("karma_players_cannot_use_gk"));
            return false;
        }

        ProcessResult payment = CommunityPaymentHelper.payForService(player, Config.COMMUNITY_TELEPORT_PRICE);
        if (payment.isFailure()) {
            return false;
        }

        player.fakeCast(
                SkillHolder.SOE,
                Config.COMMUNITY_TELEPORT_DELAY,
                () -> player.teleToLocation(teleportDestination.get().getCoordinates(), true)
        );
        return true;
    }

    private boolean handleTeleportCommand(String command, L2PcInstance player) {
        Optional<ActionArgs> actionArgsOption = ActionArgs.parse(command);

        if (actionArgsOption.isEmpty()) {
            return false;
        }

        String teleportDestinationCode = actionArgsOption.get().getActionName();
        return handleTeleport(player, teleportDestinationCode);
    }

    @Override
    public boolean parseCommunityBoardCommand(String command, L2PcInstance player) {
        if (command.startsWith("_bbs_teleport_board")) {
            String menuTemplate = HtmCache.getInstance().getHtm(
                    player.getHtmlPrefix(),
                    CustomHomeBoard.HTML_ROOT + "/menu_template.html"
            );
            String teleportBoardHtml = HtmCache.getInstance().getHtm(
                    player.getHtmlPrefix(),
                    CustomHomeBoard.HTML_ROOT + "/teleport/teleport_template.html"
            );

            if (CommunityPaymentHelper.checkFreeService(player)) {
                String free = player.string("free");
                teleportBoardHtml = teleportBoardHtml.replace("%teleport_price%", free);
            } else {
                teleportBoardHtml = teleportBoardHtml.replace("%teleport_price%",
                        DecimalFormatStandard.moneyFormat().format(Config.COMMUNITY_TELEPORT_PRICE)
                );
            }

            CommunityBoardHandler.separateAndSend(menuTemplate.replace("%page_content%", teleportBoardHtml), player);
            return true;
        } else if (command.startsWith("_bbs_teleport")) {
            return handleTeleportCommand(command, player);
        }
        return false;
    }

    @Override
    public String[] getCommunityBoardCommands() {
        return new String[]{
                "_bbs_teleport",
                "_bbs_teleport_board"
        };
    }
}
