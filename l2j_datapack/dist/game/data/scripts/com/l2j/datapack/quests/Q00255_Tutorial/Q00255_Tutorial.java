package com.l2j.datapack.quests.Q00255_Tutorial;

import com.l2jserver.Config;
import com.l2jserver.common.util.Pair;
import com.l2jserver.common.util.StringUtil;
import com.l2jserver.gameserver.enums.audio.Sound;
import com.l2jserver.gameserver.enums.audio.Voice;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Q00255_Tutorial extends Quest {

    private static final String QUEST_NAME = "Q00255_Tutorial";

    private static final Map<Integer, Pair<Voice, String>> QUEST_TIMER_DATA = createQuestTimerData();
    private static final Map<Integer, StringWithCoords> CLIENT_EVENT_ENABLE = createClientEventEnableData();
    private static final Map<Integer, StringWithCoords> QUESTION_MARK_CLICKED = createQuestionMarkClickedData();
    private static final Map<Integer, String> QUESTION_MARK_CLICKED_NEWBIE = createQuestionMarkClickedNewbieData();
    private static final Map<Integer, String> QUESTION_MARK_CLICKED_FIRST_CLASS
            = createQuestionMarkClickedFirstClassData();
    private static final Map<Integer, String> QUESTION_MARK_CLICKED_SECOND_CLASS
            = createQuestionMarkClickedSecondClassData();
    private static final Map<Integer, String> CLOSE_LINK_SECOND_CLASS_23
            = createCloseLinkSecondClass23Data();
    private static final Map<Integer, String> CLOSE_LINK_SECOND_CLASS_24
            = createCloseLinkSecondClass24Data();
    // Npc
    private static final int ROIEN = 30008;
    private static final int NEWBIE_HELPER_HUMAN_FIGHTER = 30009;
    private static final int GALLINT = 30017;
    private static final int NEWBIE_HELPER_HUMAN_MAGE = 30019;
    private static final int MITRAELL = 30129;
    private static final int NEWBIE_HELPER_DARK_ELF = 30131;
    private static final int NERUPA = 30370;
    private static final int NEWBIE_HELPER_ELF = 30400;
    private static final int LAFERON = 30528;
    private static final int NEWBIE_HELPER_DWARF = 30530;
    private static final int VULKUS = 30573;
    private static final int NEWBIE_HELPER_ORC = 30575;
    private static final int PERWAN = 32133;
    private static final int NEWBIE_HELPER_KAMAEL = 32134;
    // Monster
    private static final int TUTORIAL_GREMLIN = 18342;
    public Q00255_Tutorial() {
        super(255, QUEST_NAME, "Tutorial");
    }

    private static Map<Integer, Pair<Voice, String>> createQuestTimerData() {
        var map = new HashMap<Integer, Pair<Voice, String>>();
        map.put(0, Pair.of(Voice.TUTORIAL_VOICE_001A_2000, "tutorial_human_fighter001.htm"));
        map.put(10, Pair.of(Voice.TUTORIAL_VOICE_001B_2000, "tutorial_human_mage001.htm"));
        map.put(18, Pair.of(Voice.TUTORIAL_VOICE_001C_2000, "tutorial_elven_fighter001.htm"));
        map.put(25, Pair.of(Voice.TUTORIAL_VOICE_001D_2000, "tutorial_elven_mage001.htm"));
        map.put(31, Pair.of(Voice.TUTORIAL_VOICE_001E_2000, "tutorial_delf_fighter001.htm"));
        map.put(38, Pair.of(Voice.TUTORIAL_VOICE_001F_2000, "tutorial_delf_mage001.htm"));
        map.put(44, Pair.of(Voice.TUTORIAL_VOICE_001G_2000, "tutorial_orc_fighter001.htm"));
        map.put(49, Pair.of(Voice.TUTORIAL_VOICE_001H_2000, "tutorial_orc_mage001.htm"));
        map.put(53, Pair.of(Voice.TUTORIAL_VOICE_001I_2000, "tutorial_dwarven_fighter001.htm"));
        map.put(123, Pair.of(Voice.TUTORIAL_VOICE_001K_2000, "tutorial_kamael_male001.htm"));
        map.put(124, Pair.of(Voice.TUTORIAL_VOICE_001K_2000, "tutorial_kamael_female001.htm"));
        return map;
    }

    private static Map<Integer, StringWithCoords> createClientEventEnableData() {
        var map = new HashMap<Integer, StringWithCoords>();
        map.put(0, new StringWithCoords("tutorial_human_fighter007.htm", -71424, 258336, -3109));
        map.put(10, new StringWithCoords("tutorial_human_mage007.htm", -91036, 248044, -3568));
        map.put(18, new StringWithCoords("tutorial_elf007.htm", 46112, 41200, -3504));
        map.put(25, new StringWithCoords("tutorial_elf007.htm", 46112, 41200, -3504));
        map.put(31, new StringWithCoords("tutorial_delf007.htm", 28384, 11056, -4233));
        map.put(38, new StringWithCoords("tutorial_delf007.htm", 28384, 11056, -4233));
        map.put(44, new StringWithCoords("tutorial_orc007.htm", -56736, -113680, -672));
        map.put(49, new StringWithCoords("tutorial_orc007.htm", -56736, -113680, -672));
        map.put(53, new StringWithCoords("tutorial_dwarven_fighter007.htm", 108567, -173994, -406));
        map.put(123, new StringWithCoords("tutorial_kamael007.htm", -125872, 38016, 1251));
        map.put(124, new StringWithCoords("tutorial_kamael007.htm", -125872, 38016, 1251));
        return map;
    }

    private static Map<Integer, StringWithCoords> createQuestionMarkClickedData() {
        var map = new HashMap<Integer, StringWithCoords>();
        map.put(0, new StringWithCoords("tutorial_fighter017.htm", -83165, 242711, -3720));
        map.put(10, new StringWithCoords("tutorial_mage017.htm", -85247, 244718, -3720));
        map.put(18, new StringWithCoords("tutorial_fighter017.htm", 45610, 52206, -2792));
        map.put(25, new StringWithCoords("tutorial_mage017.htm", 45610, 52206, -2792));
        map.put(31, new StringWithCoords("tutorial_fighter017.htm", 10344, 14445, -4242));
        map.put(38, new StringWithCoords("tutorial_mage017.htm", 10344, 14445, -4242));
        map.put(44, new StringWithCoords("tutorial_fighter017.htm", -46324, -114384, -200));
        map.put(49, new StringWithCoords("tutorial_fighter017.htm", -46305, -112763, -200));
        map.put(53, new StringWithCoords("tutorial_fighter017.htm", 115447, -182672, -1440));
        map.put(123, new StringWithCoords("tutorial_fighter017.htm", -118132, 42788, 723));
        map.put(124, new StringWithCoords("tutorial_fighter017.htm", -118132, 42788, 723));
        return map;
    }

    private static Map<Integer, String> createQuestionMarkClickedNewbieData() {
        var map = new HashMap<Integer, String>();
        map.put(0, "tutorial_human009.htm");
        map.put(10, "tutorial_human009.htm");
        map.put(18, "tutorial_elf009.htm");
        map.put(25, "tutorial_elf009.htm");
        map.put(31, "tutorial_delf009.htm");
        map.put(38, "tutorial_delf009.htm");
        map.put(44, "tutorial_orc009.htm");
        map.put(49, "tutorial_orc009.htm");
        map.put(53, "tutorial_dwarven009.htm");
        map.put(123, "tutorial_kamael009.htm");
        map.put(124, "tutorial_kamael009.htm");
        return map;
    }

    private static Map<Integer, String> createQuestionMarkClickedFirstClassData() {
        var map = new HashMap<Integer, String>();
        map.put(0, "tutorial_21.htm");
        map.put(10, "tutorial_21a.htm");
        map.put(18, "tutorial_21b.htm");
        map.put(25, "tutorial_21c.htm");
        map.put(31, "tutorial_21g.htm");
        map.put(38, "tutorial_21h.htm");
        map.put(44, "tutorial_21d.htm");
        map.put(49, "tutorial_21e.htm");
        map.put(53, "tutorial_21f.htm");
        return map;
    }

    private static Map<Integer, String> createQuestionMarkClickedSecondClassData() {
        var map = new HashMap<Integer, String>();
        map.put(1, "tutorial_22w.htm");
        map.put(4, "tutorial_22.htm");
        map.put(7, "tutorial_22b.htm");
        map.put(11, "tutorial_22c.htm");
        map.put(15, "tutorial_22d.htm");
        map.put(19, "tutorial_22e.htm");
        map.put(22, "tutorial_22f.htm");
        map.put(26, "tutorial_22g.htm");
        map.put(29, "tutorial_22h.htm");
        map.put(32, "tutorial_22n.htm");
        map.put(35, "tutorial_22o.htm");
        map.put(39, "tutorial_22p.htm");
        map.put(42, "tutorial_22q.htm");
        map.put(45, "tutorial_22i.htm");
        map.put(47, "tutorial_22j.htm");
        map.put(50, "tutorial_22k.htm");
        map.put(54, "tutorial_22l.htm");
        map.put(56, "tutorial_22m.htm");
        return map;
    }

    private static Map<Integer, String> createCloseLinkSecondClass23Data() {
        var map = new HashMap<Integer, String>();
        map.put(4, "tutorial_22aa.htm");
        map.put(7, "tutorial_22ba.htm");
        map.put(11, "tutorial_22ca.htm");
        map.put(15, "tutorial_22da.htm");
        map.put(19, "tutorial_22ea.htm");
        map.put(22, "tutorial_22fa.htm");
        map.put(26, "tutorial_22ga.htm");
        map.put(32, "tutorial_22na.htm");
        map.put(35, "tutorial_22oa.htm");
        map.put(39, "tutorial_22pa.htm");
        map.put(50, "tutorial_22ka.htm");
        return map;
    }

    private static Map<Integer, String> createCloseLinkSecondClass24Data() {
        var map = new HashMap<Integer, String>();
        map.put(4, "tutorial_22ab.htm");
        map.put(7, "tutorial_22bb.htm");
        map.put(11, "tutorial_22cb.htm");
        map.put(15, "tutorial_22db.htm");
        map.put(19, "tutorial_22eb.htm");
        map.put(22, "tutorial_22fb.htm");
        map.put(26, "tutorial_22gb.htm");
        map.put(32, "tutorial_22nb.htm");
        map.put(35, "tutorial_22ob.htm");
        map.put(39, "tutorial_22pb.htm");
        map.put(50, "tutorial_22kb.htm");
        return map;
    }

    public String onUserConnected(L2PcInstance player, QuestState questState) {
        var playerLevel = player.getLevel();
        if (playerLevel < 6 && questState.getInt("onlyone") == 0) {
            var uc = questState.getInt("ucMemo");
            switch (uc) {
                case 0 -> {
                    questState.set("ucMemo", "0");
                    questState.startQuestTimer("QT", 10000);
                    questState.set("ucMemo", "0");
                    questState.set("Ex", "-2");
                }
                case 1 -> {
                    questState.showQuestionMark(1);
                    questState.playSound(Voice.TUTORIAL_VOICE_006_1000);
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                }
                case 2 -> {
                    if (questState.getInt("Ex") == 2) {
                        questState.showQuestionMark(3);
                        questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    }
                    if (questState.getQuestItemsCount(6353) == 1) {
                        questState.showQuestionMark(5);
                        questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    }
                }
                case 3 -> {
                    questState.showQuestionMark(12);
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.onTutorialClientEvent(0);
                }
            }
        } else if (playerLevel == 18 && player.getQuestState("Q10276_MutatedKaneusGludio") == null) {
            questState.showQuestionMark(33);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
        } else if (playerLevel == 28 && player.getQuestState("Q10277_MutatedKaneusDion") == null) {
            questState.showQuestionMark(33);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
        } else if (playerLevel == 28 && player.getQuestState("Q10278_MutatedKaneusHeine") == null) {
            questState.showQuestionMark(33);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
        } else if (playerLevel == 28 && player.getQuestState("Q10279_MutatedKaneusOren") == null) {
            questState.showQuestionMark(33);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
        } else if (playerLevel == 28 && player.getQuestState("Q10280_MutatedKaneusSchuttgart") == null) {
            questState.showQuestionMark(33);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
        } else if (playerLevel == 28 && player.getQuestState("Q10281_MutatedKaneusRune") == null) {
            questState.showQuestionMark(33);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
        } else if (playerLevel == 79 && player.getQuestState("Q00192_SevenSignSeriesOfDoubt") == null) {
            questState.showQuestionMark(33);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
        }
        return null;
    }

    public String onQuestTimer(L2PcInstance player, QuestState questState) {
        var ex = questState.getInt("Ex");
        var classId = questState.getPlayer().getClassId().getId();

        switch (ex) {
            case -2 -> {
                var questTimerData = QUEST_TIMER_DATA.get(classId);
                if (questTimerData != null) {
                    questState.playSound(questTimerData.getLeft());
                }
                if (player.isOnline() && questState.getQuestItemsCount(5588) == 0) {
                    questState.giveItems(5588, 1);
                    questState.startQuestTimer("QT", 30000);
                    questState.set("Ex", "-3");
                }
            }
            case -3 -> {
                questState.playSound(Voice.TUTORIAL_VOICE_002_1000);
                questState.set("Ex", "0");
            }
            case -4 -> {
                questState.playSound(Voice.TUTORIAL_VOICE_008_1000);
                questState.set("Ex", "-5");
            }
        }
        return null;
    }

    public String onTutorialEnd(String event, QuestState questState) {
        var eventIdString = event.substring(0, 2);
        var classId = questState.getPlayer().getClassId().getId();

        if (!StringUtil.isInteger(eventIdString)) {
            return StringUtil.EMPTY;
        }

        var eventId = Integer.parseInt(eventIdString);

        if (eventId == 0) {
            questState.closeTutorialHtml();
        } else if (eventId == 1) {
            questState.closeTutorialHtml();
            questState.playSound(Voice.TUTORIAL_VOICE_006_1000);
            questState.showQuestionMark(1);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
            questState.startQuestTimer("QT", 30000);
            questState.set("Ex", "-4");
        } else if (eventId == 2) {
            questState.playSound(Voice.TUTORIAL_VOICE_003_2000);
            questState.onTutorialClientEvent(1);
            questState.set("Ex", "-5");
            return "tutorial_02.htm";
        } else if (eventId == 3) {
            questState.onTutorialClientEvent(2);
            return "tutorial_03.htm";
        } else if (eventId == 5) {
            questState.onTutorialClientEvent(8);
            return "tutorial_05.htm";
        } else if (eventId == 7) {
            questState.onTutorialClientEvent(0);
            return "tutorial_100.htm";
        } else if (eventId == 8) {
            questState.onTutorialClientEvent(0);
            return "tutorial_101.htm";
        } else if (eventId == 10) {
            questState.onTutorialClientEvent(0);
            return "tutorial_103.htm";
        } else if (eventId == 12) {
            questState.closeTutorialHtml();
        } else if (eventId == 23) {
            return CLOSE_LINK_SECOND_CLASS_23.getOrDefault(classId, StringUtil.EMPTY);
        } else if (eventId == 24) {
            return CLOSE_LINK_SECOND_CLASS_24.getOrDefault(classId, StringUtil.EMPTY);
        } else if (eventId == 25) {
            return "tutorial_22cc.htm";
        } else if (eventId == 26) {
            return QUESTION_MARK_CLICKED_SECOND_CLASS.getOrDefault(classId, StringUtil.EMPTY);
        } else if (eventId == 27) {
            return "tutorial_29.htm";
        } else if (eventId == 28) {
            return "tutorial_28.htm";
        }
        return StringUtil.EMPTY;
    }

    public String onClientEventEnable(String event, L2PcInstance player, QuestState questState) {
        var eventIdString = event.substring(0, 2);
        var classId = questState.getPlayer().getClassId().getId();

        if (!StringUtil.isInteger(eventIdString)) {
            return StringUtil.EMPTY;
        }

        var eventId = Integer.parseInt(eventIdString);
        var playerLevel = player.getLevel();

        if (eventId == 1 && playerLevel < 6) {
            questState.playSound(Voice.TUTORIAL_VOICE_004_5000);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
            questState.onTutorialClientEvent(2);
            return "tutorial_03.htm";
        } else if (eventId == 2 && playerLevel < 6) {
            questState.playSound(Voice.TUTORIAL_VOICE_005_5000);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
            questState.onTutorialClientEvent(8);
            return "tutorial_05.htm";
        } else if (eventId == 8 && playerLevel < 6) {
            var data = CLIENT_EVENT_ENABLE.get(classId);
            if (data != null) {
                questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                questState.addRadar(data.x, data.y, data.z);
                questState.playSound(Voice.TUTORIAL_VOICE_007_3500);
                questState.set("ucMemo", "1");
                questState.set("Ex", "-5");
                return data.name;
            }
        } else if (eventId == 30 && playerLevel < 10 && questState.getInt("Die") == 0) {
            questState.playSound(Voice.TUTORIAL_VOICE_016_1000);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
            questState.set("Die", "1");
            questState.showQuestionMark(8);
            questState.onTutorialClientEvent(0);
        } else if (eventId == 800000 && playerLevel < 6 && questState.getInt("sit") == 0) {
            questState.playSound(Voice.TUTORIAL_VOICE_018_1000);
            questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
            questState.set("sit", "1");
            questState.onTutorialClientEvent(0);
            return "tutorial_21z.htm";
        } else if (eventId == 40) {
            if (playerLevel == 5 && player.getClassId().level() == 0) {
                if (questState.getInt("lvl") < 5) {
                    if (!player.getClassId().isMage() || classId == 49) {
                        questState.playSound(Voice.TUTORIAL_VOICE_014_1000);
                        questState.showQuestionMark(9);
                        questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                        questState.set("lvl", "5");
                    }
                }
            } else if (playerLevel == 6 && questState.getInt("lvl") < 6 && player.getClassId().level() == 0) {
                questState.playSound(Voice.TUTORIAL_VOICE_020_1000);
                questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                questState.showQuestionMark(24);
                questState.set("lvl", "6");
            } else if (playerLevel == 7 && player.getClassId().isMage() && classId != 49) {
                if (questState.getInt("lvl") < 7 && player.getClassId().level() == 0) {
                    questState.playSound(Voice.TUTORIAL_VOICE_019_1000);
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.set("lvl", "7");
                    questState.showQuestionMark(11);
                }
            } else if (playerLevel == 10 && questState.getInt("lvl") < 10) {
                questState.playSound(Voice.TUTORIAL_VOICE_030_1000);
                questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                questState.set("lvl", "10");
                questState.showQuestionMark(27);
            } else if (playerLevel == 15 && questState.getInt("lvl") < 15) {
                questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                questState.set("lvl", "15");
                questState.showQuestionMark(17);
            } else if (playerLevel == 18 && questState.getInt("lvl") < 18) {
                questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                questState.set("lvl", "18");
                questState.showQuestionMark(33);
            } else if (playerLevel == 19 && questState.getInt("lvl") < 19) {
                var race = questState.getPlayer().getRace().ordinal();
                if (race != 5
                        && player.getClassId().level() == 0
                        && List.of(0, 10, 18, 25, 31, 38, 44, 49, 52).contains(classId)
                ) {
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.set("lvl", "19");
                    questState.showQuestionMark(35);
                }
            } else if (playerLevel == 28 && questState.getInt("lvl") < 28) {
                questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                questState.set("lvl", "28");
                questState.showQuestionMark(33);
            } else if (playerLevel == 35) {
                if (questState.getInt("lvl") < 35) {
                    var race = questState.getPlayer().getRace().ordinal();
                    if (race != 5 &&
                            player.getClassId().level() == 1 &&
                            List.of(1, 4, 7, 11, 15, 19, 22, 26, 29, 32, 35, 39, 42, 45, 47, 50, 54, 56)
                                    .contains(classId)
                    ) {
                        questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                        questState.set("lvl", "35");
                        questState.showQuestionMark(34);
                    }
                }
            } else if (playerLevel == 38) {
                if (questState.getInt("lvl") < 38) {
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.set("lvl", "38");
                    questState.showQuestionMark(33);
                }
            } else if (playerLevel == 48) {
                if (questState.getInt("lvl") < 48) {
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.set("lvl", "48");
                    questState.showQuestionMark(33);
                }
            } else if (playerLevel == 58) {
                if (questState.getInt("lvl") < 58) {
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.set("lvl", "58");
                    questState.showQuestionMark(33);
                }
            } else if (playerLevel == 68) {
                if (questState.getInt("lvl") < 68) {
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.set("lvl", "68");
                    questState.showQuestionMark(33);
                }
            } else if (playerLevel == 79) {
                if (questState.getInt("lvl") < 79) {
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.set("lvl", "79");
                    questState.showQuestionMark(33);
                }
            }
        } else if (eventId == 45) {
            if (playerLevel < 10) {
                if (questState.getInt("HP") == 0) {
                    questState.playSound(Voice.TUTORIAL_VOICE_017_1000);
                    questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                    questState.set("HP", "1");
                    questState.showQuestionMark(10);
                    questState.onTutorialClientEvent(800000);
                }
            }
        } else if (eventId == 57) {
            if (playerLevel < 6 && questState.getInt("Adena") == 0) {
                questState.playSound(Voice.TUTORIAL_VOICE_012_1000);
                questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                questState.set("Adena", "1");
                questState.showQuestionMark(23);
            }
        } else if (eventId == 6353) {
            if (playerLevel < 6 && questState.getInt("Gemstone") == 0) {
                questState.playSound(Voice.TUTORIAL_VOICE_013_1000);
                questState.playSound(Sound.ITEMSOUND_QUEST_MIDDLE);
                questState.set("Gemstone", "1");
                questState.showQuestionMark(5);
            }
        }
        return StringUtil.EMPTY;
    }

    public String onQuestionMarkClicked(String event, L2PcInstance player, QuestState questState) {
        var eventIdString = event.substring(0, 2);
        var classId = questState.getPlayer().getClassId().getId();

        if (!StringUtil.isInteger(eventIdString)) {
            return StringUtil.EMPTY;
        }

        var MarkId = Integer.parseInt(eventIdString);

        if (MarkId == 1) {
            questState.playSound(Voice.TUTORIAL_VOICE_007_3500);
            questState.set("Ex", "-5");
            var data = CLIENT_EVENT_ENABLE.get(classId);
            if (data != null) {
                questState.addRadar(data.x, data.y, data.z);
                return data.name;
            }
        } else if (MarkId == 3) {
            return "tutorial_09.htm";
        } else if (MarkId == 5) {
            var data = CLIENT_EVENT_ENABLE.get(classId);
            if (data != null) {
                questState.addRadar(data.x, data.y, data.z);
                return "tutorial_11.htm";
            }
        } else if (MarkId == 7) {
            questState.set("ucMemo", "3");
            return "tutorial_15.htm";
        } else if (MarkId == 8) {
            return "tutorial_18.htm";
        } else if (MarkId == 9) {
            var data = QUESTION_MARK_CLICKED.get(classId);
            if (data != null) {
                questState.addRadar(data.x, data.y, data.z);
                return data.name;
            }
        } else if (MarkId == 10) {
            return "tutorial_19.htm";
        } else if (MarkId == 11) {
            var data = QUESTION_MARK_CLICKED.get(classId);
            if (data != null) {
                questState.addRadar(data.x, data.y, data.z);
                return data.name;
            }
        } else if (MarkId == 12) {
            questState.set("ucMemo", "4");
            return "tutorial_15.htm";
        } else if (MarkId == 13) {
            return "tutorial_30.htm";
        } else if (MarkId == 17) {
            return "tutorial_27.htm";
        } else if (MarkId == 23) {
            return "tutorial_24.htm";
        } else if (MarkId == 24) {
            return QUESTION_MARK_CLICKED_NEWBIE.get(classId);
        } else if (MarkId == 26) {
            if (questState.getPlayer().getClassId().isMage() && classId != 49) {
                return "tutorial_newbie004b.htm";
            } else {
                return "tutorial_newbie004a.htm";
            }
        } else if (MarkId == 27) {
            return "tutorial_20.htm";
        } else if (MarkId == 33) {
            var lvl = player.getLevel();
            if (lvl == 18) {
                return "tutorial_kama_18.htm";
            } else if (lvl == 28) {
                return "tutorial_kama_28.htm";
            } else if (lvl == 38) {
                return "tutorial_kama_38.htm";
            } else if (lvl == 48) {
                return "tutorial_kama_48.htm";
            } else if (lvl == 58) {
                return "tutorial_kama_58.htm";
            } else if (lvl == 68) {
                return "tutorial_kama_68.htm";
            } else if (lvl == 79) {
                return "tutorial_epic_quequestState.htm";
            }
        } else if (MarkId == 34) {
            return "tutorial_28.htm";
        } else if (MarkId == 35) {
            return QUESTION_MARK_CLICKED_FIRST_CLASS.get(classId);
        }
        return StringUtil.EMPTY;
    }

    @Override
    public String onAdvEvent(String event, L2Npc npc, L2PcInstance player) {
        if (Config.DISABLE_TUTORIAL) {
            return null;
        }

        QuestState questState = getQuestState(player, true);
        var eventCode = event.substring(0, 2);

        var htmlText = switch (eventCode) {
            case "UC" -> onUserConnected(player, questState);
            case "QT" -> onQuestTimer(player, questState);
            case "TE" -> onTutorialEnd(event, questState);
            case "CE" -> onClientEventEnable(event, player, questState);
            case "QM" -> onQuestionMarkClicked(event, player, questState);
            default -> "";
        };

        if (!StringUtil.isBlank(htmlText)) {
            questState.showTutorialHTML(htmlText);
        }
        return null;
    }

    static class StringWithCoords {
        String name;
        int x;
        int y;
        int z;

        public StringWithCoords(String name, int x, int y, int z) {
            this.name = name;
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

}

