package com.l2j.datapack.custom.bots.behaviour

import com.l2j.datapack.custom.bots.skills.SkillPreferences
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.sets.presets.FighterFullBuff
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.sets.presets.MageFullBuff
import com.l2jserver.common.noarg.NoArg

@NoArg
class ClassBehaviour(
    val name: String,
    val equipmentGroup: String,
    val skillPreferences: SkillPreferences,
    val buffPreference: String,
    val desiredNextClassIds: List<Int> = emptyList()
) {
    companion object {
        @JvmField
        val DEFAULT_FIGHTER = ClassBehaviour(
            "Default Fighter",
            "fighter",
            SkillPreferences.DEFAULT_FIGHTER,
            FighterFullBuff.NAME
        )
        @JvmField
        val DEFAULT_MAGE = ClassBehaviour(
            "Default Mage",
            "mage",
            SkillPreferences.DEFAULT_MAGE,
            MageFullBuff.NAME
        )
        @JvmField
        val DEFAULT_KAMAEL = ClassBehaviour(
            "Default Kamael",
            "kamael",
            SkillPreferences.DEFAULT_KAMAEL,
            FighterFullBuff.NAME
        )
    }
}