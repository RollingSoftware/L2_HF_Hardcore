package com.l2j.datapack.custom.bots.goals;

public interface AiGoal {

    boolean check();

}
