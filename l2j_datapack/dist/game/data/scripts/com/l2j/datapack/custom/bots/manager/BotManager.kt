package com.l2j.datapack.custom.bots.manager

import com.l2j.datapack.ai.npc.AbstractNpcAI
import com.l2j.datapack.custom.bots.ai.BotAi
import com.l2j.datapack.custom.bots.behaviour.ClassBehaviourTable
import com.l2j.datapack.custom.bots.characters.CharacterCreator
import com.l2j.datapack.custom.bots.equipment.EquipmentTable
import com.l2j.datapack.custom.bots.farmzones.FarmZonesTable
import com.l2j.datapack.custom.bots.manager.BotCommands.botManager
import com.l2j.datapack.custom.bots.manager.BotManager
import com.l2jserver.gameserver.ThreadPoolManager
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.events.Containers
import com.l2jserver.gameserver.model.events.EventType
import com.l2jserver.gameserver.model.events.listeners.RunnableEventListener
import org.apache.logging.log4j.LogManager
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

class BotManager : AbstractNpcAI(BotManager::class.java.simpleName, "Fake Players Manager") {
    private val serverStarted = AtomicBoolean(false)
    private var serverStartListener: RunnableEventListener? = null
    private var serverShutdownListener: RunnableEventListener? = null
    private var managerFuture: ScheduledFuture<*>? = null
    private var botRunner: BotManagerRunnable? = null
    private val characterCreator = CharacterCreator()
    private val farmZonesTable = FarmZonesTable()
    private val classBehaviourTable = ClassBehaviourTable()
    private val botDao = BotDao()
    private val botManagerConfig = BotManagerConfig()
    private val equipmentTable = EquipmentTable()

    init {
        botManager = this
        load()
    }

    fun reload() {
        unload()
        load()
    }

    override fun unload(): Boolean {
        Containers.Global().removeListener(serverStartListener)
        Containers.Global().removeListener(serverShutdownListener)
        botRunner!!.cancelled.set(true)
        managerFuture!!.cancel(false)
        return super.unload()
    }

    fun load() {
        LOG.info("Initializing FakePlayer Manager")
        botDao.createTableIfNotExist()

        serverStartListener = RunnableEventListener(
            Containers.Global(),
            EventType.ON_SERVER_STARTED,
            { serverStarted.set(true) },
            this
        )
        Containers.Global().addListener(serverStartListener)

        serverShutdownListener = RunnableEventListener(
            Containers.Global(),
            EventType.ON_SERVER_SHUTDOWN,
            {
                serverStarted.set(false)
                managerFuture?.cancel(true)
            },
            this
        )
        Containers.Global().addListener(serverShutdownListener)
        botRunner = BotManagerRunnable(
            characterCreator,
            farmZonesTable,
            classBehaviourTable,
            botDao,
            botManagerConfig,
            serverStarted,
            equipmentTable
        )
        managerFuture = ThreadPoolManager.getInstance().scheduleBotAiAtFixedRate(
            botRunner,
            0,
            botManagerConfig.runInterval.seconds,
            TimeUnit.SECONDS
        )
    }

    fun setTargetBotCount(count: Int) {
        botManagerConfig.targetBotCount = count
    }

    fun makeTargetABot(player: L2PcInstance) {
        if (player.hasAI()) {
            player.ai.stopAITask()
        }
        val ai = BotAi(player, farmZonesTable, classBehaviourTable, equipmentTable)
        player.setAiSupplier { ai }
        player.ai = ai
        ai.startAi()
    }

    companion object {
        private val LOG = LogManager.getLogger()
    }
}