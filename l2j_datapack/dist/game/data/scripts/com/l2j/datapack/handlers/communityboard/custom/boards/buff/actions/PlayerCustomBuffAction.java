package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.CommunityBuffList;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class PlayerCustomBuffAction implements BoardAction {

    private final static Logger LOG = LogManager.getLogger();

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.isEmpty() || args.getArgs().size() != 2) {
            LOG.warn("{} is trying to buff custom preset with invalid args number {}", player, args.getArgs().size());
            return ProcessResult.failure(player.string("invalid_preset_buff_request"));
        }

        String presetName = args.getArgs().get(0);
        Optional<CommunityBuffList> communityBuffList = DAOFactory.getInstance().getCommunityBuffListDao().findSingleCommunityBuffSet(player.getObjectId(), presetName);
        if (communityBuffList.isEmpty()) {
            LOG.warn("Player {} is trying to buff a preset {} that does not belong to him", player, presetName);
            return ProcessResult.failure(player.string("could_not_buff_your_custom_preset"));
        }

        PresetBuffAction presetBuffAction = new PresetBuffAction(() -> communityBuffList.get().getSkills());
        return presetBuffAction.process(player, ActionArgs.subActionArgs(args));
    }

}
