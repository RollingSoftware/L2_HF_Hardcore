/*
 * Copyright (C) 2004-2016 L2J DataPack
 *
 * This file is part of L2J DataPack.
 *
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2j.datapack.handlers;

import com.l2j.datapack.handlers.actionhandlers.*;
import com.l2j.datapack.handlers.actionshifthandlers.*;
import com.l2j.datapack.handlers.admincommandhandlers.*;
import com.l2j.datapack.handlers.bypasshandlers.*;
import com.l2j.datapack.handlers.bypasshandlers.bulk.BulkSell;
import com.l2j.datapack.handlers.bypasshandlers.bulk.BulkWarehouseStore;
import com.l2j.datapack.handlers.bypasshandlers.olympiad.OlympiadCycleClassHandler;
import com.l2j.datapack.handlers.chathandlers.*;
import com.l2j.datapack.handlers.communityboard.*;
import com.l2j.datapack.handlers.communityboard.custom.CustomHomeBoard;
import com.l2j.datapack.handlers.communityboard.custom.boards.blacksmith.BlacksmithBoard;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.BuffBoard;
import com.l2j.datapack.handlers.communityboard.custom.boards.shop.ShopBoard;
import com.l2j.datapack.handlers.communityboard.custom.boards.teleport.TeleportBoard;
import com.l2j.datapack.handlers.custom.CustomAnnouncePkPvP;
import com.l2j.datapack.handlers.itemhandlers.*;
import com.l2j.datapack.handlers.punishmenthandlers.BanHandler;
import com.l2j.datapack.handlers.punishmenthandlers.ChatBanHandler;
import com.l2j.datapack.handlers.punishmenthandlers.JailHandler;
import com.l2j.datapack.handlers.targethandlers.*;
import com.l2j.datapack.handlers.usercommandhandlers.*;
import com.l2j.datapack.handlers.voicedcommandhandlers.*;
import com.l2jserver.Config;
import com.l2jserver.gameserver.handler.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Master handler.
 *
 * @author UnAfraid
 */
public class MasterHandler {
    private static final Logger LOG = LogManager.getLogger();

    private static final IHandler<?, ?>[] LOAD_INSTANCES =
            {
                    ActionHandler.getInstance(),
                    ActionShiftHandler.getInstance(),
                    AdminCommandHandler.getInstance(),
                    BypassHandler.getInstance(),
                    ChatHandler.getInstance(),
                    CommunityBoardHandler.getInstance(),
                    ItemHandler.getInstance(),
                    PunishmentHandler.getInstance(),
                    UserCommandHandler.getInstance(),
                    VoicedCommandHandler.getInstance(),
                    TargetHandler.getInstance()
            };

    private static final Class<?>[][] HANDLERS =
            {
                    {
                            // Action Handlers
                            L2ArtefactInstanceAction.class,
                            L2DecoyAction.class,
                            L2DoorInstanceAction.class,
                            L2ItemInstanceAction.class,
                            L2NpcAction.class,
                            L2PcInstanceAction.class,
                            L2PetInstanceAction.class,
                            L2StaticObjectInstanceAction.class,
                            L2SummonAction.class,
                            L2TrapAction.class,
                    },
                    {
                            // Action Shift Handlers
                            L2DoorInstanceActionShift.class,
                            L2ItemInstanceActionShift.class,
                            L2NpcActionShift.class,
                            L2PcInstanceActionShift.class,
                            L2StaticObjectInstanceActionShift.class,
                            L2SummonActionShift.class,
                    },
                    {
                            // Admin Command Handlers
                            AdminAdmin.class,
                            AdminAnnouncements.class,
                            AdminBBS.class,
                            AdminBuffs.class,
                            AdminCamera.class,
                            AdminChangeAccessLevel.class,
                            AdminCHSiege.class,
                            AdminClan.class,
                            AdminPcCondOverride.class,
                            AdminCreateItem.class,
                            AdminCursedWeapons.class,
                            AdminDebug.class,
                            AdminDelete.class,
                            AdminDisconnect.class,
                            AdminDoorControl.class,
                            AdminEditChar.class,
                            AdminEffects.class,
                            AdminElement.class,
                            AdminEnchant.class,
                            AdminEventEngine.class,
                            AdminEvents.class,
                            AdminExpSp.class,
                            AdminFightCalculator.class,
                            AdminFortSiege.class,
                            AdminGeodata.class,
                            AdminGm.class,
                            AdminGmChat.class,
                            AdminGraciaSeeds.class,
                            AdminGrandBoss.class,
                            AdminHeal.class,
                            AdminHtml.class,
                            AdminInstance.class,
                            AdminInstanceZone.class,
                            AdminInvul.class,
                            AdminKick.class,
                            AdminKill.class,
                            AdminLevel.class,
                            AdminLogin.class,
                            AdminMammon.class,
                            AdminManor.class,
                            AdminMenu.class,
                            AdminMessages.class,
                            AdminMobGroup.class,
                            AdminMonsterRace.class,
                            AdminPathNode.class,
                            AdminPetition.class,
                            AdminPForge.class,
                            AdminPledge.class,
                            AdminPolymorph.class,
                            AdminPunishment.class,
                            AdminQuest.class,
                            AdminReload.class,
                            AdminRepairChar.class,
                            AdminRes.class,
                            AdminRide.class,
                            AdminScan.class,
                            AdminShop.class,
                            AdminShowQuests.class,
                            AdminShutdown.class,
                            AdminSiege.class,
                            AdminSkill.class,
                            AdminSpawn.class,
                            AdminSummon.class,
                            AdminTarget.class,
                            AdminTargetSay.class,
                            AdminTeleport.class,
                            AdminTerritoryWar.class,
                            AdminTest.class,
                            AdminTvTEvent.class,
                            AdminUnblockIp.class,
                            AdminVitality.class,
                            AdminZone.class,
                            AdminLocation.class,
                            AdminBots.class,
                            AdminInventory.class
                    },
                    {
                            // Bypass Handlers
                            Augment.class,
                            Buy.class,
                            BuyShadowItem.class,
                            ChatLink.class,
                            ClanWarehouse.class,
                            EventEngine.class,
                            Festival.class,
                            Freight.class,
                            ItemAuctionLink.class,
                            Link.class,
                            Loto.class,
                            Multisell.class,
                            NpcViewMod.class,
                            Observation.class,
                            OlympiadObservation.class,
                            OlympiadManagerLink.class,
                            QuestLink.class,
                            PlayerHelp.class,
                            PrivateWarehouse.class,
                            QuestList.class,
                            ReceivePremium.class,
                            ReleaseAttribute.class,
                            RentPet.class,
                            Rift.class,
                            SkillList.class,
                            SupportBlessing.class,
                            SupportMagic.class,
                            TerritoryStatus.class,
                            TutorialClose.class,
                            VoiceCommand.class,
                            Wear.class,
                            BulkSell.class,
                            BulkWarehouseStore.class
                    },
                    {
                            // Chat Handlers
                            ChatAll.class,
                            ChatAlliance.class,
                            ChatBattlefield.class,
                            ChatClan.class,
                            ChatHeroVoice.class,
                            ChatParty.class,
                            ChatPartyMatchRoom.class,
                            ChatPartyRoomAll.class,
                            ChatPartyRoomCommander.class,
                            ChatPetition.class,
                            ChatShout.class,
                            ChatTell.class,
                            ChatTrade.class,
                    },
                    {
                            // Community Board
                            ClanBoard.class,
                            FavoriteBoard.class,
                            FriendsBoard.class,
                            Config.CUSTOM_COMMUNITY ? CustomHomeBoard.class : HomeBoard.class,
                            HomepageBoard.class,
                            MailBoard.class,
                            MemoBoard.class,
                            RegionBoard.class,
                            BuffBoard.class,
                            TeleportBoard.class,
                            ShopBoard.class,
                            BlacksmithBoard.class
                    },
                    {
                            // Item Handlers
                            BeastSoulShot.class,
                            BeastSpiritShot.class,
                            BlessedSpiritShot.class,
                            Book.class,
                            Bypass.class,
                            Calculator.class,
                            CharmOfCourage.class,
                            Disguise.class,
                            Elixir.class,
                            EnchantAttribute.class,
                            EnchantScrolls.class,
                            EventItem.class,
                            ExtractableItems.class,
                            FishShots.class,
                            Harvester.class,
                            ItemSkills.class,
                            ItemSkillsTemplate.class,
                            ManaPotion.class,
                            Maps.class,
                            MercTicket.class,
                            NicknameColor.class,
                            PetFood.class,
                            Recipes.class,
                            RollingDice.class,
                            Seed.class,
                            SevenSignsRecord.class,
                            SoulShots.class,
                            SpecialXMas.class,
                            SpiritShot.class,
                            SummonItems.class,
                            TeleportBookmark.class,
                    },
                    {
                            // Punishment Handlers
                            BanHandler.class,
                            ChatBanHandler.class,
                            JailHandler.class,
                    },
                    {
                            // User Command Handlers
                            ClanPenalty.class,
                            ClanWarsList.class,
                            Dismount.class,
                            Unstuck.class,
                            InstanceZone.class,
                            Loc.class,
                            Mount.class,
                            PartyInfo.class,
                            Time.class,
                            OlympiadStat.class,
                            ChannelLeave.class,
                            ChannelDelete.class,
                            ChannelInfo.class,
                            MyBirthday.class,
                            SiegeStatus.class,
                    },
                    {
                            // Voiced Command Handlers
                            StatsVCmd.class,
                            // TODO: Add configuration options for this voiced commands:
                            // CastleVCmd.class,
                            // SetVCmd.class,
                            (Config.L2JMOD_ALLOW_WEDDING ? Wedding.class : null),
                            (Config.BANKING_SYSTEM_ENABLED ? Banking.class : null),
                            (Config.L2JMOD_CHAT_ADMIN ? ChatAdmin.class : null),
                            (Config.L2JMOD_MULTILANG_ENABLE && Config.L2JMOD_MULTILANG_VOICED_ALLOW ? Lang.class : null),
                            (Config.L2JMOD_DEBUG_VOICE_COMMAND ? Debug.class : null),
                            (Config.L2JMOD_ALLOW_CHANGE_PASSWORD ? ChangePassword.class : null),
                    },
                    {
                            // Target Handlers
                            Area.class,
                            AreaCorpseMob.class,
                            AreaFriendly.class,
                            AreaSummon.class,
                            Aura.class,
                            AuraCorpseMob.class,
                            BehindArea.class,
                            BehindAura.class,
                            Clan.class,
                            ClanMember.class,
                            CommandChannel.class,
                            CorpseClan.class,
                            CorpseMob.class,
                            EnemySummon.class,
                            FlagPole.class,
                            FrontArea.class,
                            FrontAura.class,
                            Ground.class,
                            Holy.class,
                            One.class,
                            OwnerPet.class,
                            Party.class,
                            PartyClan.class,
                            PartyMember.class,
                            PartyNotMe.class,
                            PartyOther.class,
                            PcBody.class,
                            Pet.class,
                            Self.class,
                            Servitor.class,
                            Summon.class,
                            TargetParty.class,
                            Unlockable.class,
                    },
                    {
                            // Custom Handlers
                            CustomAnnouncePkPvP.class,
                            OlympiadCycleClassHandler.class
                    }
            };

    public MasterHandler() {
        LOG.info("Loading Handlers...");

        Map<IHandler<?, ?>, Method> registerHandlerMethods = new HashMap<>();
        for (IHandler<?, ?> loadInstance : LOAD_INSTANCES) {
            registerHandlerMethods.put(loadInstance, null);
            for (Method method : loadInstance.getClass().getMethods()) {
                if (method.getName().equals("registerHandler") && !method.isBridge()) {
                    registerHandlerMethods.put(loadInstance, method);
                }
            }
        }

        registerHandlerMethods
                .entrySet()
                .stream()
                .filter(e -> e.getValue() == null)
                .forEach(e ->
                        LOG.warn("Failed loading handlers of: " + e.getKey().getClass().getSimpleName() + " seems registerHandler function does not exist.")
                );

        for (Class<?>[] classes : HANDLERS) {
            for (Class<?> c : classes) {
                if (c == null) {
                    continue; // Disabled handler
                }

                try {
                    Object handler = c.getConstructor().newInstance();
                    for (Entry<IHandler<?, ?>, Method> entry : registerHandlerMethods.entrySet()) {
                        if ((entry.getValue() != null) && entry.getValue().getParameterTypes()[0].isInstance(handler)) {
                            entry.getValue().invoke(entry.getKey(), handler);
                        }
                    }
                } catch (Exception e) {
                    LOG.warn("Failed loading handler: " + c.getSimpleName(), e);
                }
            }
        }

        for (IHandler<?, ?> loadInstance : LOAD_INSTANCES) {
            LOG.info(loadInstance.getClass().getSimpleName() + ": Loaded " + loadInstance.size() + " Handlers");
        }
        LOG.info("Handlers Loaded...");
    }
}