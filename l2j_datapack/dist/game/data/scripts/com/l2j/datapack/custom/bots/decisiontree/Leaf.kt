package com.l2j.datapack.custom.bots.decisiontree

data class Leaf(val action: () -> Unit, var complete: Boolean = false) {
    fun invoke() {
        action.invoke()
        complete = true
    }
}