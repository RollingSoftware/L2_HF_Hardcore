package com.l2j.datapack.custom.bots.skills;

public class SkillParameter {

    private Integer skillId;
    private Integer useFrequency;

    public SkillParameter() {
    }

    public SkillParameter(Integer skillId, Integer useFrequency) {
        this.skillId = skillId;
        this.useFrequency = useFrequency;
    }

    public Integer getSkillId() {
        return skillId;
    }

    public Integer getUseFrequency() {
        return useFrequency;
    }
}
