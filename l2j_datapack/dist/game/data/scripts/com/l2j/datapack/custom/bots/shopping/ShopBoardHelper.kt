package com.l2j.datapack.custom.bots.shopping

import com.l2jserver.gameserver.data.xml.impl.ArmorSetsData
import com.l2jserver.gameserver.data.xml.impl.BuyListData
import com.l2jserver.gameserver.datatables.ItemTable
import com.l2jserver.gameserver.datatables.MerchantPriceConfigTable
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.buylist.Product
import com.l2jserver.gameserver.model.holders.ItemHolder
import com.l2jserver.gameserver.model.holders.UniqueItemHolder
import com.l2jserver.gameserver.model.items.L2Weapon
import com.l2jserver.gameserver.model.items.graded.Grade
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance
import com.l2jserver.gameserver.model.items.type.EtcItemType
import com.l2jserver.gameserver.model.items.type.WeaponType
import com.l2jserver.gameserver.network.clientpackets.RequestBuyItem
import com.l2jserver.gameserver.network.clientpackets.RequestSellItem
import com.l2j.datapack.custom.bots.equipment.EquipmentManager
import com.l2j.datapack.custom.items.IdTable
import com.l2j.datapack.handlers.communityboard.custom.boards.shop.ShopBoard

object ShopBoardHelper {

    private fun findItemsInShopBoard(itemIds: List<Int>): List<Product> =
        ShopBoard.knownBuyLists.map { buyListId ->
            BuyListData.getInstance().getBuyList(buyListId)
        }.flatMap { buyList ->
            itemIds.map { itemId -> buyList.getProductByItemId(itemId) }
        }.filterNotNull().distinctBy { it.itemId }

    fun findBestWeaponsYouCanAfford(budget: Long, weaponTypes: List<WeaponType>): List<L2Weapon> {
        return ShopBoard.knownBuyLists.map { buyListId ->
            BuyListData.getInstance().getBuyList(buyListId)
        }.flatMap { buyList ->
            buyList.products
                .asSequence()
                .filter { it.price <= budget }
                .map { ItemTable.getInstance().getTemplate(it.itemId) }
                .filter { it.isWeapon }
                .map { it as L2Weapon }
                .filter { it.itemType in weaponTypes }
                .toList()
        }.sortedByDescending { it.referencePrice }
    }

    fun buyBetterWeapon(player: L2PcInstance, equipmentManager: EquipmentManager): L2ItemInstance? {
        if (player.adena <= safetyBudget) {
            return null
        }

        val budget = player.adena - safetyBudget
        val bestWeaponsYouCanAfford =
            findBestWeaponsYouCanAfford(
                budget,
                equipmentManager.equipmentPreferences.preferredWeaponTypes.map { it.weaponType() }
            )

        val preferredWeaponYouCanAfford = equipmentManager.findPreferredBetterWeapons(player, bestWeaponsYouCanAfford)
        val bought = preferredWeaponYouCanAfford?.let {
            buyIfEnoughAdena(player, listOf(ItemHolder(it.id, 1)))
        }
        return if (bought == true) {
            player.inventory.getFirstOrNull(preferredWeaponYouCanAfford.id)
        } else {
            null
        }
    }

    const val expectedHealingPotionStock = 100L
    const val expectedManaDrugStock = 50L
    const val safetyBudget = 5_000_000

    fun hasSpareMoney(player: L2PcInstance): Boolean = player.adena > safetyBudget

    fun replenishPotions(player: L2PcInstance) {
        val healingPotionStockCount = player.inventory.getFirstOrNull(IdTable.whiteGreaterHealingPotionId)?.count ?: 0
        if (healingPotionStockCount < expectedHealingPotionStock) {
            buyIfEnoughAdena(player, listOf(ItemHolder(IdTable.whiteGreaterHealingPotionId, expectedHealingPotionStock - healingPotionStockCount)))
        }

        val manaDrugStockCount = player.inventory.getFirstOrNull(IdTable.manaDrugId)?.count ?: 0
        if (manaDrugStockCount < expectedManaDrugStock) {
            buyIfEnoughAdena(player, listOf(ItemHolder(IdTable.manaDrugId, expectedManaDrugStock - manaDrugStockCount)))
        }
    }

    val unusedGradePredicateCurry =
        { gradesToSell: List<Grade> ->
            { item: L2ItemInstance -> Grade.fromCrystalType(item.item.crystalType) in gradesToSell }
        }
    val scrollsPredicate = { item: L2ItemInstance -> item.isScroll }
    val partsPredicate = { item: L2ItemInstance -> item.isEtcItem && item.etcItem.itemType in listOf(EtcItemType.RECIPE, EtcItemType.MATERIAL) }

    fun sellUnused(player: L2PcInstance) {
        val gradesToSell = Grade.gradesByLevel(player.level).drop(1)
        val unusedGradePredicate = unusedGradePredicateCurry(gradesToSell)
        val unusedGradeEquipmentPredicate = { item: L2ItemInstance -> item.isEquipable && unusedGradePredicate.invoke(item) }

        val itemsToSell = player.inventory.allInventoryItems.filter {
            unusedGradeEquipmentPredicate.invoke(it) || scrollsPredicate.invoke(it) || partsPredicate.invoke(it)
        }.map {
            UniqueItemHolder(
                it.itemId,
                it.objectId,
                it.count
            )
        }

        if (itemsToSell.isNotEmpty()) {
            val requestSell = RequestSellItem(
                ShopBoard.knownBuyLists.first(),
                itemsToSell
            )
            requestSell.client = player.client
            requestSell.run()
        }
    }

    // TODO Allow to buy missing parts
    fun buySetIfEnoughAdena(player: L2PcInstance, armorSetId: Int): Boolean {
        val armorSet = ArmorSetsData.getInstance().getSetById(armorSetId)

        val allPartsFoundInShop = findItemsInShopBoard(armorSet.allParts)
        val allPartsAvailable = armorSet.containsAllSetParts(allPartsFoundInShop.map { it.itemId })
        if (!allPartsAvailable) {
            return false
        }

        return buyIfEnoughAdena(player, allPartsFoundInShop.map { ItemHolder(it.itemId, 1) })
    }

    fun buyIfEnoughAdena(player: L2PcInstance, itemsToBuy: List<ItemHolder>): Boolean {
        val itemIds = itemsToBuy.map { it.id }
        val itemCountMap = itemsToBuy.associateBy { it.id }
        val allItemsFoundInShop = findItemsInShopBoard(itemIds)

        val allItemsAvailable = allItemsFoundInShop.map { it.itemId }.containsAll(itemIds)
        if (!allItemsAvailable) {
            return false
        }

        val regionalPriceConfig = MerchantPriceConfigTable.getInstance().getMerchantPriceConfig(player)
        val totalPrice = allItemsFoundInShop.sumOf { product -> product.price }
        val totalPriceWithTax = totalPrice * regionalPriceConfig.totalTaxRate

        if (player.adena < totalPriceWithTax) {
            return false
        }

        allItemsFoundInShop.groupBy(Product::getBuyListId).map { productEntry ->
            val requestBuyItem = RequestBuyItem(
                productEntry.key,
                productEntry.value.map { itemCountMap[it.itemId] }
            )
            requestBuyItem.client = player.client
            requestBuyItem.run()
        }
        return true
    }
}