package com.l2j.datapack.custom.bots.equipment.types;

import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.WeaponType;

public class AncientSwordType implements EquippableWeaponType {

    public static final AncientSwordType INSTANCE = new AncientSwordType();

    @Override
    public boolean typeOf(L2Weapon weapon) {
        return weapon.getItemType() == weaponType();
    }

    @Override
    public String name() {
        return "AncientSword";
    }

    @Override
    public WeaponType weaponType() {
        return WeaponType.ANCIENTSWORD;
    }
}