package com.l2j.datapack.custom.bots.goals

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance

data class ReachLevelGoal(val player: L2PcInstance, val targetLevel: Int) : AiGoal {

    override fun check(): Boolean {
        return player.level >= targetLevel;
    }

}
