package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2j.datapack.handlers.communityboard.custom.BoardCondition;
import com.l2j.datapack.handlers.communityboard.custom.CommunityPaymentHelper;
import com.l2jserver.Config;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.target.TargetHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BoardCancelAction implements BoardAction {

    private final static Logger LOG = LogManager.getLogger();

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.getArgs().size() != 1) {
            LOG.warn("{} is trying to use cancel with invalid args number {}", player, args.getArgs().size());
            return ProcessResult.failure(player.string("invalid_request"));
        }

        String targetString = args.getArgs().get(0);
        TargetHolder targetHolder = TargetHolder.parseTarget(player, targetString);


        ProcessResult checkResult = BoardCondition.checkCondition(player);
        if (checkResult.isFailure()) {
            return checkResult;
        }

        ProcessResult paymentResult = CommunityPaymentHelper.payForService(player, Config.COMMUNITY_CANCEL_PRICE);
        if (paymentResult.isFailure()) {
            return paymentResult;
        }

        int delay = GameTimeController.TICKS_PER_SECOND * GameTimeController.MILLIS_IN_TICK;
        SkillHolder cancellation = new SkillHolder(1056, 1);

        if (targetHolder.isSummonTarget()) {
            targetHolder.getMaster().fakeCast(cancellation, delay, () -> cancelBuffs(targetHolder.getSummon()), targetHolder.getSummon());
        } else {
            targetHolder.getMaster().fakeCast(cancellation, delay, () -> cancelBuffs(targetHolder.getMaster()), targetHolder.getMaster());
        }

        return ProcessResult.success();
    }

    private void cancelBuffs(L2Character target) {
        target.getEffectList().stopAllBuffs(true, false);
        target.getEffectList().stopAllDances(true);
    }

}
