package com.l2j.datapack.custom.clan.territory

import com.l2jserver.common.resources.YamlResource
import com.l2j.datapack.helper.ScriptResourceResolver.Companion.resolve
import org.apache.logging.log4j.LogManager

class TerritoryOwningRewardsTable {
    private lateinit var allTerritoryOwningRewards: Map<Int, TerritoryOwningReward>

    fun load() {
        try {
            val territoryOwningRewards = YamlResource.loadFile(
                resolve(javaClass, REWARDS_FILE_PATH),
                TerritoryOwningRewardData::class.java
            )
            allTerritoryOwningRewards = (territoryOwningRewards.castles + territoryOwningRewards.fortresses).associateBy { it.residenceId }

            LOG.info("{} {} reward entries loaded!", TerritoryOwningRewardsManager.LOG_TAG, allTerritoryOwningRewards.size)
        } catch (e: IllegalStateException) {
            val message = "Could not read rewards file from '$REWARDS_FILE_PATH', please check that file exists and is a valid YML"
            LOG.error("{}$message error: {}", TerritoryOwningRewardsManager.LOG_TAG, e.message)
            throw IllegalStateException(message)
        }
    }

    fun findResidenceReward(residenceId: Int): TerritoryOwningReward? = allTerritoryOwningRewards[residenceId]

    companion object {
        private val LOG = LogManager.getLogger()
        private const val REWARDS_FILE_PATH = "./territory_owning_rewards.yml"
    }
}