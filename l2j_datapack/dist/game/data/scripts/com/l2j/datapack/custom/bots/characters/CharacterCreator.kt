package com.l2j.datapack.custom.bots.characters

import com.l2j.datapack.custom.bots.connection.FakeMmoConnection
import com.l2jserver.common.util.Rnd
import com.l2jserver.gameserver.LoginServerThread
import com.l2jserver.gameserver.data.sql.impl.CharNameTable
import com.l2jserver.gameserver.model.actor.appearance.PcAppearance
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.base.ClassId
import com.l2jserver.gameserver.network.L2GameClient
import com.l2jserver.gameserver.network.clientpackets.CharacterCreate
import com.l2jserver.localization.Language

class CharacterCreator {
    private var nicknames: Nicknames = Nicknames.loadFromFile()

    private fun generateFreeNickname(): String {
        repeat(nicknameGenerateMaxTries) {
            val nickname = nicknames.generateRandomNickname()
            if (!CharNameTable.getInstance().doesCharNameExist(nickname)) {
                return nickname
            }
        }
        throw IllegalStateException("Ran out of nicknames, cannot generate unique ones no more")
    }

    fun createRandomCharacter(): L2PcInstance {
        val sex = Rnd.nextBoolean()
        val nickname = generateFreeNickname()

        val baseClass = BASE_CLASSES.random()
        val isKamael = baseClass in KAMAEL_BASE_CLASSES
        val fakePlayer = L2PcInstance.create(
            baseClass,
            FAKE_ACCOUNT_NAME,
            nickname,
            PcAppearance(
                FACES.random(),
                if (isKamael) KAMAEL_HAIR_COLOR.random() else HAIR_COLOR.random(),
                if (sex) FEMALE_HAIRSTYLES.random() else MALE_HAIRSTYLES.random(),
                sex
            ),
            true
        )
        CharNameTable.getInstance().addName(fakePlayer)

        val fakeClient = injectFakeClient(fakePlayer)
        LoginServerThread.getInstance().addGameServerLogin(fakePlayer.accountName, fakeClient)
        fakePlayer.currentHp = fakePlayer.maxHp.toDouble()
        fakePlayer.currentMp = fakePlayer.maxMp.toDouble()
        CharacterCreate.initNewChar(fakeClient, fakePlayer, Language.english().code)
        val loadedCharacter = L2PcInstance.load(fakePlayer.objectId)
        loadedCharacter.client = fakeClient
        fakeClient.activeChar = loadedCharacter
        return loadedCharacter
    }

    companion object {
        private const val FAKE_ACCOUNT_NAME = "FAKE_SYSTEM_ACCOUNT"
        private val MALE_HAIRSTYLES = listOf(0, 1, 2, 3, 4).map { obj: Int -> obj.toByte() }
        private val FEMALE_HAIRSTYLES = listOf(0, 1, 2, 3, 4, 5, 6).map { obj: Int -> obj.toByte() }
        private val HAIR_COLOR = listOf(0, 1, 2, 3).map { obj: Int -> obj.toByte() }
        private val KAMAEL_HAIR_COLOR = listOf(0, 1, 2).map { obj: Int -> obj.toByte() }
        private val FACES = listOf(0, 1, 2).map { obj: Int -> obj.toByte() }
        private val BASE_CLASSES = listOf(
            ClassId.darkFighter, ClassId.darkMage,
            ClassId.fighter, ClassId.mage,
            ClassId.elvenFighter, ClassId.elvenMage,
            ClassId.orcFighter, ClassId.orcMage,
            ClassId.dwarvenFighter,
            ClassId.maleSoldier, ClassId.femaleSoldier
        ).map { it.id }

        private val KAMAEL_BASE_CLASSES = listOf(ClassId.maleSoldier, ClassId.femaleSoldier).map { it.id }

        const val nicknameGenerateMaxTries: Int = 100

        fun injectFakeClient(player: L2PcInstance): L2GameClient {
            val fakeConnection = FakeMmoConnection<L2GameClient>()
            val fakeClient = L2GameClient(fakeConnection)

            fakeConnection.client = fakeClient
            player.client = fakeClient
            fakeClient.accountLanguage = Language.english()
            fakeClient.activeChar = player
            fakeClient.accountName = player.accountNamePlayer
            fakeClient.state = L2GameClient.GameClientState.IN_GAME

            return fakeClient
        }
    }

}