package com.l2j.datapack.custom.bots.states

import com.l2jserver.gameserver.model.Location
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.util.Util

class StuckMonitoring(val player: L2PcInstance) {

    private val locationChangeUpdateFrequency = 1000
    private val locationChangeThreshold = 7 * 60 * 1000

    private var lastTimeLocationChanged = System.currentTimeMillis()
    private var lastLocation = Location(player)

    fun check(): Boolean {
        val locationChangeDelta = System.currentTimeMillis() - lastTimeLocationChanged
        if (locationChangeDelta > locationChangeUpdateFrequency) {
            if (Util.calculateDistance(lastLocation, player.location, false, false) > 50) {
                lastTimeLocationChanged = System.currentTimeMillis()
                lastLocation = player.location
            } else if (locationChangeDelta > locationChangeThreshold) {
                return true
            }
        }
        return false
    }

}