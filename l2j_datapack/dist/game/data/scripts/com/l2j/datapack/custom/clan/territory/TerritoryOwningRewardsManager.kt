package com.l2j.datapack.custom.clan.territory

import com.l2j.datapack.ai.npc.AbstractNpcAI
import com.l2jserver.gameserver.GameTimeController
import com.l2jserver.gameserver.ThreadPoolManager
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory
import com.l2jserver.gameserver.datatables.ItemTable
import com.l2jserver.gameserver.instancemanager.CastleManager
import com.l2jserver.gameserver.instancemanager.FortManager
import com.l2jserver.gameserver.instancemanager.MailManager
import com.l2jserver.gameserver.localization.Strings
import com.l2jserver.gameserver.model.entity.Message
import com.l2jserver.gameserver.model.entity.interfaces.Residence
import com.l2jserver.gameserver.model.holders.ItemHolder
import org.apache.logging.log4j.LogManager
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneOffset
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import java.util.function.Consumer
import java.util.stream.Collectors

class TerritoryOwningRewardsManager private constructor() :
    AbstractNpcAI(TerritoryOwningRewardsManager::class.java.simpleName, "custom/clan/territory") {

    private val territoryOwningRewardsTable = TerritoryOwningRewardsTable()
    private var taskHandle: ScheduledFuture<*>? = null

    private fun scheduleRewardTask() {
        val currentDateTime = LocalDateTime.now()
        val currentTime = currentDateTime.toLocalTime()
        val expectedExecutionDateTime: LocalDateTime = if (currentTime.isBefore(DISTRIBUTION_TIME)) {
            DISTRIBUTION_TIME.atDate(currentDateTime.toLocalDate())
        } else {
            DISTRIBUTION_TIME.atDate(currentDateTime.toLocalDate()).plusDays(1L)
        }
        val expectedTimeMillis = expectedExecutionDateTime.atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli()
        val currentTimeMillis = currentDateTime.atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli()
        val delay = expectedTimeMillis - currentTimeMillis
        taskHandle = ThreadPoolManager.getInstance()
            .scheduleGeneralAtFixedRate({ distributeRewards() }, delay, GameTimeController.FULL_DAY_IN_MILLIS, TimeUnit.MILLISECONDS)
        LOG.info("{} Next reward distribution will be at {} [{} millis]", LOG_TAG, expectedExecutionDateTime, delay)
    }

    private fun distributeRewards() {
        LOG.info("{} Reward distribution!", LOG_TAG)
        val castles = onlyOwnedByClan(CastleManager.getInstance().castles)
        val forts = onlyOwnedByClan(FortManager.getInstance().forts)
        distributeRewardsByResidences(castles + forts)
    }

    private fun distributeRewardsByResidences(residences: List<Residence>) {
        for (residence in residences) {
            val territoryOwningReward = territoryOwningRewardsTable.findResidenceReward(residence.residenceId)
            if (territoryOwningReward == null) {
                LOG.warn("Residence {} [{}] has no Territory Owning Reward data", residence.name, residence.residenceId)
                continue
            }
            val rewards = territoryOwningReward.rewards
            rewards.forEach(
                Consumer { reward: ItemHolder ->
                    residence.ownerClan.warehouse.addItem(
                        "Territory Owning Reward",
                        reward.id,
                        reward.count,
                        null,
                        null
                    )
                }
            )

            val rewardsList = rewards.stream()
                .map { itemInstance: ItemHolder -> ItemTable.getInstance().getTemplate(itemInstance.id).name + " [" + itemInstance.count + "]" }
                .collect(Collectors.joining(", \n"))

            val leaderId = residence.ownerClan.leader.objectId
            val leaderLanguage = DAOFactory.getInstance().playerDAO.loadLanguageByCharacterId(leaderId)
            val stringsTable = Strings.lang(leaderLanguage)
            val rewardMessage = Message(
                leaderId,
                stringsTable["territory_owning_reward"] + '!',
                """
                   ${stringsTable["territory_owning_reward_message"]}:
                    
                   $rewardsList
                """.trimIndent(),
                Message.SendBySystem.NONE
            )
            MailManager.getInstance().sendMessage(rewardMessage)
        }
    }

    fun reload() {
        if (taskHandle != null) {
            taskHandle!!.cancel(false)
        }
        territoryOwningRewardsTable.load()
        scheduleRewardTask()
    }

    override fun unload(): Boolean {
        if (taskHandle != null) {
            taskHandle!!.cancel(false)
        }
        return super.unload()
    }

    companion object {
        private val LOG = LogManager.getLogger()
        const val LOG_TAG = "[TerritoryOwningRewardsManager]"
        private val DISTRIBUTION_TIME = LocalTime.parse("08:00:00") // 24H format

        fun <T : Residence?> onlyOwnedByClan(residences: List<T>): List<Residence> {
            return residences.stream().filter { residence: T -> residence?.ownerClan != null }.collect(Collectors.toList())
        }
    }

    init {
        territoryOwningRewardsTable.load()
        scheduleRewardTask()
    }
}