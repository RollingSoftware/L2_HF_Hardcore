package com.l2j.datapack.custom.bots.farmzones

import com.l2jserver.common.noarg.NoArg
import com.l2jserver.common.primitives.Range

@NoArg
data class FarmZone(
    val name: String,
    val communityTeleport: String,
    val levels: Range
)