package com.l2j.datapack.custom.bots.characters

import com.l2j.datapack.helper.ScriptResourceResolver
import com.l2jserver.common.noarg.NoArg
import com.l2jserver.common.resources.YamlResource
import kotlin.random.Random

@NoArg
data class Nicknames(
    val nicknames: Set<String>
) {
    fun generateRandomNickname(): String {
        val baseName = randomBaseNickname()
        return when (NickDecoratorType.values().random()) {
            NickDecoratorType.SYMBOL -> wrapInPaddingSymbols(baseName)
            NickDecoratorType.NUMERIC -> baseName + generateNumericSuffix()
            NickDecoratorType.NONE -> baseName
        }
    }

    private fun generateNumericSuffix(): String {
        val size = (1..4).random()
        val number = Random.nextInt(9999).toString()
        if (number.length < size) {
            return number.padStart(size, '0')
        }
        return number
    }

    private fun wrapInPaddingSymbols(nickname: String): String {
        val symbol = paddingSymbols.random()

        val paddingSize = (1..3).random()
        val padding = (1..paddingSize).map {
            when (TransformSymbol.values().random()) {
                TransformSymbol.UPPER -> symbol.uppercase()
                TransformSymbol.LOWER -> symbol.lowercase()
                TransformSymbol.SKIP -> symbol
            }
        }.joinToString("")

        val prefix = if (Random.nextBoolean()) padding else ""
        val suffix = if (Random.nextBoolean()) padding else ""
        return prefix + nickname + suffix
    }

    private fun randomBaseNickname(): String =
        nicknames.randomOrNull() ?: "Defaults"

    companion object {
        fun loadFromFile(): Nicknames {
            val resourcePath = "./nicknames.yml"
            val nicknamesPayload = YamlResource.loadFile(
                ScriptResourceResolver.resolve(Nicknames::class.java, resourcePath),
                Nicknames::class.java
            )

            val processedNickNames = nicknamesPayload.nicknames.map { it.replace(" ", "") }.toSet()
            return Nicknames(processedNickNames)
        }
    }
}

val paddingSymbols = listOf("x", "y", "z", "s", "c", "a")

enum class TransformSymbol {
    UPPER, LOWER, SKIP
}

enum class NickDecoratorType {
    SYMBOL, NUMERIC, NONE
}