package com.l2j.datapack.custom.bots.manager

import com.l2j.datapack.custom.bots.manager.BotScheduleHelper.current
import java.time.LocalTime
import java.time.temporal.ChronoUnit

data class BotSchedule(
    val start: LocalTime,
    val end: LocalTime,
    val count: Int
)

object BotScheduleHelper {
    fun List<BotSchedule>.current(localTime: LocalTime = LocalTime.now()): BotSchedule {
        val now = localTime.truncatedTo(ChronoUnit.MINUTES)
        return first { it.start <= now && it.end >= now }
    }

    val default = listOf(
        BotSchedule(LocalTime.of(0, 0), LocalTime.of(8, 0), 15),
        BotSchedule(LocalTime.of(8, 0), LocalTime.of(12, 0), 35),
        BotSchedule(LocalTime.of(12, 0), LocalTime.of(16, 0), 50),
        BotSchedule(LocalTime.of(16, 0), LocalTime.of(18, 0), 60),
        BotSchedule(LocalTime.of(18, 0), LocalTime.of(21, 0), 90),
        BotSchedule(LocalTime.of(21, 0), LocalTime.of(23, 59), 80),
    )
}

fun main() {
    (0..23).forEach { hours ->
        (0..59).forEach { minutes ->
            val time = LocalTime.of(hours, minutes).truncatedTo(ChronoUnit.MINUTES)
            println(BotScheduleHelper.default.current(time))
        }
    }
}