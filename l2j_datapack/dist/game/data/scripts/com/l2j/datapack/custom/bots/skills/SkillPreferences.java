package com.l2j.datapack.custom.bots.skills;

import com.google.common.collect.Lists;
import java.util.List;

public class SkillPreferences {

    public static final SkillPreferences DEFAULT_MAGE = new SkillPreferences(
            Lists.newArrayList(
                    new SkillParameter(1177, 100) // Wind Strike
            )
    );

    public static final SkillPreferences DEFAULT_FIGHTER = new SkillPreferences(
            Lists.newArrayList(
                    new SkillParameter(3, 50) // Power Strike
            )
    );

    public static final SkillPreferences DEFAULT_KAMAEL = new SkillPreferences(
            Lists.newArrayList(
                    new SkillParameter(468, 50), // Fallen Attack
                    new SkillParameter(1431, 50) // Fallen Arrow
            )
    );

    private List<SkillParameter> attackSkillIds;

    public SkillPreferences() {
    }

    public SkillPreferences(List<SkillParameter> attackSkillIds) {
        this.attackSkillIds = attackSkillIds;
    }

    public List<SkillParameter> getAttackSkillIds() {
        return attackSkillIds;
    }
}
