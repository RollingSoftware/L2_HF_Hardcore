package com.l2j.datapack.custom.bots.equipment

import com.fasterxml.jackson.core.type.TypeReference
import com.l2j.datapack.helper.ScriptResourceResolver
import com.l2jserver.common.resources.YamlResource
import org.apache.logging.log4j.LogManager

class EquipmentTable {
    private val log = LogManager.getLogger()

    private val equipmentPreferenceByName: Map<String, EquipmentPreferences> = loadFromFile()

    private fun loadFromFile(): Map<String, EquipmentPreferences> {
        val resourcePath = "./equipment-preferences.yaml"
        return try {
            val result = YamlResource.loadFile(
                ScriptResourceResolver.resolve(javaClass, resourcePath),
                object : TypeReference<Map<String, EquipmentPreferences>>() {}
            )
            log.info("{} equipment preferences loaded", result.size)
            result
        } catch (e: RuntimeException) {
            log.warn("Failed to load equipment preferences from $resourcePath, falling back to empty table", e)
            HashMap()
        }
    }

    fun findPreference(group: String): EquipmentPreferences =
        equipmentPreferenceByName[group] ?: throw IllegalArgumentException("There is no '$group' preferences group")
}