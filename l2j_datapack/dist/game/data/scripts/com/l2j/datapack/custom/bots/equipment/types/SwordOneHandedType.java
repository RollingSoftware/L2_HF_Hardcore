package com.l2j.datapack.custom.bots.equipment.types;

import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.type.WeaponType;

public class SwordOneHandedType implements EquippableWeaponType {

    public static final SwordOneHandedType INSTANCE = new SwordOneHandedType();

    @Override
    public boolean typeOf(L2Weapon weapon) {
        return weapon.getItemType() == weaponType() && weapon.isOneHanded();
    }

    @Override
    public String name() {
        return "SwordOneHanded";
    }

    @Override
    public WeaponType weaponType() {
        return WeaponType.SWORD;
    }
}
