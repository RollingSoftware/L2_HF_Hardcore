package com.l2j.datapack.handlers.communityboard.custom.boards.teleport;

import com.l2jserver.common.noarg.NoArg
import com.l2jserver.gameserver.model.Location

@NoArg
data class TeleportDestination(val code: String, val coordinates: Location)
