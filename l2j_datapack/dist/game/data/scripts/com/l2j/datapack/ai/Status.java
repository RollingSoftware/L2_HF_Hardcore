package com.l2j.datapack.ai;

public interface Status {

    int getId();
    int ordinal();
    String getName();

}
