package com.l2j.datapack.custom.bots.manager

import com.l2j.datapack.custom.bots.ai.BotAi
import com.l2j.datapack.custom.bots.ai.BotState
import com.l2j.datapack.custom.bots.behaviour.ClassBehaviourTable
import com.l2j.datapack.custom.bots.characters.CharacterCreator
import com.l2j.datapack.custom.bots.equipment.EquipmentTable
import com.l2j.datapack.custom.bots.farmzones.FarmZonesTable
import com.l2j.datapack.custom.bots.manager.BotScheduleHelper.current
import com.l2jserver.common.monitoring.MetricsManager
import com.l2jserver.gameserver.LoginServerThread
import com.l2jserver.gameserver.model.L2World
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.network.L2GameClient
import com.l2jserver.gameserver.network.clientpackets.EnterWorld
import com.l2jserver.gameserver.network.clientpackets.Logout
import io.micrometer.core.instrument.Counter
import org.apache.logging.log4j.LogManager
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean

class BotManagerRunnable(
    private val characterCreator: CharacterCreator,
    private val farmZonesTable: FarmZonesTable,
    private val classBehaviourTable: ClassBehaviourTable,
    private val botDao: BotDao,
    private val botManagerConfig: BotManagerConfig,
    private val serverStarted: AtomicBoolean,
    private val equipmentTable: EquipmentTable
) : Runnable {
    private val log = LogManager.getLogger()
    private val activeBots = ConcurrentHashMap<Int, L2PcInstance>()

    private val botsMissingDataCounter: Counter = MetricsManager.instance.metricsRegistry.counter("l2j_bots_missing_data")
    private val botsCompletedTaskCounter: Counter = MetricsManager.instance.metricsRegistry.counter("l2j_bots_completed_task")
    private val botsRanOutOfMoneyCounter: Counter = MetricsManager.instance.metricsRegistry.counter("l2j_bots_ran_out_of_money")
    private val botsStuckCounter: Counter = MetricsManager.instance.metricsRegistry.counter("l2j_bots_stuck")
    private val botsCreated: Counter = MetricsManager.instance.metricsRegistry.counter("l2j_bots_created")

    val cancelled = AtomicBoolean(false)

    private fun loginPlayer(player: L2PcInstance) {
        player.isBot = true
        L2World.getInstance().addPlayerToWorld(player)
        val enterWorld = EnterWorld()
        enterWorld.client = player.client
        enterWorld.run(player)
        player.setOnlineStatus(true, true)
        player.setIsRunning(true)
    }

    private fun logoutPlayer(player: L2PcInstance) {
        val logout = Logout()
        logout.client = player.client
        logout.run()
        player.logout(true)
        player.deleteMe()
    }

    private fun startBotAi(bot: L2PcInstance) {
        val ai = BotAi(bot, farmZonesTable, classBehaviourTable, equipmentTable)
        bot.setAiSupplier { ai.startAi(); ai }
        bot.ai = ai
        ai.startAi()
    }

    data class FuzzyContainer(val range: IntRange) {

        var lastFuzzy: Long = System.currentTimeMillis()
        var lastValue = range.random()
        private val threshold = 5 * 60 * 1000

        fun get(): Int {
            val delta = System.currentTimeMillis() - lastFuzzy
            if (delta > threshold) {
                lastValue = range.random()
                lastFuzzy = System.currentTimeMillis()
            }
            return lastValue
        }

    }

    val fuzzyContainer = FuzzyContainer(0..2)

    // TODO Bot loader sometimes loads wrong bot data
    override fun run() {
        if (cancelled.get()) {
            log.info("Task is cancelled, logging all {} bots off", activeBots.size)
            activeBots.values.forEach {
                logoutPlayer(it)
                activeBots.remove(it.objectId)
            }
            return
        }

        if (!serverStarted.get()) {
            log.debug("Server is not yet stared, bot manager will rest")
            return
        }

        if (!LoginServerThread.getInstance().isConnected) {
            log.debug("Login Server is not currently connected, bot manager will rest")
            return
        }

        activeBots.forEach { (id, bot) ->
            val ai = bot.ai as BotAi
            if (((ai.botGoal.check() && ai.isNotFighting()) || ai.botState in listOf(BotState.STUCK, BotState.NO_MONEY))) {
                ai.stopAi()
                logoutPlayer(bot)
                L2GameClient.deleteCharByObjId(id)
                botDao.delete(listOf(id))

                if (ai.botGoal.check()) {
                    log.debug("Bot {} was deleted because he did what he had to", bot)
                    botsCompletedTaskCounter.increment()
                } else {
                    log.debug("Bot {} was deleted because he was in '{}' state", bot, ai.botState)
                    if (ai.botState == BotState.STUCK) {
                        botsStuckCounter.increment()
                    } else if (ai.botState == BotState.NO_MONEY) {
                        botsRanOutOfMoneyCounter.increment()
                    }
                }

                activeBots.remove(id)
            }
        }

        val targetBotCount = if (botManagerConfig.targetBotCount > -1) {
            botManagerConfig.targetBotCount
        } else {
            botManagerConfig.botSchedule.current().count + fuzzyContainer.get()
        }

        when {
            activeBots.size > targetBotCount -> {
                val botOverflowCount = activeBots.size - targetBotCount
                val botsToKill = activeBots.values.take(botOverflowCount)
                botsToKill.forEach { bot ->
                    val ai = bot.ai as BotAi
                    if (ai.isNotFighting()) {
                        activeBots.remove(bot.objectId)
                        ai.stopAi()
                        logoutPlayer(bot)
                    }
                }
            }
            activeBots.size < targetBotCount -> {
                val openBotSlots = targetBotCount - activeBots.size
                val amountOfBotsToGetOnline = if (openBotSlots < botManagerConfig.botsPerRun) {
                    botManagerConfig.botsPerRun - openBotSlots
                } else {
                    botManagerConfig.botsPerRun
                }

                val activeBotIds = activeBots.keys().toList()
                val existingNotActiveBots = botDao.findAllExceptForIdsLimit(activeBotIds, amountOfBotsToGetOnline)

                val existingBots = existingNotActiveBots.map {
                    Pair(it.characterId, L2PcInstance.load(it.characterId))
                }

                val (botsThatCouldNotBeLoaded, loadedBots) = existingBots.partition { it.second == null }
                if (botsThatCouldNotBeLoaded.isNotEmpty()) {
                    log.debug("Data is missing for {} bots", botsThatCouldNotBeLoaded.size)
                    botDao.delete(botsThatCouldNotBeLoaded.map { it.first })
                    botsMissingDataCounter.increment(botsThatCouldNotBeLoaded.size.toDouble())
                }

                if (loadedBots.isNotEmpty()) {
                    loadedBots.forEach { (objectId, bot) ->
                        activeBots[objectId] = bot
                        CharacterCreator.injectFakeClient(bot)
                        loginPlayer(bot)
                        startBotAi(bot)
                    }
                    log.debug("Logged in {} existing bots", loadedBots.size)
                }

                val amountOfBotsToCreate = amountOfBotsToGetOnline - existingBots.size
                val newBots = (1..amountOfBotsToCreate).map {
                    val newBot = characterCreator.createRandomCharacter()
                    activeBots[newBot.objectId] = newBot
                    newBot
                }
                botDao.create(newBots.map { it.objectId })

                if (newBots.isNotEmpty()) {
                    botsCreated.increment(newBots.size.toDouble())
                    log.debug("Generated {} new bots and stored their ids", newBots.size)
                }

                newBots.forEach {
                    loginPlayer(it)
                    startBotAi(it)
                }
                log.debug(
                    "Logged in and started {} new bots AI out of {}",
                    activeBots.size,
                    targetBotCount
                )
            }
            else -> {
                log.trace(
                    "Currently there is enough bots active {} out of {}, bot manager will rest",
                    activeBots.size,
                    targetBotCount
                )
            }
        }
    }

}