package com.l2j.datapack.handlers.communityboard.custom;

import com.l2jserver.Config;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

public class CustomHomeBoard implements IParseBoardHandler {

    public static final String HTML_ROOT = "data/html/CommunityBoard/custom";

    @Override
    public String[] getCommunityBoardCommands() {
        return new String[]{
                "_bbshome",
                "_bbstop"
        };
    }

    @Override
    public boolean parseCommunityBoardCommand(String command, L2PcInstance player) {
        if (command.equals("_bbshome") || command.startsWith("_bbstop")) {
            String menuTemplate = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), HTML_ROOT + "/menu_template.html");
            String homeTemplate = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), HTML_ROOT + "/home/custom_home.html");

            homeTemplate = homeTemplate.replace("%server_name%", Config.SERVER_NAME);

            CommunityBoardHandler.separateAndSend(menuTemplate.replace("%page_content%", homeTemplate), player);
            return true;
        }
        return false;
    }

}
