package com.l2j.datapack.handlers.communityboard.custom.boards.blacksmith

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs
import com.l2j.datapack.handlers.communityboard.custom.BoardCondition
import com.l2j.datapack.handlers.communityboard.custom.CustomHomeBoard
import com.l2j.datapack.handlers.communityboard.custom.NotificationHelper
import com.l2jserver.gameserver.cache.HtmCache
import com.l2jserver.gameserver.data.xml.impl.MultisellData
import com.l2jserver.gameserver.handler.CommunityBoardHandler
import com.l2jserver.gameserver.handler.IParseBoardHandler
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.network.serverpackets.ExShowVariationCancelWindow
import com.l2jserver.gameserver.network.serverpackets.ExShowVariationMakeWindow
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class BlacksmithBoard : IParseBoardHandler {
    val log: Logger = LogManager.getLogger()

    override fun parseCommunityBoardCommand(command: String, player: L2PcInstance): Boolean {
        if (!command.startsWith("_bbs_blacksmith_board")) {
            return false
        }

        val result = BoardCondition.checkCondition(player)
        if (result.isFailure) {
            NotificationHelper.notify(player, result.result)
            return false
        }

        if (player.karma > 0) {
            NotificationHelper.notify(player, player.string("karma_players_cannot_use_the_blacksmith"))
            return false
        }

        val actionArgs = ActionArgs.parse(command)
        return actionArgs.map {
            when (it.actionName) {
                "augment" -> handleAugmentation(player, it)
                "multisell" -> handleMultisell(player, it)
                "exc_multisell" -> handleExcMultisell(player, it)
                else -> false
            }
        }.orElseGet {
            val html =
                HtmCache.getInstance()
                    .getHtm(player.htmlPrefix, CustomHomeBoard.HTML_ROOT + "/blacksmith/blacksmith.html")

            val menuTemplate = HtmCache.getInstance().getHtm(
                player.htmlPrefix,
                CustomHomeBoard.HTML_ROOT + "/menu_template.html"
            )
            CommunityBoardHandler.separateAndSend(menuTemplate.replace("%page_content%", html), player)
            true
        }
    }

    private fun handleMultisell(player: L2PcInstance, actionArgs: ActionArgs): Boolean {
        if (actionArgs.args.size != 1) {
            return false
        }
        val multisellId = actionArgs.args.first().toInt()
        if (multisellId !in knownMultisells) {
            return false
        }

        MultisellData.getInstance().handleMultisell(
            multisellId,
            player,
            null,
            false,
            1.0,
            1.0
        )
        return true
    }

    private fun handleExcMultisell(player: L2PcInstance, actionArgs: ActionArgs): Boolean {
        if (actionArgs.args.size != 1) {
            return false
        }
        val multisellId = actionArgs.args.first().toInt()
        if (multisellId !in knownMultisells) {
            return false
        }

        MultisellData.getInstance().handleMultisell(
            multisellId,
            player,
            null,
            true,
            1.0,
            1.0
        )
        return true
    }

    private fun handleAugmentation(player: L2PcInstance, actionArgs: ActionArgs): Boolean {
        if (actionArgs.args.size != 1) {
            return false
        }
        return when (actionArgs.args.first().toInt()) {
            1 -> {
                player.sendPacket(ExShowVariationMakeWindow.STATIC_PACKET)
                true
            }
            2 -> {
                player.sendPacket(ExShowVariationCancelWindow.STATIC_PACKET)
                true
            }
            else -> false
        }
    }

    override fun getCommunityBoardCommands(): Array<String> = arrayOf(
        "_bbs_blacksmith_board"
    )

    val knownMultisells = listOf(
        1004,
        1008,
        323470004,
        326150006,
        326150008,
        323470003,
        372000001,
        372000000,
        371000001,
        371000000,
        370000001,
        370000000
    )
}