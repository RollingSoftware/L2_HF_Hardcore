package com.l2j.datapack.custom.bots.states

import com.l2j.datapack.custom.skills.SkillTable
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.items.type.CrystalType
import com.l2jserver.gameserver.model.items.type.WeaponType

object KamaelChangeWeaponHelper {

    fun changeWeapon(player: L2PcInstance): Boolean {
        if (player.isKamael) {
            val changeWeaponSkill = player.skills[SkillTable.changeWeaponSkillId]

            if (player.activeWeaponItem != null &&
                player.activeWeaponItem.changeWeaponId > 0 &&
                player.activeWeaponItem.itemGrade.isGreater(CrystalType.NONE) &&
                player.activeWeaponItem.itemType in listOf(WeaponType.SWORD, WeaponType.BOW) &&
                changeWeaponSkill != null
            ) {
                player.doCast(changeWeaponSkill)
                return true
            }
        }
        return false
    }

}