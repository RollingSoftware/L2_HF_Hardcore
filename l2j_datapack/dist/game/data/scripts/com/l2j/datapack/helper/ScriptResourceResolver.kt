package com.l2j.datapack.helper

import com.l2jserver.gameserver.scripting.ScriptEngineManager
import java.io.File

class ScriptResourceResolver {
    companion object {
        @JvmStatic
        fun resolve(clazz: Class<*>, path: String): String {
            return File(
                File(
                    ScriptEngineManager.SCRIPT_RESOURCE_ROOT,
                    "/" + clazz.packageName.replace(".", "/"),
                ),
                path
            ).canonicalPath
        }
    }
}