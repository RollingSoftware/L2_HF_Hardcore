package com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions;

import com.l2j.datapack.handlers.communityboard.custom.ActionArgs;
import com.l2j.datapack.handlers.communityboard.custom.BoardAction;
import com.l2j.datapack.handlers.communityboard.custom.BoardCondition;
import com.l2j.datapack.handlers.communityboard.custom.CommunityPaymentHelper;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.Buffs;
import com.l2jserver.Config;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.FakeCast;
import com.l2jserver.gameserver.model.target.TargetHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class PresetBuffAction implements BoardAction {

    private final static Logger LOG = LogManager.getLogger();

    private final Buffs buffs;

    public PresetBuffAction(Buffs buffs) {
        this.buffs = buffs;
    }

    @Override
    public ProcessResult process(L2PcInstance player, ActionArgs args) {
        if (args.getArgs().size() != 1) {
            LOG.warn("{} is trying to use buff preset with invalid args number {}", player, args.getArgs().size());
            return ProcessResult.failure(player.string("invalid_preset_buff_request"));
        }

        TargetHolder targetHolder = TargetHolder.parseTarget(player, args.getArgs().get(0));

        ProcessResult checkResult = BoardCondition.checkCondition(player);
        if (checkResult.isFailure()) {
            return checkResult;
        }

        ProcessResult paymentResult = CommunityPaymentHelper.payForService(player, Config.COMMUNITY_DEFAULT_PRESET_PRICE);
        if (paymentResult.isFailure()) {
            return paymentResult;
        }

        int delay = (int) (GameTimeController.TICKS_PER_SECOND * GameTimeController.MILLIS_IN_TICK * Config.COMMUNITY_BUFF_SPEED);
        if (targetHolder.isSummonTarget()) {
            List<FakeCast> fakeCasts = buffs.getBuffs()
                    .stream()
                    .map(buff -> new FakeCast(buff, delay, () -> buff.getSkill().applyEffects(targetHolder.getMaster(), targetHolder.getSummon())))
                    .collect(Collectors.toList());
            targetHolder.getMaster().fakeCastSeries(fakeCasts, targetHolder.getSummon());
        } else {
            List<FakeCast> fakeCasts = buffs.getBuffs()
                    .stream()
                    .map(buff -> new FakeCast(buff, delay, () -> buff.getSkill().applyEffects(targetHolder.getMaster(), targetHolder.getMaster())))
                    .collect(Collectors.toList());
            targetHolder.getMaster().fakeCastSeries(fakeCasts, targetHolder.getMaster());
        }

        return ProcessResult.success();
    }

}
