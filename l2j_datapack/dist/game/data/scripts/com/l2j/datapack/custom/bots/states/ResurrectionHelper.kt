package com.l2j.datapack.custom.bots.states

import com.l2jserver.gameserver.GameTimeController
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.network.clientpackets.RequestRestartPoint

class ResurrectionHelper(val player: L2PcInstance) {
    private val resurrectionDelay = 2000L
    private var resurrectTick = 0

    fun isDead(): Boolean = player.isDead || player.currentHp <= 0.0

    fun checkAndResurrect(): Boolean {
        if (!isDead()) {
            return false
        }

        if (resurrectTick > 0 && resurrectTick < GameTimeController.getInstance().gameTicks) {
            val requestRestartPoint = RequestRestartPoint()
            requestRestartPoint.client = player.client
            requestRestartPoint.run()
            resurrectTick = 0
            return true
        } else if (resurrectTick <= 0) {
            setNewResurrectionTick()
            return false
        }

        return false
    }

    private fun setNewResurrectionTick() {
        resurrectTick = GameTimeController.getInstance().getFutureGameTicks(resurrectionDelay)
    }
}