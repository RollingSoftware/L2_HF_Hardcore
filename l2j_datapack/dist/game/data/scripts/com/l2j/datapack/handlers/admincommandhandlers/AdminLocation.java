package com.l2j.datapack.handlers.admincommandhandlers;

import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AdminLocation implements IAdminCommandHandler {

    private static final Logger LOG = LogManager.getLogger();

    private static final String[] ADMIN_COMMANDS = {
            "admin_location_record"
    };

    @Override
    public boolean useAdminCommand(String command, L2PcInstance activeChar) {
        LOG.info("""
                                
                - x: {}
                  y: {}
                  z: {}""", activeChar.getLocation().getX(), activeChar.getLocation().getY(), activeChar.getLocation().getZ());
        return true;
    }

    @Override
    public String[] getAdminCommandList() {
        return ADMIN_COMMANDS;
    }
}
