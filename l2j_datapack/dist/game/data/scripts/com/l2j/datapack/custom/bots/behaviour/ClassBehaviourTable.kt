package com.l2j.datapack.custom.bots.behaviour

import com.fasterxml.jackson.core.type.TypeReference
import com.l2j.datapack.helper.ScriptResourceResolver.Companion.resolve
import com.l2jserver.common.resources.YamlResource
import com.l2jserver.gameserver.data.xml.impl.CategoryData
import com.l2jserver.gameserver.enums.CategoryType
import com.l2jserver.gameserver.model.base.ClassId
import org.apache.logging.log4j.LogManager

class ClassBehaviourTable {
    private val log = LogManager.getLogger()

    private val classBehaviourMap: Map<Int, ClassBehaviour> = loadFromFile()

    private fun loadFromFile(): Map<Int, ClassBehaviour> {
        val resourcePath = "./class_behaviour.yml"
        return try {
            val result = YamlResource.loadFile(
                resolve(javaClass, resourcePath),
                object : TypeReference<Map<Int, ClassBehaviour>>() {}
            )
            log.info("Behaviours loaded")
            result
        } catch (e: RuntimeException) {
            log.warn("Failed to load class behaviour from $resourcePath, falling back to empty table", e)
            HashMap()
        }
    }

    fun findBehaviour(classId: ClassId): ClassBehaviour {
        val depthSafeguard = 4
        var depthCounter = 0
        var classBehaviour: ClassBehaviour? = null
        while (depthCounter < depthSafeguard) {
            classBehaviour = classBehaviourMap[classId.id]
            if (classBehaviour != null) {
                break
            }
            depthCounter += 1
        }
        return classBehaviour
            ?: when {
                CategoryData.getInstance().getCategoryByType(CategoryType.KAMAEL_ALL_CLASS).contains(classId.id) -> {
                    ClassBehaviour.DEFAULT_KAMAEL
                }
                CategoryData.getInstance().getCategoryByType(CategoryType.MAGE_GROUP).contains(classId.id) -> {
                    ClassBehaviour.DEFAULT_MAGE
                }
                else -> {
                    ClassBehaviour.DEFAULT_FIGHTER
                }
            }
    }
}