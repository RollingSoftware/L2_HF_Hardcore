/*
 * Copyright (C) 2004-2016 L2J DataPack
 *
 * This file is part of L2J DataPack.
 *
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2j.datapack.instances;

import com.l2j.datapack.instances.CrystalCaverns.CrystalCaverns;
import com.l2j.datapack.instances.DarkCloudMansion.DarkCloudMansion;
import com.l2j.datapack.instances.DisciplesNecropolisPast.DisciplesNecropolisPast;
import com.l2j.datapack.instances.ElcadiasTent.ElcadiasTent;
import com.l2j.datapack.instances.HideoutOfTheDawn.HideoutOfTheDawn;
import com.l2j.datapack.instances.IceQueensCastle.IceQueensCastle;
import com.l2j.datapack.instances.JiniaGuildHideout1.JiniaGuildHideout1;
import com.l2j.datapack.instances.JiniaGuildHideout2.JiniaGuildHideout2;
import com.l2j.datapack.instances.JiniaGuildHideout3.JiniaGuildHideout3;
import com.l2j.datapack.instances.JiniaGuildHideout4.JiniaGuildHideout4;
import com.l2j.datapack.instances.LibraryOfSages.LibraryOfSages;
import com.l2j.datapack.instances.MithrilMine.MithrilMine;
import com.l2j.datapack.instances.MonasteryOfSilence1.MonasteryOfSilence1;
import com.l2j.datapack.instances.NornilsGarden.NornilsGarden;
import com.l2j.datapack.instances.NornilsGardenQuest.NornilsGardenQuest;
import com.l2j.datapack.instances.SanctumOftheLordsOfDawn.SanctumOftheLordsOfDawn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Instance class-loader.
 *
 * @author FallenAngel
 */
public final class InstanceLoader {
    private static final Logger LOG = LogManager.getLogger();

    private static final Class<?>[] SCRIPTS = {
            CrystalCaverns.class,
            DarkCloudMansion.class,
            DisciplesNecropolisPast.class,
            ElcadiasTent.class,
            HideoutOfTheDawn.class,
            IceQueensCastle.class,
            JiniaGuildHideout1.class,
            JiniaGuildHideout2.class,
            JiniaGuildHideout3.class,
            JiniaGuildHideout4.class,
            LibraryOfSages.class,
            MithrilMine.class,
            MonasteryOfSilence1.class,
            NornilsGarden.class,
            NornilsGardenQuest.class,
            SanctumOftheLordsOfDawn.class,
    };

    public InstanceLoader() {
        LOG.info(InstanceLoader.class.getSimpleName() + ": Loading Instances scripts.");
        for (Class<?> script : SCRIPTS) {
            try {
                script.getConstructor().newInstance();
            } catch (Exception e) {
                LOG.error(InstanceLoader.class.getSimpleName() + ": Failed loading " + script.getSimpleName() + ":", e);
            }
        }
    }
}
