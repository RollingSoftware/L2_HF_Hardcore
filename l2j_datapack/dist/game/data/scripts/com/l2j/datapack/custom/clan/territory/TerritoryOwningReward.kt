package com.l2j.datapack.custom.clan.territory

import com.fasterxml.jackson.annotation.JsonProperty
import com.l2jserver.common.noarg.NoArg
import com.l2jserver.gameserver.model.holders.ItemHolder

@NoArg
data class TerritoryOwningReward(
    @get:JsonProperty("residence_id") val residenceId: Int = 0,
    val rewards: List<ItemHolder>
)