package com.l2j.datapack.handlers.communityboard.custom.boards.buff;

import com.l2j.datapack.handlers.communityboard.custom.*;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.actions.*;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.sets.*;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.bufflists.sets.presets.*;
import com.l2j.datapack.handlers.communityboard.custom.boards.buff.renderers.BuffCategoriesRender;
import com.l2jserver.Config;
import com.l2jserver.common.DecimalFormatStandard;
import com.l2jserver.common.monad.ProcessResult;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.CommunityBuffList;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class BuffBoard implements IParseBoardHandler {

    private static final Logger LOG = LogManager.getLogger();

    private final Map<String, BoardAction> actions;

    public BuffBoard() {
        actions = new HashMap<>();
        actions.put("restore", new BoardRestoreAction());
        actions.put("cancel", new BoardCancelAction());
        actions.put("tank_buff", new PresetBuffAction(new TankBuff()));
        actions.put(TankFullBuff.NAME, new PresetBuffAction(new TankFullBuff()));
        actions.put("tank_song_dance", new PresetBuffAction(new TankDanceAndSong()));
        actions.put("fighter_buff", new PresetBuffAction(new FighterBuff()));
        actions.put(FighterFullBuff.NAME, new PresetBuffAction(new FighterFullBuff()));
        actions.put("fighter_song_dance", new PresetBuffAction(new FighterDanceAndSong()));
        actions.put("mage_buff", new PresetBuffAction(new MageBuff()));
        actions.put(MageFullBuff.NAME, new PresetBuffAction(new MageFullBuff()));
        actions.put("mage_song_dance", new PresetBuffAction(new MageDanceAndSong()));

        Map<String, BoardAction> listBuffActions = new HashMap<>();
        listBuffActions.put("prophet", new ShowBuffListAction("prophet", new Hierophant()));
        listBuffActions.put("dances", new ShowBuffListAction("dances", new SpectralDancer()));
        listBuffActions.put("songs", new ShowBuffListAction("songs", new SwordMuse()));
        listBuffActions.put("elven_elder", new ShowBuffListAction("elven_elder", new ElvenSaint()));
        listBuffActions.put("shillen_elder", new ShowBuffListAction("shillen_elder", new ShillenSaint()));
        listBuffActions.put("overlord", new ShowBuffListAction("overlord", new Dominator()));
        listBuffActions.put("warcryer", new ShowBuffListAction("warcryer", new Doomcryer()));
        listBuffActions.put("others", new ShowBuffListAction("others", new Others()));


        actions.put("list_buff", new RouteAction(listBuffActions));

        actions.put("buff", new BuffHandlerAction(AllBuffs.getBuffMap()));

        actions.put("delete_preset", new DeletePresetAction());
        actions.put("create_preset", new CreatePresetAction());
        actions.put("create_preset_dialog", new CreatePresetDialogAction());
        actions.put("update_preset", new UpdatePresetAction());

        actions.put("remove_preset_buff", new RemovePresetBuffAction());
        actions.put("add_preset_buff", new AddPresetBuffAction());

        actions.put("buff_preset", new PlayerCustomBuffAction());
        actions.put("add_current_to_preset", new AddCurrentBuffsToPresetAction());

        Map<String, BoardAction> listAddToPresetBuffActions = new HashMap<>();
        listAddToPresetBuffActions.put("prophet", new ShowAddToPresetBuffAction(new Hierophant()));
        listAddToPresetBuffActions.put("dances", new ShowAddToPresetBuffAction(new SpectralDancer()));
        listAddToPresetBuffActions.put("songs", new ShowAddToPresetBuffAction(new SwordMuse()));
        listAddToPresetBuffActions.put("elven_elder", new ShowAddToPresetBuffAction(new ElvenSaint()));
        listAddToPresetBuffActions.put("shillen_elder", new ShowAddToPresetBuffAction(new ShillenSaint()));
        listAddToPresetBuffActions.put("overlord", new ShowAddToPresetBuffAction(new Dominator()));
        listAddToPresetBuffActions.put("warcryer", new ShowAddToPresetBuffAction(new Doomcryer()));
        listAddToPresetBuffActions.put("others", new ShowAddToPresetBuffAction(new Others()));

        actions.put("list_add_to_preset_buff", new RouteAction(listAddToPresetBuffActions));
    }

    @Override
    public boolean parseCommunityBoardCommand(String command, L2PcInstance player) {
        if (command.equals("_bbs_buff_board")) {
            String html = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), CustomHomeBoard.HTML_ROOT + "/buff/buff.html");

            if (Config.COMMUNITY_RESTORE_ENABLED) {
                String healButton = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), CustomHomeBoard.HTML_ROOT + "/buff/buff_restoration_button.html");
                html = html.replace("%restore_button%", healButton);
            } else {
                html = html.replace("%restore_button%", "");
            }

            if (CommunityPaymentHelper.checkFreeService(player)) {
                String free = player.string("free");
                html = html.replace("%single_buff_price%", free);
                html = html.replace("%preset_buff_price%", free);
            } else {
                html = html.replace("%single_buff_price%", DecimalFormatStandard.moneyFormat().format(Config.COMMUNITY_SINGLE_BUFF_PRICE));
                html = html.replace("%preset_buff_price%", DecimalFormatStandard.moneyFormat().format(Config.COMMUNITY_DEFAULT_PRESET_PRICE));
            }

            List<CommunityBuffList> presets = DAOFactory.getInstance().getCommunityBuffListDao().findAllCommunityBuffSets(player.getObjectId());
            List<String> buffPresetNames = presets.stream().map(CommunityBuffList::getName).collect(Collectors.toList());

            html = html.replace("%user_buff_presets%", String.join(";", buffPresetNames));
            html = html.replace("%buff_list%", BuffCategoriesRender.renderBuffCategoriesList("list_buff", player));

            String menuTemplate = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), CustomHomeBoard.HTML_ROOT + "/menu_template.html");
            CommunityBoardHandler.separateAndSend(menuTemplate.replace("%page_content%", html), player);
            return true;
        } else if (command.startsWith("_bbs_buff")) {
            Optional<ActionArgs> actionArgsOption = ActionArgs.parse(command);

            if (actionArgsOption.isEmpty()) {
                return false;
            }

            CommunityBoardHandler.getInstance().addBypass(player, "Buff", command);

            ActionArgs actionArgs = actionArgsOption.get();
            String actionName = actionArgs.getActionName();
            BoardAction action = actions.get(actionName);
            if (action == null) {
                LOG.warn("Illegal action selected {}", actionName);
                return false;
            }

            ProcessResult result = action.process(player, actionArgs);
            if (result.isFailure()) {
                NotificationHelper.notify(player, result.getResult());
                return false;
            }
            return true;
        }

        return false;
    }

    @Override
    public String[] getCommunityBoardCommands() {
        return new String[]{
                "_bbs_buff_board",
                "_bbs_buff",
        };
    }

}