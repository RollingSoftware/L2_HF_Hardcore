package com.l2j.datapack.custom.bots.states

import com.l2j.datapack.custom.bots.equipment.EquipmentManager
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance


class EquipmentManagementState(
    private val player: L2PcInstance,
    private val equipmentManager: EquipmentManager,
    private val equipmentManagementDelay: Int = 5000,
    equipmentManagementDelayOffset: Int = 1000
) {
    private var lastActionTime = 0L

    private val offsetRange = (-equipmentManagementDelayOffset..equipmentManagementDelayOffset)

    fun manageEquipment() {
        val actionDelta = System.currentTimeMillis() - lastActionTime
        if ((actionDelta > (equipmentManagementDelay + offsetRange.random())) && !player.isAttackingNow) {
            lastActionTime = System.currentTimeMillis()
            equipmentManager.manageEquipment()
        }
    }

}