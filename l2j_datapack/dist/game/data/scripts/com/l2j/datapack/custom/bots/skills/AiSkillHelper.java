package com.l2j.datapack.custom.bots.skills;

import com.l2jserver.common.util.Pair;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.skills.Skill;

import java.util.Optional;

public class AiSkillHelper {

    private final L2PcInstance character;
    private final SkillPreferences skillPreferences;

    public AiSkillHelper(L2PcInstance character, SkillPreferences skillPreferences) {
        this.character = character;
        this.skillPreferences = skillPreferences;
    }

    public Optional<Pair<SkillParameter, Skill>> findAppropriateSkill() {
        return skillPreferences.getAttackSkillIds()
                .stream()
                .map(it -> new Pair<>(it, character.getKnownSkill(it.getSkillId())))
                .filter(it -> it.getRight() != null)
                .findAny();
    }

}
