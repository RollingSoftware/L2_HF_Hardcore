package com.l2j.datapack.custom.bots.equipment

import com.l2jserver.gameserver.model.GradedArmorSet
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.findSetItemsInInventoryWithoutShield
import com.l2jserver.gameserver.model.getSetBodyPartWithoutShield
import com.l2jserver.gameserver.model.itemcontainer.Inventory
import com.l2jserver.gameserver.model.items.L2Item
import com.l2jserver.gameserver.model.items.L2Weapon
import com.l2jserver.gameserver.model.items.graded.Grade
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance
import com.l2jserver.gameserver.model.stats.Stats

class EquipmentManager(
    val player: L2PcInstance,
    var equipmentPreferences: EquipmentPreferences
) {

    private val armorSlots = listOf(
        EquipmentSlot(Inventory.PAPERDOLL_HEAD, L2Item.SLOT_HEAD),
        EquipmentSlot(Inventory.PAPERDOLL_LEGS, L2Item.SLOT_LEGS),
        EquipmentSlot(Inventory.PAPERDOLL_CHEST, L2Item.SLOT_CHEST),
        EquipmentSlot(Inventory.PAPERDOLL_FEET, L2Item.SLOT_FEET),
        EquipmentSlot(Inventory.PAPERDOLL_GLOVES, L2Item.SLOT_GLOVES)
    )

    private val jewelSlots = listOf(
        EquipmentSlot(Inventory.PAPERDOLL_LFINGER, L2Item.SLOT_LR_FINGER),
        EquipmentSlot(Inventory.PAPERDOLL_RFINGER, L2Item.SLOT_LR_FINGER),
        EquipmentSlot(Inventory.PAPERDOLL_LEAR, L2Item.SLOT_LR_EAR),
        EquipmentSlot(Inventory.PAPERDOLL_REAR, L2Item.SLOT_LR_EAR),
        EquipmentSlot(Inventory.PAPERDOLL_NECK, L2Item.SLOT_NECK)
    )

    private val enchantLevelComparator = Comparator.comparing { item: L2ItemInstance -> item.enchantLevel }.reversed()

    fun manageEquipment() {
        val availableGrades = Grade.gradesByLevel(player.level)
        val preferredSets = equipmentPreferences.getAllPreferredSets(player.level)

        unEquipGradePenaltyItems(availableGrades)
        equipBetterWeapon(availableGrades)
        equipBetterArmor(availableGrades, preferredSets)
        equipBetterShield(availableGrades, preferredSets)
        equipBetterJewels(availableGrades)
    }

    private fun unEquipGradePenaltyItems(availableGrades: Set<Grade>) {
        val penaltyItems = player.inventory.allEquippedItems.filter {
            Grade.fromCrystalType(it.item.itemGrade) !in availableGrades
        }
        penaltyItems.forEach {
            player.inventory.unEquipItemInSlot(it.locationSlot)
        }
    }

    private fun chainItemStatsComparators(stats: List<Stats>, defaultStat: Stats): Comparator<L2ItemInstance> {
        if (stats.isEmpty()) {
            return Comparator.comparing { it.getStatValue(player, defaultStat) }
        }
        return stats
            .map { stat ->
                Comparator.comparing { instance: L2ItemInstance -> instance.getStatValue(player, stat) }
            }.reduce { acc, next ->
                acc.thenComparing(next)
            }
    }

    private fun chainWeaponStatsComparators(stats: List<Stats>): Comparator<L2Weapon> {
        if (stats.isEmpty()) {
            return Comparator.comparing { it.getBaseStatValue(player, Stats.POWER_ATTACK) }
        }
        return stats
            .map { stat ->
                Comparator.comparing { instance: L2Weapon -> instance.getBaseStatValue(player, stat) }
            }.reduce { acc, next ->
                acc.thenComparing(next)
            }
    }

    private fun <T, Y> comparatorItemByListIndex(itemWeights: List<Y>, extractField: (t: T) -> Y): Comparator<T> {
        return Comparator.comparing { left: T -> itemWeights.indexOf(extractField(left)) }
    }

    fun findPreferredBetterWeapons(player: L2PcInstance, items: List<L2Weapon>): L2Weapon? {
        val availableGrades = Grade.gradesByLevel(player.level)

        val preferredWeaponTypes = equipmentPreferences.preferredWeaponTypes
        val preferredWeaponStats = equipmentPreferences.preferredWeaponStats
        val statComparator = chainWeaponStatsComparators(preferredWeaponStats).reversed()

        val equippedWeapon = player.inventory.getPaperdollItem(Inventory.PAPERDOLL_RHAND)?.weaponItem

        return if (equippedWeapon == null) {
            items.sortedWith(statComparator).firstOrNull()
        } else {
            items
                .filter { inventoryWeapon ->
                    preferredWeaponTypes.any { preferredWeaponType ->
                        preferredWeaponType.typeOf(inventoryWeapon)
                    }
                }.ifEmpty {
                    items
                }.filter {
                    Grade.fromCrystalType(it.itemGrade) in availableGrades && statComparator.compare(equippedWeapon, it) > 0
                }.sortedWith(statComparator).firstOrNull()
        }
    }

    private fun equipBetterWeapon(availableGrades: Set<Grade>) {
        val items = player.inventory.unequippedWeapons

        val preferredWeaponTypes = equipmentPreferences.preferredWeaponTypes
        val preferredWeaponStats = equipmentPreferences.preferredWeaponStats
        val statComparator = chainItemStatsComparators(preferredWeaponStats, Stats.POWER_ATTACK).reversed()
        val comparator = statComparator.thenComparing(enchantLevelComparator)

        val inventoryItemsSortedByStats = items
            .filter { inventoryWeapon ->
                preferredWeaponTypes.any { preferredWeaponType ->
                    preferredWeaponType.typeOf(inventoryWeapon.weaponItem)
                }
            }.ifEmpty {
                items
            }.filter {
                Grade.fromCrystalType(it.item.itemGrade) in availableGrades
            }.sortedWith(comparator)

        val equippedWeapon = player.inventory.getPaperdollItem(Inventory.PAPERDOLL_RHAND)
        compareAndEquip(inventoryItemsSortedByStats, equippedWeapon, comparator)
    }

    private fun equipBetterShield(
        availableGrades: Set<Grade>,
        preferredSets: List<GradedArmorSet>
    ) {
        val preferredShieldTypes = equipmentPreferences.preferredShieldTypes
        val preferredStat = Stats.SHIELD_DEFENCE

        if (player.isKamael) {
            return
        }

        val shieldPaperdollLocation = Inventory.PAPERDOLL_LHAND
        val equippedWeapon = player.inventory.getPaperdollItem(Inventory.PAPERDOLL_RHAND)
        val equippedItem = player.inventory.getPaperdollItem(shieldPaperdollLocation)

        if (equippedWeapon != null && equippedWeapon.item.isTwoHanded) {
            return
        }

        val currentlyEquippedSet: GradedArmorSet? = findEquippedPreferredSet(preferredSets)
        if (currentlyEquippedSet != null && currentlyEquippedSet.armorSet.shield.isNotEmpty()) {
            val setShield = player.inventory.allItems.firstOrNull { it.id in currentlyEquippedSet.armorSet.shield }
            if (setShield != null) {
                if (setShield.isNotEquipped) {
                    player.inventory.equipItem(setShield)
                }
                return
            }
        }

        val typesComparator = comparatorItemByListIndex(preferredShieldTypes) { item: L2ItemInstance ->
            item.armorItem.itemType
        }
        val statComparator = Comparator.comparing { item: L2ItemInstance -> item.getStatValue(player, preferredStat) }.reversed()
        val comparator = typesComparator.thenComparing(statComparator).thenComparing(enchantLevelComparator)

        val shieldsSortedByTypeAndStats = player.inventory
            .unequippedArmor
            .filter {
                Grade.fromCrystalType(it.item.itemGrade) in availableGrades &&
                        preferredShieldTypes.contains(it.armorItem.itemType)
            }
            .sortedWith(comparator)

        compareAndEquip(shieldsSortedByTypeAndStats, equippedItem, comparator)
    }

    fun findNonEquippedPreferredSets(
        preferredSets: List<GradedArmorSet> = equipmentPreferences.getAllPreferredSets(
            player.level
        )
    ): List<GradedArmorSet> {
        val currentlyEquippedSet: GradedArmorSet? = findEquippedPreferredSet(preferredSets)
        return if (currentlyEquippedSet != null) {
            preferredSets.filter {
                currentlyEquippedSet.grade < it.grade
            }.sortedBy { it.grade }
        } else {
            preferredSets
        }
    }

    private fun equipBetterArmor(
        availableGrades: Set<Grade>,
        preferredSets: List<GradedArmorSet>
    ) {
        val currentlyEquippedSet: GradedArmorSet? = findEquippedPreferredSet(preferredSets)
        val filteredPreferredSets = findNonEquippedPreferredSets(preferredSets)

        val setsInPlayerInventory = filteredPreferredSets.map {
            it.armorSet.findSetItemsInInventoryWithoutShield(player)
        }.filter { it.isNotEmpty() }

        val setToEquip = setsInPlayerInventory.firstOrNull()
        val equippedSetBodyParts = setToEquip?.map {
            if (!it.isEquipped) {
                player.useEquippableItem(it, true)
            }
            it.item.bodyPart
        } ?: (currentlyEquippedSet?.armorSet?.getSetBodyPartWithoutShield() ?: emptyList())

        val preferredTypes = equipmentPreferences.preferredArmorTypes
        val preferredStats = equipmentPreferences.preferredArmorStats

        val typeComparator = comparatorItemByListIndex(preferredTypes) { item: L2ItemInstance ->
            item.armorItem.itemType
        }.reversed()
        val statComparator = chainItemStatsComparators(preferredStats, Stats.POWER_DEFENCE).reversed()
        val comparator = typeComparator.thenComparing(statComparator).thenComparing(enchantLevelComparator)

        val setContainedFullBody = equippedSetBodyParts.any { it == L2Item.SLOT_FULL_ARMOR }
        val availableSlots =
            if (setContainedFullBody) armorSlots.filter { it.itemBodyPart != L2Item.SLOT_LEGS } else armorSlots

        availableSlots
            .filter {
                it.itemBodyPart !in equippedSetBodyParts
            }
            .forEach { armorSlot ->
                val equippedItem = player.inventory.getPaperdollItem(armorSlot.slotOnCharacter)

                val armorItemsSortedByTypeAndStats = player.inventory
                    .unequippedArmor
                    .filter {
                        Grade.fromCrystalType(it.item.itemGrade) in availableGrades &&
                                it.item.bodyPart == armorSlot.itemBodyPart
                    }
                    .sortedWith(comparator)

                compareAndEquip(armorItemsSortedByTypeAndStats, equippedItem, comparator)
            }
    }

    private fun equipBetterJewels(availableGrades: Set<Grade>) {
        val preferredStat = Stats.MAGIC_DEFENCE
        val statComparator = Comparator.comparing { itemInstance: L2ItemInstance ->
            itemInstance.getStatValue(player, preferredStat)
        }.reversed()
        val comparator = statComparator.thenComparing(enchantLevelComparator)

        jewelSlots.forEach { jewelSlot ->
            val equippedItem = player.inventory.getPaperdollItem(jewelSlot.slotOnCharacter)

            val inventoryItems = player.inventory
                .unequippedArmor
                .filter {
                    Grade.fromCrystalType(it.item.itemGrade) in availableGrades &&
                            it.item.bodyPart == jewelSlot.itemBodyPart
                }
                .sortedWith(comparator)

            compareAndEquip(inventoryItems, equippedItem, comparator)
        }
    }

    private fun findEquippedPreferredSet(preferredSets: List<GradedArmorSet>): GradedArmorSet? =
        preferredSets.firstOrNull { it.armorSet.containAll(player) }

    private fun compareAndEquip(
        sortedInventoryItems: List<L2ItemInstance>,
        equippedItem: L2ItemInstance?,
        comparator: Comparator<L2ItemInstance>
    ) {
        val betterPart = sortedInventoryItems.firstOrNull() ?: return
        if (equippedItem == null) {
            player.useEquippableItem(betterPart, true)
            return
        }

        if (comparator.compare(equippedItem, betterPart) > 0) {
            player.useEquippableItem(betterPart, true)
        }
    }

}
