plugins {
    kotlin("jvm") apply true
    id("org.jetbrains.kotlin.plugin.noarg") apply true
}

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_15
    targetCompatibility = JavaVersion.VERSION_15
    sourceSets {
        main {
            java {
                srcDir("dist/game/data/scripts")
            }
        }
    }
}

dependencies {
    implementation(project(":mmocore"))
    implementation(project(":l2j_gameserver"))
    implementation(project(":common"))

    implementation("org.jdbi:jdbi3-core:3.+")
    implementation("org.jdbi:jdbi3-sqlobject:3.+")
    implementation("org.apache.httpcomponents:httpclient:4.+")

    implementation("com.fasterxml.jackson.core:jackson-core:2.1+")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.1+")
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.1+")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.1+")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.1+")

    implementation("com.google.guava:guava:31.+")

    implementation("org.apache.logging.log4j:log4j-api:2.+")

    implementation("io.micrometer:micrometer-registry-prometheus:latest.release")

    testImplementation("org.assertj:assertj-core:3.+")
    testImplementation("org.mockito:mockito-core:2.+")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.+")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.+")
    implementation(kotlin("stdlib-jdk8"))
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "15"
    }
}

task("zip", Zip::class) {
    val filename = "l2j_datapack.zip"
    archiveFileName.set(filename)
    into("/") {
        from("dist")
        exclude("**/geodata/**")
        exclude("**/pathnode/**")
        exclude("**/scripts/**")
        exclude("**/*.java")
        exclude("**/*.kt")
    }
    into("/game/data/scripts") {
        from("build/classes/kotlin/main")
        exclude("**/geodata/**")
        exclude("**/pathnode/**")
        exclude("**/*.java")
        exclude("**/*.kt")
    }
    into("/game/data/scripts") {
        from("build/classes/java/main")
        exclude("**/geodata/**")
        exclude("**/pathnode/**")
        exclude("**/*.java")
        exclude("**/*.kt")
    }
    into("/game/data/scripts") {
        from("dist/game/data/scripts")
        exclude("**/geodata/**")
        exclude("**/pathnode/**")
        exclude("**/*.java")
        exclude("**/*.kt")
    }
    duplicatesStrategy = DuplicatesStrategy.FAIL
    println("Build in build/distributions/$filename")
}

tasks {
    test {
        useJUnitPlatform()
    }
}

tasks {
    build {
        finalizedBy(getByName("zip"))
    }
}

noArg {
    annotation("com.l2jserver.common.noarg.NoArg")
    invokeInitializers = true
}