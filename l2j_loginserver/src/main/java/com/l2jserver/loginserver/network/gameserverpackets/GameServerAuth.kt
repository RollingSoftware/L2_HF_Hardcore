/*
 * Copyright (C) 2004-2016 L2J Server
 *
 * This file is part of L2J Server.
 *
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.network.gameserverpackets

import com.l2jserver.common.config.CommonConfig
import com.l2jserver.common.util.network.BaseRecievePacket
import com.l2jserver.loginserver.GameServerTable
import com.l2jserver.loginserver.GameServerTable.GameServerInfo
import com.l2jserver.loginserver.GameServerThread
import com.l2jserver.loginserver.config.Config
import com.l2jserver.loginserver.network.L2JGameServerPacketHandler.GameServerState
import com.l2jserver.loginserver.network.loginserverpackets.AuthResponse
import com.l2jserver.loginserver.network.loginserverpackets.LoginServerFail
import org.apache.logging.log4j.LogManager
import java.util.Arrays

/**
 * <pre>
 * Format: cccddb
 * c desired ID
 * c accept alternative ID
 * c reserve Host
 * s ExternalHostName
 * s InetranlHostName
 * d max players
 * d hexid size
 * b hexid
</pre> *
 *
 * @author -Wooden-
 */
class GameServerAuth(decrypt: ByteArray?, private val _server: GameServerThread) : BaseRecievePacket(decrypt) {
    private val _hexId: ByteArray
    private val _desiredId: Int
    private val _hostReserved: Boolean
    private val _acceptAlternativeId: Boolean
    private val _maxPlayers: Int
    private val _port: Int
    private val _hosts: Array<String>

    private fun handleRegProcess(): Boolean {
        val gameServerTable = GameServerTable.getInstance()
        val id = _desiredId
        val hexId = _hexId
        val existingServerById = gameServerTable.getRegisteredGameServerById(id)
        if (existingServerById == null) {
            return handleNewServer(id, hexId, gameServerTable)
        }

        if (Arrays.equals(existingServerById.hexId, hexId)) {
            return handleExistingServerWithCorrectHex(existingServerById)
        } else {
            return if (Config.ACCEPT_NEW_GAMESERVER && _acceptAlternativeId) {
                val newServer = GameServerInfo(id, hexId, _server)
                if (gameServerTable.registerWithFirstAvailableId(newServer)) {
                    _server.attachGameServerInfo(newServer, _port, _hosts, _maxPlayers)
                    gameServerTable.registerServerOnDB(newServer)
                    true
                } else {
                    _server.forceClose(LoginServerFail.REASON_NO_FREE_ID)
                    false
                }
            } else {
                LOG.warn("Server id is already taken, and we can't get a new one for {}")
                _server.forceClose(LoginServerFail.REASON_WRONG_HEXID)
                false
            }
        }
    }

    private fun handleExistingServerWithCorrectHex(existingServerById: GameServerInfo): Boolean {
        synchronized(existingServerById) {
            if (existingServerById.isAuthed) {
                _server.forceClose(LoginServerFail.REASON_ALREADY_LOGGED8IN)
                return false
            }
            _server.attachGameServerInfo(existingServerById, _port, _hosts, _maxPlayers)
            return true
        }
    }

    private fun handleNewServer(id: Int, hexId: ByteArray, gameServerTable: GameServerTable): Boolean {
        return if (Config.ACCEPT_NEW_GAMESERVER) {
            val gameServerInfo = GameServerInfo(id, hexId, _server)
            if (gameServerTable.register(id, gameServerInfo)) {
                _server.attachGameServerInfo(gameServerInfo, _port, _hosts, _maxPlayers)
                gameServerTable.registerServerOnDB(gameServerInfo)
                true
            } else {
                _server.forceClose(LoginServerFail.REASON_ID_RESERVED)
                false
            }
        } else {
            _server.forceClose(LoginServerFail.REASON_WRONG_HEXID)
            false
        }
    }

    companion object {
        private var LOG = LogManager.getLogger()
    }

    init {
        _desiredId = readC()
        _acceptAlternativeId = readC() != 0
        _hostReserved = readC() != 0
        _port = readH()
        _maxPlayers = readD()
        var size = readD()
        _hexId = readB(size)
        size = 2 * readD()
        _hosts = Array(size = size) { "" }
        for (i in 0 until size) {
            _hosts[i] = readS()
        }
        if (CommonConfig.DEBUG) {
            LOG.info("Auth request received")
        }
        if (handleRegProcess()) {
            val ar = AuthResponse(
                _server.gameServerInfo.id
            )
            _server.sendPacket(ar)
            if (CommonConfig.DEBUG) {
                LOG.info("Authed: id: " + _server.gameServerInfo.id)
            }
            _server.loginConnectionState = GameServerState.AUTHED
        }
    }
}