/*
 * Copyright (C) 2004-2016 L2J Server
 *
 * This file is part of L2J Server.
 *
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.network;

import com.l2jserver.common.util.network.BaseRecievePacket;
import com.l2jserver.loginserver.GameServerThread;
import com.l2jserver.loginserver.config.Config;
import com.l2jserver.loginserver.network.gameserverpackets.*;
import com.l2jserver.loginserver.network.loginserverpackets.LoginServerFail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author mrTJO
 */
public class L2JGameServerPacketHandler {
    protected static Logger LOG = LogManager.getLogger();

    public enum GameServerState {
        CONNECTED,
        BF_CONNECTED,
        AUTHED
    }

    private static void logUnknownOpCode(int opcode, GameServerState state) {
        LOG.warn(
                "Unknown Opcode ({}) in state {} from GameServer, closing connection.",
                Integer.toHexString(opcode).toUpperCase(),
                state.name()
        );
    }

    public static BaseRecievePacket handlePacket(byte[] data, GameServerThread server) {
        BaseRecievePacket msg = null;
        int opcode = data[0] & 0xff;
        GameServerState state = server.getLoginConnectionState();
        switch (state) {
            case CONNECTED:
                if (opcode == 0x00) {
                    msg = new BlowFishKey(data, server);
                } else {
                    logUnknownOpCode(opcode, state);
                    server.forceClose(LoginServerFail.NOT_AUTHED);
                }
                break;
            case BF_CONNECTED:
                if (opcode == 0x01) {
                    msg = new GameServerAuth(data, server);
                } else {
                    logUnknownOpCode(opcode, state);
                    server.forceClose(LoginServerFail.NOT_AUTHED);
                }
                break;
            case AUTHED:
                switch (opcode) {
                    case 0x02:
                        msg = new PlayerInGame(data, server);
                        break;
                    case 0x03:
                        msg = new PlayerLogout(data, server);
                        break;
                    case 0x04:
                        msg = new ChangeAccessLevel(data, server);
                        break;
                    case 0x05:
                        msg = new PlayerAuthRequest(data, server);
                        break;
                    case 0x06:
                        msg = new ServerStatus(data, server);
                        break;
                    case 0x07:
                        msg = new PlayerTracert(data);
                        break;
                    case 0x08:
                        msg = new ReplyCharacters(data, server);
                        break;
                    case 0x09:
                        if (Config.EMAIL_SYS_ENABLED) {
                            msg = new RequestSendMail(data);
                        }
                        break;
                    case 0x0A:
                        msg = new RequestTempBan(data);
                        break;
                    case 0x0B:
                        new ChangePassword(data);
                        break;
                    default:
                        logUnknownOpCode(opcode, state);
                        server.forceClose(LoginServerFail.NOT_AUTHED);
                        break;
                }
                break;
        }
        return msg;
    }
}
