/*
 * Copyright (C) 2004-2016 L2J Server
 *
 * This file is part of L2J Server.
 *
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver;

import com.l2jserver.loginserver.config.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author -Wooden-
 */
public abstract class FloodProtectedListener extends Thread {
    private final Logger LOG = LogManager.getLogger();
    private final Map<String, ForeignConnection> _floodProtection = new ConcurrentHashMap<>();
    private final ServerSocket _serverSocket;

    public FloodProtectedListener(String listenIp, int port) throws IOException {
        if (listenIp.equals("*")) {
            _serverSocket = new ServerSocket(port);
        } else {
            _serverSocket = new ServerSocket(port, 50, InetAddress.getByName(listenIp));
        }
    }

    @Override
    public void run() {
        Socket connection = null;
        while (!isInterrupted()) {
            try {
                connection = _serverSocket.accept();
                if (Config.FLOOD_PROTECTION) {
                    ForeignConnection fConnection = _floodProtection.get(connection.getInetAddress().getHostAddress());
                    if (fConnection != null) {
                        fConnection.connectionNumber += 1;
                        if (((fConnection.connectionNumber > Config.FAST_CONNECTION_LIMIT) && ((System.currentTimeMillis() - fConnection.lastConnection) < Config.NORMAL_CONNECTION_TIME)) || ((System.currentTimeMillis() - fConnection.lastConnection) < Config.FAST_CONNECTION_TIME) || (fConnection.connectionNumber > Config.MAX_CONNECTION_PER_IP)) {
                            fConnection.lastConnection = System.currentTimeMillis();
                            connection.close();
                            fConnection.connectionNumber -= 1;
                            if (!fConnection.isFlooding) {
                                LOG.warn("Potential Flood from {}", connection.getInetAddress().getHostAddress());
                            }
                            fConnection.isFlooding = true;
                            continue;
                        }
                        if (fConnection.isFlooding) // if connection was flooding server but now passed the check
                        {
                            fConnection.isFlooding = false;
                            LOG.info("{} is not considered as flooding anymore.", connection.getInetAddress().getHostAddress());
                        }
                        fConnection.lastConnection = System.currentTimeMillis();
                    } else {
                        fConnection = new ForeignConnection(System.currentTimeMillis());
                        _floodProtection.put(connection.getInetAddress().getHostAddress(), fConnection);
                    }
                }

                addClient(connection);
            } catch (Exception e) {
                if (isInterrupted()) {
                    // shutdown?
                    try {
                        _serverSocket.close();
                    } catch (IOException io) {
                        LOG.info("Failed to shutdown server gracefully", io);
                    }
                    break;
                }
            }
        }
    }

    protected static class ForeignConnection {
        public int connectionNumber;
        public long lastConnection;
        public boolean isFlooding = false;

        public ForeignConnection(long time) {
            lastConnection = time;
            connectionNumber = 1;
        }
    }

    public abstract void addClient(Socket s);

    public void removeFloodProtection(String ip) {
        if (!Config.FLOOD_PROTECTION) {
            return;
        }
        ForeignConnection fConnection = _floodProtection.get(ip);
        if (fConnection != null) {
            fConnection.connectionNumber -= 1;
            if (fConnection.connectionNumber == 0) {
                _floodProtection.remove(ip);
            }
        } else {
            LOG.warn("Removing a flood protection for a GameServer that was not in the connection map: {}", ip);
        }
    }

    public void close() {
        try {
            _serverSocket.close();
        } catch (IOException e) {
            LOG.warn("{}: {}", e.getMessage());
        }
    }
}