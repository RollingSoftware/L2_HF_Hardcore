package com.l2jserver.loginserver.config;

import com.l2jserver.common.config.PropertiesParser;
import com.l2jserver.common.resources.ResourceResolver;
import com.l2jserver.common.util.EnvHelper;

public class Config {

    public static final String EMAIL_CONFIG_FILE = ResourceResolver.resolveConfig("./server/Email.properties");
    public static final String LOGIN_CONFIGURATION_FILE = ResourceResolver.resolveConfig("./server/LoginServer.properties");

    public static boolean LOGIN_SERVER_SCHEDULE_RESTART;
    public static long LOGIN_SERVER_SCHEDULE_RESTART_TIME;

    public static boolean SHOW_LICENCE;
    public static boolean ACCEPT_NEW_GAMESERVER;

    public static boolean AUTO_CREATE_ACCOUNTS;

    public static int PORT_LOGIN;
    public static String LOGIN_BIND_ADDRESS;
    public static int LOGIN_TRY_BEFORE_BAN;
    public static int LOGIN_BLOCK_AFTER_BAN;

    public static boolean FLOOD_PROTECTION;
    public static int FAST_CONNECTION_LIMIT;
    public static int NORMAL_CONNECTION_TIME;
    public static int FAST_CONNECTION_TIME;
    public static int MAX_CONNECTION_PER_IP;

    // Email
    public static String EMAIL_SERVERINFO_NAME;
    public static String EMAIL_SERVERINFO_ADDRESS;
    public static boolean EMAIL_SYS_ENABLED;
    public static String EMAIL_SYS_HOST;
    public static int EMAIL_SYS_PORT;
    public static boolean EMAIL_SYS_SMTP_AUTH;
    public static String EMAIL_SYS_FACTORY;
    public static boolean EMAIL_SYS_FACTORY_CALLBACK;
    public static String EMAIL_SYS_USERNAME;
    public static String EMAIL_SYS_PASSWORD;
    public static String EMAIL_SYS_ADDRESS;
    public static String EMAIL_SYS_SELECTQUERY;
    public static String EMAIL_SYS_DBFIELD;

    public static String GAME_SERVER_LOGIN_HOST;
    public static int GAME_SERVER_LOGIN_PORT;

    public static void load() {
        final PropertiesParser serverSettings = new PropertiesParser(LOGIN_CONFIGURATION_FILE);

        LOGIN_BIND_ADDRESS = serverSettings.getString("LoginserverHostname", "*");
        PORT_LOGIN = serverSettings.getInt("LoginserverPort", 2106);

        ACCEPT_NEW_GAMESERVER = serverSettings.getBoolean("AcceptNewGameServer", true);

        LOGIN_TRY_BEFORE_BAN = serverSettings.getInt("LoginTryBeforeBan", 5);
        LOGIN_BLOCK_AFTER_BAN = serverSettings.getInt("LoginBlockAfterBan", 900);

        LOGIN_SERVER_SCHEDULE_RESTART = serverSettings.getBoolean("LoginRestartSchedule", false);
        LOGIN_SERVER_SCHEDULE_RESTART_TIME = serverSettings.getLong("LoginRestartTime", 24);

        SHOW_LICENCE = serverSettings.getBoolean("ShowLicence", true);

        AUTO_CREATE_ACCOUNTS = Boolean.parseBoolean(EnvHelper.env("L2_AUTO_CREATE_ACCOUNTS").orElse(serverSettings.getString("AutoCreateAccounts", "true")));

        FLOOD_PROTECTION = serverSettings.getBoolean("EnableFloodProtection", true);
        FAST_CONNECTION_LIMIT = serverSettings.getInt("FastConnectionLimit", 15);
        NORMAL_CONNECTION_TIME = serverSettings.getInt("NormalConnectionTime", 700);
        FAST_CONNECTION_TIME = serverSettings.getInt("FastConnectionTime", 350);
        MAX_CONNECTION_PER_IP = serverSettings.getInt("MaxConnectionPerIP", 50);

        // Email
        final PropertiesParser emailSettings = new PropertiesParser(EMAIL_CONFIG_FILE);

        EMAIL_SERVERINFO_NAME = emailSettings.getString("ServerInfoName", "Unconfigured L2J Server");
        EMAIL_SERVERINFO_ADDRESS = emailSettings.getString("ServerInfoAddress", "info@myl2jserver.com");

        EMAIL_SYS_ENABLED = emailSettings.getBoolean("EmailSystemEnabled", false);
        EMAIL_SYS_HOST = emailSettings.getString("SmtpServerHost", "smtp.gmail.com");
        EMAIL_SYS_PORT = emailSettings.getInt("SmtpServerPort", 465);
        EMAIL_SYS_SMTP_AUTH = emailSettings.getBoolean("SmtpAuthRequired", true);
        EMAIL_SYS_FACTORY = emailSettings.getString("SmtpFactory", "javax.net.ssl.SSLSocketFactory");
        EMAIL_SYS_FACTORY_CALLBACK = emailSettings.getBoolean("SmtpFactoryCallback", false);
        EMAIL_SYS_USERNAME = emailSettings.getString("SmtpUsername", "user@gmail.com");
        EMAIL_SYS_PASSWORD = emailSettings.getString("SmtpPassword", "password");
        EMAIL_SYS_ADDRESS = emailSettings.getString("EmailSystemAddress", "noreply@myl2jserver.com");
        EMAIL_SYS_SELECTQUERY = emailSettings.getString("EmailDBSelectQuery", "SELECT value FROM account_data WHERE account_name=? AND var='email_addr'");
        EMAIL_SYS_DBFIELD = emailSettings.getString("EmailDBField", "value");

        GAME_SERVER_LOGIN_HOST = serverSettings.getString("LoginHostname", "127.0.0.1");
        EnvHelper.env("L2_LS_GAMEHANDLER_HOST").map(value -> GAME_SERVER_LOGIN_HOST = value);

        GAME_SERVER_LOGIN_PORT = serverSettings.getInt("LoginPort", 9013);
        EnvHelper.env("L2_LS_GAMEHANDLER_PORT").map(value -> GAME_SERVER_LOGIN_PORT = Integer.parseInt(value));
    }

}
