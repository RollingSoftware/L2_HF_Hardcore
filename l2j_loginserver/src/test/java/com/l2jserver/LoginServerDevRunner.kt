package com.l2jserver

import com.l2jserver.common.config.CommonConfig
import com.l2jserver.loginserver.LoginServer
import java.io.File

fun main(args: Array<String>) {
    CommonConfig.DEFAULT_DATAPACK_ROOT = File("./l2j_loginserver/dist/login").canonicalPath
    CommonConfig.DEFAULT_CONFIG_ROOT = File("./l2j_loginserver/dist/login/config").canonicalPath
    LoginServer.main(args)
}