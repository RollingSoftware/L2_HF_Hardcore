plugins {
    kotlin("jvm") apply true
    id("org.jetbrains.kotlin.plugin.noarg") apply true
}

version = "1.1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":mmocore"))

    implementation("at.favre.lib:bcrypt:0.+")
    implementation("com.zaxxer:HikariCP:5.+")

    implementation("com.google.guava:guava:31.+")
    implementation("commons-io:commons-io:2.+")

    implementation("org.liquibase:liquibase-core:4.+")

    implementation("org.apache.logging.log4j:log4j-api:2.+")
    implementation("org.apache.logging.log4j:log4j-core:2.+")
    implementation("org.apache.logging.log4j:log4j-slf4j18-impl:2.+")
    implementation("org.apache.logging.log4j:log4j-jul:2.+")

    implementation("com.fasterxml.jackson.core:jackson-core:2.1+")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.1+")
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.1+")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.1+")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.1+")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.1+")

    implementation("io.micrometer:micrometer-registry-prometheus:latest.release")
    implementation(kotlin("stdlib-jdk8"))
}

java {
    sourceCompatibility = JavaVersion.VERSION_15
    targetCompatibility = JavaVersion.VERSION_15
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "15"
    }
}

noArg {
    annotation("com.l2jserver.common.noarg.NoArg")
    invokeInitializers = true
}