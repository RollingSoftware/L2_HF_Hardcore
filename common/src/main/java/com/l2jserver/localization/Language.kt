package com.l2jserver.localization

data class Language(val code: String) {
    companion object {
        private val EN = Language("en")
        private val RU = Language("ru")
        private val DEFAULT = EN

        val languages = listOf(EN, RU)

        @JvmStatic
        fun of(languageString: String?): Language {
            return if (languageString == null) {
                defaultLanguage()
            } else Language(languageString)
        }

        fun english(): Language {
            return EN
        }

        @JvmStatic
        fun defaultLanguage(): Language {
            return DEFAULT
        }
    }
}