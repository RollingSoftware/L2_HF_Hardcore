package com.l2jserver.common.monitoring

data class MetricsConfig(
    val scrapeEnabled: Boolean = false,
    val scrapePath: String = "/metrics",
    val scrapePort: Int = 9100
)