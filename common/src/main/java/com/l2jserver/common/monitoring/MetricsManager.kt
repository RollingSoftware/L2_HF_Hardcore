package com.l2jserver.common.monitoring;

import com.l2jserver.common.resources.ResourceResolver
import com.l2jserver.common.resources.YamlResource
import com.l2jserver.common.util.EnvHelper
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import com.sun.net.httpserver.HttpServer
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import java.net.InetSocketAddress
import java.util.concurrent.Executors
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class MetricsManager {
    companion object {
        val log: Logger = LogManager.getLogger()
        val instance = MetricsManager()
    }

    private val metricsConfig: MetricsConfig
    private var server: HttpServer? = null

    val metricsRegistry: PrometheusMeterRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

    private val metricsConfigFilePath = ResourceResolver.resolveConfig("./metrics-config.yaml")

    init {
        metricsConfig = loadConfig()
    }

    private fun loadConfig(): MetricsConfig {
        val preparedConfig = YamlResource.loadFileOrDefault(
            metricsConfigFilePath,
            MetricsConfig::class.java
        ) { MetricsConfig() }

        return preparedConfig.copy(
            scrapeEnabled = EnvHelper.env("L2J_METRICS_SCRAPE_ENABLED")
                .map { it.toBoolean() }
                .orElse(preparedConfig.scrapeEnabled),
            scrapePath = EnvHelper.env("L2J_METRICS_SCRAPE_PATH")
                .orElse(preparedConfig.scrapePath),
            scrapePort = EnvHelper.env("L2J_METRICS_SCRAPE_PORT")
                .map { it.toInt() }
                .orElse(preparedConfig.scrapePort)
        )
    }

    fun startScapeServer() {
        if (!metricsConfig.scrapeEnabled) {
            log.info("Metrics scrape server is disabled")
            return
        }
        try {
            server = HttpServer.create(InetSocketAddress(metricsConfig.scrapePort), 0)
            server?.let {
                it.createContext(metricsConfig.scrapePath, MetricsScrapingHandler(metricsRegistry))
                it.executor = Executors.newSingleThreadExecutor()
                it.start()
            }
        } catch (e: Exception) {
            log.error("Failed to start scrape server on '${metricsConfig.scrapePort}' because: '${e.message}'")
        }
    }

    fun stopScrapeServer() {
        server?.stop(1)
    }
}

class MetricsScrapingHandler(private val metricsRegistry: PrometheusMeterRegistry) : HttpHandler {
    private val successHttp = 200

    override fun handle(httpExchange: HttpExchange) {
        val response = metricsRegistry.scrape()
        httpExchange.sendResponseHeaders(successHttp, response.length.toLong())
        httpExchange.responseBody.use {
            it.write(response.toByteArray())
        }
    }
}