package com.l2jserver.common.util;

import java.util.Optional;

public class EnvHelper {
    public static Optional<String> env(String key) {
        var value = System.getenv(key);
        if (StringUtil.isNotBlank(value)) {
            return Optional.of(value);
        } else {
            return Optional.empty();
        }
    }
}
