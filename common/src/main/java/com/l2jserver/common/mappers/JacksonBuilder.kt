package com.l2jserver.common.mappers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

fun ObjectMapper.registerModules() {
    this.registerKotlinModule().registerModule(JavaTimeModule())
}