package com.l2jserver.common.mappers

import com.fasterxml.jackson.databind.ObjectMapper

object Json {

    val json: ObjectMapper = ObjectMapper().registerModules()

}