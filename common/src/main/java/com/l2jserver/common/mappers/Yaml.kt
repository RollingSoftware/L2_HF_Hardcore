package com.l2jserver.common.mappers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper

object Yaml {

    val yaml: ObjectMapper = YAMLMapper().registerModules()

}