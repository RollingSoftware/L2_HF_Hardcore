package com.l2jserver.common.classloader;

import java.net.URL;
import java.net.URLClassLoader;

/*
 * Development hot reload classloader
 * DO NOT USE FOR PRODUCTION CODE
 * LOADER IS PRONE TO MEMORY LEAKS
 */
public class DynamicUrlClassloader {
    private final URL[] urls;
    private final URLClassLoader mainClassLoader;

    public DynamicUrlClassloader(URL[] urls) {
        this.urls = urls;
        mainClassLoader = new URLClassLoader(urls, getClass().getClassLoader());
    }

    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return mainClassLoader.loadClass(name);
    }

    public Class<?> reloadClass(String name) throws ClassNotFoundException {
        return new URLClassLoader(urls).loadClass(name);
    }
}
