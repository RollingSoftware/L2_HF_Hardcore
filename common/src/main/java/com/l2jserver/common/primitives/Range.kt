package com.l2jserver.common.primitives

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import com.l2jserver.common.util.Rnd

data class Range(val low: Int, val high: Int) {

    constructor(value: Int) : this(low = value, high = value)

    fun randomWithin(): Int {
        return Rnd.get(low, high)
    }

    fun isWithin(value: Int): Boolean {
        return value in low..high
    }

    fun equalsTo(single: Int): Boolean {
        return low == single && high == single
    }

    val isPositive: Boolean
        get() = high - low >= 0

    override fun toString(): String =
        asString()

    @JsonValue
    fun asString(): String {
        return if (low == high) {
            low.toString()
        } else {
            "$low-$high"
        }
    }

    operator fun compareTo(levels: Range?): Int {
        if (levels == null) {
            return high
        }
        return high - levels.high
    }

    companion object {
        private val ZERO = Range(0)
        private val ONE = Range(1)

        @JvmStatic
        @JsonCreator
        fun fromString(raw: String): Range {
            val parts = raw.split("-").toTypedArray()
            return if (parts.size == 2) {
                Range(parts[0].toInt(), parts[1].toInt())
            } else {
                val singleValue = raw.toInt()
                Range(singleValue, singleValue)
            }
        }

        @JvmStatic
        fun one(): Range {
            return ONE
        }

        fun zero(): Range {
            return ZERO
        }
    }
}