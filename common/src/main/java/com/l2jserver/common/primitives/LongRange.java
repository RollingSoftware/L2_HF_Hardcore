package com.l2jserver.common.primitives;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.l2jserver.common.util.Rnd;
import java.util.Objects;

public class LongRange {

    private static final Range ZERO = new Range(0);
    private static final Range ONE = new Range(1);

    private long low;
    private long high;

    public LongRange() {
    }

    public LongRange(long value) {
        this.low = value;
        this.high = value;
    }

    public LongRange(long low, long high) {
        this.low = low;
        this.high = high;
    }

    public long getLow() {
        return low;
    }

    public long getHigh() {
        return high;
    }

    public long randomWithin() {
        return Rnd.get(low, high);
    }

    public boolean isWithin(int value) {
        return low <= value && value <= high;
    }

    public boolean equalsTo(int single) {
        return low == single && high == single;
    }

    public boolean isPositive() {
        return high - low >= 0;
    }

    @JsonCreator
    public static LongRange fromString(String raw) {
        String[] parts = raw.split("-");
        if (parts.length == 2) {
            return new LongRange(Long.parseLong(parts[0]), Long.parseLong(parts[1]));
        } else {
            long singleValue = Long.parseLong(raw);
            return new LongRange(singleValue, singleValue);
        }
    }

    @JsonValue
    public String asString() {
        if (low == high) {
            return String.valueOf(low);
        } else {
            return low + "-" + high;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LongRange range = (LongRange) o;
        return low == range.low &&
                high == range.high;
    }

    @Override
    public int hashCode() {
        return Objects.hash(low, high);
    }

    @Override
    public String toString() {
        return asString();
    }

    public static Range one() {
        return ONE;
    }

    public static Range zero() {
        return ZERO;
    }

}
