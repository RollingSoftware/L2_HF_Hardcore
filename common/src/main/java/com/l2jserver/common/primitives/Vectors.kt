package com.l2jserver.common.primitives

import com.l2jserver.common.primitives.Vectors.zero2d
import com.l2jserver.common.primitives.Vectors.zero3d
import kotlin.math.sqrt

data class Vector2(val x: Double, val y: Double) {
    infix operator fun minus(other: Vector2) =
        Vector2(x - other.x, y - other.y)

    infix operator fun div(d: Double) = Vector2(x / d, y / d)

    infix operator fun times(d: Double) = Vector2(x * d, y * d)

    val length: Double
        get() = sqrt(x * x + y * y)

    val normalized: Vector2
        get() {
            return if (length > 0.0) {
                this / length
            } else {
                zero2d
            }
        }

    fun vect3() = Vector3(x, y, 0.0)

    fun distanceTo(other: Vector2): Double {
        val dx = other.x - x
        val dy = other.y - y
        return sqrt(dx * dx + dy * dy)
    }
}

data class Vector3(val x: Double, val y: Double, val z: Double) {
    val length: Double
        get() = sqrt(x * x + y * y + z * z)

    infix operator fun div(d: Double) = Vector3(x / d, y / d, z / d)

    val normalized: Vector3
        get() {
            return if (length > 0.0) {
                this / length
            } else {
                zero3d
            }
        }

    fun vect2() = Vector2(x, y)

    infix fun cross(v: Vector3) = Vector3(
        y * v.z - z * v.y,
        -(x * v.z - z * v.x),
        x * v.y - y * v.x
    )
}

object Vectors {
    val zero2d = Vector2(0.0, 0.0)
    val zero3d = Vector3(0.0, 0.0, 0.0)
    val up = Vector3(0.0, 0.0, 1.0)
}