package com.l2jserver.common.file;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileHelper {

    private static final Logger LOG = LogManager.getLogger();

    public static String loadFileContents(Class<?> clazz, String path) {
        try {
            var classPath = clazz.getResource(path);
            Path resolvedPath;
            if (classPath == null) {
                resolvedPath = Path.of(path);
            } else {
                resolvedPath = Path.of(clazz.getResource(path).toURI());
            }

            return Files.readString(resolvedPath);
        } catch (IOException | URISyntaxException e) {
            LOG.error("Failed to load '{}' template", path, e);
            throw new IllegalStateException(e);
        }
    }

}
