package com.l2jserver.common.rewards;

public class Reward {

    private final int itemId;
    private final long amount;

    public Reward(int itemId) {
        this(itemId, 1);
    }

    public Reward(int itemId, long amount) {
        this.itemId = itemId;
        this.amount = amount;
    }

    public int getItemId() {
        return itemId;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reward reward = (Reward) o;

        if (itemId != reward.itemId) return false;
        return amount == reward.amount;
    }

    @Override
    public int hashCode() {
        int result = itemId;
        result = 31 * result + (int) (amount ^ (amount >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Reward{" +
                "itemId=" + itemId +
                ", amount=" + amount +
                '}';
    }

}
