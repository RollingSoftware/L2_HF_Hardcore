package com.l2jserver.common.rewards;

import java.util.Objects;

public class ChancedReward {

    private final Reward reward;
    private final double chance;

    public ChancedReward(Reward reward, double chance) {
        this.reward = reward;
        this.chance = chance;
    }

    public Reward getReward() {
        return reward;
    }

    public double getChance() {
        return chance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChancedReward that = (ChancedReward) o;

        if (Double.compare(that.chance, chance) != 0) return false;
        return Objects.equals(reward, that.reward);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = reward != null ? reward.hashCode() : 0;
        temp = Double.doubleToLongBits(chance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "ChancedReward{" +
                "reward=" + reward +
                ", chance=" + chance +
                '}';
    }
}
