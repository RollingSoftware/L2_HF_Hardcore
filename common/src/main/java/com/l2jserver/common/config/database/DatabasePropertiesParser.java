package com.l2jserver.common.config.database;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.l2jserver.common.mappers.Yaml;
import com.l2jserver.common.resources.ResourceResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class DatabasePropertiesParser {

    private static final Logger LOG = LogManager.getLogger();

    public static DatabaseProperties parseDatabaseProperties() {
        String propertiesFile = ResourceResolver.resolveConfig("./database.yml");
        try {
            boolean fileExists = new File(propertiesFile).exists();
            if (!fileExists) {
                LOG.warn("Configuration file " + propertiesFile + " does not exists, returning default values");
                return new DatabaseProperties();
            }

            return Yaml.INSTANCE.getYaml().readValue(new File(propertiesFile), DatabaseProperties.class);
        } catch (JsonProcessingException e) {
            LOG.error("Failed to parse '{}'", propertiesFile);
            throw new IllegalStateException(e);
        } catch (IOException e) {
            LOG.error("Failed to read '{}'", propertiesFile);
            throw new IllegalStateException(e);
        }
    }

}
