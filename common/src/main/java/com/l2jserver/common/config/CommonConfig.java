package com.l2jserver.common.config;

import com.l2jserver.common.config.database.DatabaseProperties;
import com.l2jserver.common.config.database.DatabasePropertiesParser;
import com.l2jserver.common.resources.ResourceResolver;
import com.l2jserver.common.util.EnvHelper;
import java.io.File;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommonConfig {

    public static final Logger LOG = LogManager.getLogger();
    public static final String EOL = System.lineSeparator();

    public static boolean DEBUG;
    public static DatabaseProperties DATABASE_PROPERTIES;

    public static int MMO_SELECTOR_SLEEP_TIME;
    public static int MMO_MAX_SEND_PER_PASS;
    public static int MMO_MAX_READ_PER_PASS;
    public static int MMO_HELPER_BUFFER_COUNT;
    public static boolean MMO_TCP_NODELAY;

    public static File DATAPACK_ROOT;
    public static File CONFIG_ROOT;

    public static String DEFAULT_DATAPACK_ROOT = ".";
    public static String DEFAULT_CONFIG_ROOT = "./config";

    public static void load() {
        try {
            DATAPACK_ROOT = new File(
                    EnvHelper.env("DATAPACK_ROOT")
                            .orElse(DEFAULT_DATAPACK_ROOT)
            ).getCanonicalFile();
        } catch (IOException e) {
            LOG.warn("Error setting datapack root!", e);
            DATAPACK_ROOT = new File(DEFAULT_DATAPACK_ROOT);
        }

        try {
            CONFIG_ROOT = new File(
                    EnvHelper.env("CONFIG_ROOT")
                            .orElse(DEFAULT_CONFIG_ROOT)
            ).getCanonicalFile();
        } catch (IOException e) {
            LOG.warn("Error setting config root!", e);
            CONFIG_ROOT = new File(DEFAULT_CONFIG_ROOT);
        }

        String MMO_CONFIG_FILE = ResourceResolver.resolveConfig("/server/MMO.properties");
        final PropertiesParser mmoSettings = new PropertiesParser(MMO_CONFIG_FILE);
        MMO_SELECTOR_SLEEP_TIME = mmoSettings.getInt("SleepTime", 20);
        MMO_MAX_SEND_PER_PASS = mmoSettings.getInt("MaxSendPerPass", 12);
        MMO_MAX_READ_PER_PASS = mmoSettings.getInt("MaxReadPerPass", 12);
        MMO_HELPER_BUFFER_COUNT = mmoSettings.getInt("HelperBufferCount", 20);
        MMO_TCP_NODELAY = mmoSettings.getBoolean("TcpNoDelay", false);

        DATABASE_PROPERTIES = DatabasePropertiesParser.parseDatabaseProperties();
        EnvHelper.env("L2_DB_URL").map(value -> DATABASE_PROPERTIES.url = value);
        EnvHelper.env("L2_DB_LOGIN").map(value -> DATABASE_PROPERTIES.login = value);
        EnvHelper.env("L2_DB_PASSWORD").map(value -> DATABASE_PROPERTIES.password = value);
    }

}
