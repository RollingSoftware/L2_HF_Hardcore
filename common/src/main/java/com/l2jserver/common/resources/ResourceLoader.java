package com.l2jserver.common.resources;

import com.l2jserver.common.file.FileHelper;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ResourceLoader {

    private static final ResourceLoader INSTANCE = new ResourceLoader();

    private Map<String, String> contentsCache = new ConcurrentHashMap<>();

    public String getResource(Class<?> clazz, String resourcePath) {
        var cachedResult = contentsCache.get(resourcePath);
        if (cachedResult != null) {
            return cachedResult;
        } else {
            var result = FileHelper.loadFileContents(clazz, resourcePath);
            contentsCache.put(resourcePath, result);
            return result;
        }
    }

    public void evictAll() {
        contentsCache.clear();
        contentsCache = new ConcurrentHashMap<>();
    }

    public static ResourceLoader getInstance() {
        return ResourceLoader.INSTANCE;
    }

}
