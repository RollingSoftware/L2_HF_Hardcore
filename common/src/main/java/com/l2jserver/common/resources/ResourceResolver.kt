package com.l2jserver.common.resources

import com.l2jserver.common.config.CommonConfig
import java.io.File

class ResourceResolver {

    companion object {
        @JvmStatic
        fun resolveConfig(path: String): String = File(CommonConfig.CONFIG_ROOT.canonicalPath, path).canonicalPath

        @JvmStatic
        fun resolveDatapack(path: String): String = File(CommonConfig.DATAPACK_ROOT.canonicalPath, path).canonicalPath
    }

}