package com.l2jserver.common.resources;

import com.fasterxml.jackson.core.type.TypeReference;
import com.l2jserver.common.mappers.Yaml;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class YamlResource {

    private static final Logger LOG = LogManager.getLogger();

    public static Map<String, String> readAsStringMap(File file) {
        try {
            return Yaml.INSTANCE.getYaml().readValue(
                    file,
                    new TypeReference<HashMap<String, String>>() {
                    });
        } catch (IOException e) {
            throw new IllegalStateException("Could not read " + file, e);
        }
    }

    public static <T> T loadFromClassPathFile(Class<?> classPath, String resourcePath, Class<T> clazz) {
        try {
            return Yaml.INSTANCE.getYaml().readValue(classPath.getResourceAsStream(resourcePath), clazz);
        } catch (IOException e) {
            throw new IllegalStateException("Could not load '" + resourcePath + "'", e);
        }
    }

    public static <T> T loadFromClassPathFile(Class<?> classPath, String resourcePath, TypeReference<T> clazz) {
        try {
            return Yaml.INSTANCE.getYaml().readValue(classPath.getResourceAsStream(resourcePath), clazz);
        } catch (IOException e) {
            throw new IllegalStateException("Could not load '" + resourcePath + "'", e);
        }
    }

    public static <T> T loadFile(String path, Class<T> clazz) {
        try (var input = Files.newInputStream(Paths.get(path))) {
            return Yaml.INSTANCE.getYaml().readValue(input, clazz);
        } catch (IOException e) {
            LOG.info("Using default config value, could not process file '{}': {}", path, e.getMessage());
            throw new IllegalStateException(e);
        }
    }

    public static <T> T loadFile(String path, TypeReference<T> clazz) {
        try (var input = Files.newInputStream(Paths.get(path))) {
            return Yaml.INSTANCE.getYaml().readValue(input, clazz);
        } catch (IOException e) {
            LOG.info("Using default config value, could not process file '{}': {}", path, e.getMessage());
            throw new IllegalStateException(e);
        }
    }


    public static <T> T loadFileOrDefault(String path, Class<T> clazz, Supplier<T> defaultValue) {
        var pathHandle = Paths.get(path);
        if (Files.notExists(pathHandle)) {
            LOG.info("Using default config value, override file '{}' does not exist", path);
            return defaultValue.get();
        }

        try (var input = Files.newInputStream(pathHandle)) {
            return Yaml.INSTANCE.getYaml().readValue(input, clazz);
        } catch (IOException e) {
            LOG.info("Using default config value, could not process file '{}': {}", path, e.getMessage());
            return defaultValue.get();
        }
    }

}
