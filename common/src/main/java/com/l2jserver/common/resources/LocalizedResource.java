package com.l2jserver.common.resources;

import com.l2jserver.localization.Language;
import org.apache.commons.io.FilenameUtils;

import java.nio.file.Path;
import java.util.Objects;
import java.util.regex.Pattern;

public class LocalizedResource {

    private String absolutePath;
    private String basename;
    private Language language;
    private String extension;

    public LocalizedResource(String absolutePath, String basename, Language language, String extension) {
        this.absolutePath = absolutePath;
        this.basename = basename;
        this.language = language;
        this.extension = extension;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public String getBasename() {
        return basename;
    }

    public Language getLanguage() {
        return language;
    }

    public String getExtension() {
        return extension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocalizedResource that = (LocalizedResource) o;

        return Objects.equals(absolutePath, that.absolutePath);
    }

    @Override
    public int hashCode() {
        return absolutePath != null ? absolutePath.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "LocalizedResource{" +
                "absolutePath='" + absolutePath + '\'' +
                '}';
    }

    private static final Pattern localePattern = Pattern.compile("(?<filename>.*?)-(?<locale>[A-Za-z]{2})$");

    public static LocalizedResource from(Path path) {
        var fileName = path.getFileName().toString();
        var fileBaseName = FilenameUtils.getBaseName(fileName);
        var fileExtension = FilenameUtils.getExtension(fileName);

        var matcher = localePattern.matcher(fileBaseName);
        var found = matcher.find();
        var fileLocale = found ? Language.of(matcher.group("locale")) : Language.defaultLanguage();
        var fileBaseNameWithoutExtension = found ? matcher.group("filename") : fileBaseName;

        return new LocalizedResource(
                path.toAbsolutePath().toString(),
                fileBaseNameWithoutExtension,
                fileLocale,
                fileExtension
        );
    }
}
