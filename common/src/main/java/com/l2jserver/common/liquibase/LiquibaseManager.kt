package com.l2jserver.common.liquibase

import com.l2jserver.common.pool.impl.ConnectionFactory
import liquibase.Contexts
import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.exception.LiquibaseException
import liquibase.resource.ClassLoaderResourceAccessor
import org.apache.logging.log4j.LogManager
import java.sql.SQLException

object LiquibaseManager {
    private val LOG = LogManager.getLogger()

    @JvmStatic
    fun executeDatabaseUpdates() {
        try {
            ConnectionFactory.getInstance().connection.use { connection ->
                val database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(JdbcConnection(connection))
                val liquibase = Liquibase("liquibase/changelog.yml", ClassLoaderResourceAccessor(), database)
                liquibase.update(Contexts())
                LOG.info("Successfully applied database updates")
            }
        } catch (e: SQLException) {
            LOG.error("Failed to execute database updates: {}", e.message)
            throw IllegalStateException(e)
        } catch (e: LiquibaseException) {
            LOG.error("Failed to execute database updates: {}", e.message)
            throw IllegalStateException(e)
        }
    }

}