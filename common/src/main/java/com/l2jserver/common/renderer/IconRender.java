package com.l2jserver.common.renderer;

public class IconRender {

    public static String render(String icon) {
        return "<img height=\"32\" width=\"32\" align=\"center\" src=\"" + icon + "\">";
    }

}
