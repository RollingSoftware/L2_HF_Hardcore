package com.l2jserver.common.renderer;

public class ButtonRender {

    public static String render(String name, String action) {
        return render(name, action, 150, 22);
    }

    public static String render(String name, String action, int width) {
        return render(name, action, width, 22);
    }

    public static String render(String name, String action, int width, int height) {
        return "<button value=\"" + name + "\"" +
                "action=\"" + action + "\"" +
                "width=" + width + " height=" + height + " back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">";
    }

}
