package com.l2jserver.common.monad

import com.l2jserver.common.util.StringUtil

data class ProcessResult(val isSuccess: Boolean, val result: String) {
    val isFailure: Boolean
        get() = !isSuccess

    companion object {
        @JvmStatic
        fun failure(result: String): ProcessResult {
            return ProcessResult(false, result)
        }

        @JvmStatic
        fun success(): ProcessResult {
            return ProcessResult(true, StringUtil.EMPTY)
        }

        @JvmStatic
        fun success(text: String): ProcessResult {
            return ProcessResult(true, text)
        }
    }
}