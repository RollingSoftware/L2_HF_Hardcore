package com.l2jserver.common.monad

import com.l2jserver.common.util.StringUtil

data class ProcessResultCarrier<T>(val result: T, val message: String, val isSuccess: Boolean) {
    companion object {
        @JvmStatic
        fun <T> failure(result: String): ProcessResultCarrier<T?> {
            return ProcessResultCarrier(null, result, false)
        }

        @JvmStatic
        fun <T> success(item: T): ProcessResultCarrier<T> {
            return ProcessResultCarrier(item, StringUtil.EMPTY, true)
        }

        @JvmStatic
        fun <T> success(item: T, text: String): ProcessResultCarrier<T> {
            return ProcessResultCarrier(item, text, true)
        }
    }
}