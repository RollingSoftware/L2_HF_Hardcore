plugins {
    id("com.github.johnrengelman.shadow").version("7.1.0")
    id("distribution")
    id("org.jetbrains.kotlin.plugin.noarg") apply true
    kotlin("jvm") apply true
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":mmocore"))
    implementation(project(":common"))

    implementation("com.sun.mail:javax.mail:1.+")
    implementation("org.mariadb.jdbc:mariadb-java-client:2.7.+")
    implementation("com.google.guava:guava:31.+")

    implementation("commons-codec:commons-codec:1.+")

    implementation("com.fasterxml.jackson.core:jackson-core:2.1+")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.1+")
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.1+")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.1+")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.1+")

    implementation(files("dist/libs/L2J_GeoDriver.jar"))

    implementation("org.jdbi:jdbi3-core:3.+")
    implementation("org.jdbi:jdbi3-sqlobject:3.+")

    implementation("org.apache.logging.log4j:log4j-api:2.+")
    implementation("org.apache.logging.log4j:log4j-core:2.+")
    implementation("org.apache.logging.log4j:log4j-slf4j18-impl:2.+")
    implementation("org.apache.logging.log4j:log4j-jul:2.+")
    implementation("org.apache.logging.log4j:log4j-layout-template-json:2.+")

    implementation("io.micrometer:micrometer-registry-prometheus:latest.release")

    testImplementation("org.assertj:assertj-core:3.+")
    testImplementation("org.mockito:mockito-core:2.+")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.+")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.+")
}

java {
    sourceCompatibility = JavaVersion.VERSION_15
    targetCompatibility = JavaVersion.VERSION_15
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "15"
    }
}

tasks {
    test {
        useJUnitPlatform()
    }

    jar {
        archiveFileName.set("l2jserver-thin.jar")
        manifest.attributes(
            "Multi-Release" to "true",
            "Main-Class" to "com.l2jserver.gameserver.GameServer"
        )
    }

    shadowJar {
        archiveFileName.set("l2jserver.jar")
        manifest {
            attributes(
                "Multi-Release" to "true",
                "Main-Class" to "com.l2jserver.gameserver.GameServer"
            )
        }
        transform(com.github.jengelman.gradle.plugins.shadow.transformers.Log4j2PluginsCacheFileTransformer::class.java)
    }

    assemble {
        dependsOn(shadowJar)
    }

    task("zip", Zip::class) {
        dependsOn(build)

        from("dist")

        into("game") {
            from(shadowJar)
        }

        val filename = "l2j_gameserver.zip"
        archiveFileName.set(filename)

        println("Build in build/distributions/$filename")
    }

    build {
        finalizedBy(getByName("zip"))
    }
}

noArg {
    annotation("com.l2jserver.common.noarg.NoArg")
    invokeInitializers = true
}