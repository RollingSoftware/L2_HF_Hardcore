--liquibase formatted sql
--changeset rofgar:olympiad_cycle_class
CREATE TABLE IF NOT EXISTS `olympiad_cycle_class` (
  `class_id` INT UNSIGNED NOT NULL,
  `character_id` INT UNSIGNED NOT NULL,
  `olympiad_cycle` INT UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE UNIQUE INDEX uidx_olympiad_cycle_class
ON olympiad_cycle_class(class_id, character_id, olympiad_cycle);