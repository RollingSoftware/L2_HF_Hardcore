--liquibase formatted sql
--changeset rofgar:custom_variables
DROP TABLE IF EXISTS custom_variables;
CREATE TABLE IF NOT EXISTS custom_variables (
    var  VARCHAR(255),
    value VARCHAR(255)
);

INSERT INTO custom_variables VALUES ('MaxOnline', '0');
