--liquibase formatted sql
--changeset rofgar:is_bot_flag
ALTER TABLE characters ADD COLUMN is_bot bool;