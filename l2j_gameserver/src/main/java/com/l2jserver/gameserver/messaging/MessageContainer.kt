package com.l2jserver.gameserver.messaging

import java.util.concurrent.CopyOnWriteArrayList

class MessageContainer<MessageType> {
    private val listeners: CopyOnWriteArrayList<(message: MessageType) -> Unit> = CopyOnWriteArrayList()

    fun register(callback: (message: MessageType) -> Unit) {
        listeners.add(callback)
    }

    fun notifyAll(messageType: MessageType) {
        listeners.forEach { callback ->
            callback(messageType)
        }
    }
}