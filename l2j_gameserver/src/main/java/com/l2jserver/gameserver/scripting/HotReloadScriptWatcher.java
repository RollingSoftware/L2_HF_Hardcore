package com.l2jserver.gameserver.scripting;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HotReloadScriptWatcher implements Runnable {

    private static final Logger LOG = LogManager.getLogger();

    private final List<File> watchDirectories;
    private final ScriptEngineManager scriptEngineManager;
    private final AtomicBoolean stopping = new AtomicBoolean(false);

    public HotReloadScriptWatcher(ScriptEngineManager scriptEngineManager, List<File> watchDirectories) {
        this.watchDirectories = watchDirectories;
        this.scriptEngineManager = scriptEngineManager;
    }

    public void stop() {
        stopping.set(true);
    }

    @Override
    public void run() {
        try {
            WatchService watchService
                    = FileSystems.getDefault().newWatchService();

            watchDirectories.forEach(file -> {
                Path path = Paths.get(file.toURI());
                try {
                    var directories = Files
                            .walk(path)
                            .filter(Files::isDirectory)
                            .collect(Collectors.toSet());
                    for (Path dir : directories) {
                        dir.register(
                                watchService,
                                StandardWatchEventKinds.ENTRY_CREATE,
                                StandardWatchEventKinds.ENTRY_DELETE,
                                StandardWatchEventKinds.ENTRY_MODIFY
                        );
                    }
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            });

            while (!stopping.get()) {
                WatchKey key;
                Set<String> filesToReload = new HashSet<>();
                while ((key = watchService.poll(500, TimeUnit.MILLISECONDS)) != null) {
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();
                        if (StandardWatchEventKinds.ENTRY_CREATE.equals(kind)) {
                            LOG.debug("Created {}", event.context());
                        } else if (StandardWatchEventKinds.ENTRY_DELETE.equals(kind)) {
                            LOG.debug("Deleted {}", event.context());
                        } else if (StandardWatchEventKinds.ENTRY_MODIFY.equals(kind)) {
                            var filename = event.context().toString();
                            if (filename.endsWith(".class") && !filename.contains("$")) {
                                LOG.debug("Modified {}", event.context());
                                filesToReload.add(event.context().toString());
                            }
                        }
                    }
                    key.reset();
                }

                if (!filesToReload.isEmpty()) {
                    LOG.info("Reloading {} classes", filesToReload.size());
                    filesToReload.forEach(scriptEngineManager::reloadScriptByName);
                }
            }
        } catch (IOException | InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
