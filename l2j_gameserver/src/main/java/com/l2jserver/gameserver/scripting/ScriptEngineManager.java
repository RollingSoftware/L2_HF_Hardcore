/*
 * Copyright (C) 2004-2016 L2J Server
 *
 * This file is part of L2J Server.
 *
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.scripting;

import com.l2jserver.Config;
import com.l2jserver.common.classloader.DynamicUrlClassloader;
import com.l2jserver.common.config.CommonConfig;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.model.events.AbstractScript;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class ScriptEngineManager {
    private static final Logger LOG = LogManager.getLogger();

    public static List<File> SCRIPT_ROOTS;
    public static File SCRIPT_RESOURCE_ROOT;

    public static ScriptEngineManager getInstance() {
        return SingletonHolder._instance;
    }

    private Map<String, LiveScript> loadedScripts = new ConcurrentHashMap<>();

    private static final boolean PURGE_ERROR_LOG = true;

    private final DynamicUrlClassloader classLoader;

    public ScriptEngineManager() {
        if (SCRIPT_ROOTS == null) {
            SCRIPT_ROOTS = Collections.singletonList(
                    new File(CommonConfig.DATAPACK_ROOT.getAbsolutePath(), "data/scripts")
            );
        }

        if (SCRIPT_RESOURCE_ROOT == null) {
            SCRIPT_RESOURCE_ROOT = new File(CommonConfig.DATAPACK_ROOT.getAbsolutePath(), "data/scripts");
        }

        classLoader = new DynamicUrlClassloader(SCRIPT_ROOTS.stream().map(root -> {
            try {
                return root.toURI().toURL();
            } catch (MalformedURLException e) {
                throw new IllegalStateException(e);
            }
        }).toArray(URL[]::new));

        ThreadPoolManager.getInstance().executeGeneral(
                new HotReloadScriptWatcher(this, SCRIPT_ROOTS)
        );
    }

    public File findChildScriptFile(String childFile) {
        for (File scriptsRoot : SCRIPT_ROOTS) {
            var file = new File(scriptsRoot, childFile);
            if (file.exists() && file.isFile()) {
                return file;
            }
        }
        throw new IllegalArgumentException("File " + childFile + " is not found in any of script roots");
    }

    public void executeScriptList(File list) throws IOException {
        if (Config.ALT_DEV_NO_QUESTS) {
            try {
                executeScript(findChildScriptFile("com/l2j/datapack/handlers/MasterHandler.class"));
                LOG.info("Handlers loaded, all other scripts skipped");
            } catch (RuntimeException se) {
                LOG.warn("Failed to load com/l2j/datapack/handlers/MasterHandler.java", se);
            }
            return;
        }

        if (!list.isFile()) {
            throw new IllegalArgumentException("Argument must be a file containing a list of scripts to be loaded");
        }

        try (FileInputStream fis = new FileInputStream(list);
             InputStreamReader isr = new InputStreamReader(fis);
             LineNumberReader lnr = new LineNumberReader(isr)) {
            String line;
            while ((line = lnr.readLine()) != null) {
                String[] parts = line.trim().split("#");

                if ((parts.length > 0) && !parts[0].isEmpty() && (parts[0].charAt(0) != '#')) {
                    line = parts[0];
                    executeScript(findChildScriptFile(line));
                }
            }
        }
    }

    private void executeScript(File file) {
        if (PURGE_ERROR_LOG) {
            String name = file.getAbsolutePath() + ".error.log";
            File errorLog = new File(name);
            if (errorLog.isFile()) {
                errorLog.delete();
            }
        }

        String contentHash = contentHash(file.toPath());
        var classForFile = getClassForFile(file)
                .replace('/', '.')
                .replace('\\', '.')
                .replace("\\.java", ".class");

        LOG.debug("Loading new script '{}'", classForFile);
        try {
            Class<?> cls = classLoader.loadClass(classForFile);
            var constructor = cls.getDeclaredConstructor();
            constructor.trySetAccessible();
            var instance = constructor.newInstance();
            loadedScripts.put(classForFile, new LiveScript(instance, contentHash));
        } catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException | RuntimeException e) {
            LOG.warn("Error executing script!", e);
            reportScriptFileError(file, e);
        }
    }

    private String contentHash(Path filePath) {
        try (InputStream is = Files.newInputStream(filePath)) {
            return DigestUtils.sha3_256Hex(is);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void reloadScript(File file) {
        if (PURGE_ERROR_LOG) {
            String name = file.getAbsolutePath() + ".error.log";
            File errorLog = new File(name);
            if (errorLog.isFile()) {
                errorLog.delete();
            }
        }

        String contentHash = contentHash(file.toPath());
        var classForFile = getClassForFile(file)
                .replace('/', '.')
                .replace('\\', '.')
                .replace("\\.java", ".class");

        LOG.debug("Deciding to reload script '{}'", classForFile);

        var existingScriptInstance = loadedScripts.get(classForFile);
        if (existingScriptInstance == null) {
            LOG.trace("Script '{}' was not previously loaded, skipping", classForFile);
            return;
        }

        var contentHashesEqual = contentHash.equals(existingScriptInstance.getContentHash());

        if (!contentHashesEqual) {
            LOG.info("Trying to reload live script '{}'", classForFile);
            if (existingScriptInstance.getInstance() instanceof AbstractScript) {
                ((AbstractScript) existingScriptInstance.getInstance()).unload();
            }

            try {
                Class<?> cls = classLoader.reloadClass(classForFile);
                var constructor = cls.getDeclaredConstructor();
                constructor.trySetAccessible();
                var instance = constructor.newInstance();
                loadedScripts.put(classForFile, new LiveScript(instance, contentHash));
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException | ClassNotFoundException e) {
                LOG.warn("Error executing script!", e);
                reportScriptFileError(file, e);
            }
        }
    }

    public String getClassForFile(File script) {
        String path = script.getAbsolutePath();
        for (var scriptRoot : SCRIPT_ROOTS) {
            var scpPath = scriptRoot.getAbsolutePath();
            if (path.startsWith(scpPath)) {
                int idx = path.lastIndexOf('.');
                return path.substring(scpPath.length() + 1, idx);
            }
        }
        throw new IllegalArgumentException("Cannot get class name for script '" + script.getAbsolutePath() + "'");
    }

    private void reportScriptFileError(File script, Exception e) {
        String dir = script.getParent();
        String name = script.getName() + ".error.log";
        if (dir != null) {
            final File file = new File(dir + "/" + name);
            try (FileOutputStream fos = new FileOutputStream(file)) {
                String errorHeader = "Error on: " + file.getCanonicalPath() + CommonConfig.EOL;
                fos.write(errorHeader.getBytes());
                fos.write(e.getMessage().getBytes());
                LOG.warn("Failed executing script: " + script.getAbsolutePath() + ". See " + file.getName() + " for details.");
            } catch (IOException ioe) {
                LOG.warn("Failed executing script: " + script.getAbsolutePath() + CommonConfig.EOL + e.getMessage() + "Additionally failed when trying to write an error report on script directory. Reason: " + ioe.getMessage(), ioe);
            }
        } else {
            LOG.warn("Failed executing script: " + script.getAbsolutePath() + CommonConfig.EOL + e.getMessage() + "Additionally failed when trying to write an error report on script directory.", e);
        }
    }

    public void executeScriptByName(String scriptName) {
        executeScript(findChildScriptFile(scriptName));
    }

    public void reloadScriptByName(String scriptName) {
        reloadScript(findChildScriptFile(scriptName));
    }

    private static class SingletonHolder {
        protected static final ScriptEngineManager _instance = new ScriptEngineManager();
    }
}
