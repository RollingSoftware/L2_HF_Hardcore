package com.l2jserver.gameserver.scripting;

import java.util.Objects;

public class LiveScript {

    private Object instance;
    private String contentHash;

    public LiveScript(Object instance, String contentHash) {
        this.instance = instance;
        this.contentHash = contentHash;
    }

    public Object getInstance() {
        return instance;
    }

    public String getContentHash() {
        return contentHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LiveScript that = (LiveScript) o;

        if (!Objects.equals(instance, that.instance)) return false;
        return Objects.equals(contentHash, that.contentHash);
    }

    @Override
    public int hashCode() {
        int result = instance != null ? instance.hashCode() : 0;
        result = 31 * result + (contentHash != null ? contentHash.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LiveScript{" +
                "instance=" + instance +
                ", contentHash='" + contentHash + '\'' +
                '}';
    }
}
