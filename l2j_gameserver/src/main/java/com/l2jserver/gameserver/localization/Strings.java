package com.l2jserver.gameserver.localization;

import com.l2jserver.localization.Language;

public class Strings {

    public static StringsTable lang(Language language) {
        return MultilangTables.getInstance().get(language);
    }

}
