/*
 * Copyright (C) 2004-2016 L2J Server
 *
 * This file is part of L2J Server.
 *
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.Config;
import com.l2jserver.gameserver.data.xml.impl.BuyListData;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2MerchantInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.buylist.L2BuyList;
import com.l2jserver.gameserver.model.holders.UniqueItemHolder;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.network.serverpackets.ActionFailed;
import com.l2jserver.gameserver.network.serverpackets.ExBuySellList;
import com.l2jserver.gameserver.network.serverpackets.StatusUpdate;
import com.l2jserver.gameserver.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.l2jserver.gameserver.model.actor.L2Npc.INTERACTION_DISTANCE;
import static com.l2jserver.gameserver.model.itemcontainer.Inventory.MAX_ADENA;

/**
 * RequestSellItem client packet class.
 */
public final class RequestSellItem extends L2GameClientPacket {
    private static final String _C__37_REQUESTSELLITEM = "[C] 37 RequestSellItem";

    private static final int BATCH_LENGTH = 16;

    private int _listId;
    private List<UniqueItemHolder> _items = null;

    public RequestSellItem() {
    }

    public RequestSellItem(int _listId, List<UniqueItemHolder> _items) {
        this._listId = _listId;
        this._items = _items;
    }

    @Override
    protected void readImpl() {
        _listId = readD();
        int size = readD();
        if ((size <= 0) || (size > Config.MAX_ITEM_IN_PACKET) || ((size * BATCH_LENGTH) != _buf.remaining())) {
            return;
        }

        _items = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            int objectId = readD();
            int itemId = readD();
            long count = readQ();
            if ((objectId < 1) || (itemId < 1) || (count < 1)) {
                _items = null;
                return;
            }
            _items.add(new UniqueItemHolder(itemId, objectId, count));
        }
    }

    @Override
    protected void runImpl() {
        processSell();
    }

    public static Optional<L2MerchantInstance> validateAndCastNpcBuyListConditions(L2PcInstance player, L2BuyList buyList) {
        L2Object target = player.getTarget();
        if (target == null) {
            return Optional.empty();
        }

        L2MerchantInstance merchant;
        if (target instanceof L2MerchantInstance) {
            merchant = (L2MerchantInstance) target;
        } else {
            return Optional.empty();
        }

        if (player.getInstanceId() != target.getInstanceId()) {
            return Optional.empty();
        }

        if (!player.isInsideRadius(target, INTERACTION_DISTANCE, true, false)) {
            return Optional.empty();
        }

        if (buyList.isNpcNotAllowed(merchant.getId())) {
            return Optional.empty();
        }

        return Optional.of(merchant);
    }

    private void processSell() {
        L2PcInstance player = getClient().getActiveChar();

        if (player == null) {
            return;
        }

        if (!getClient().getFloodProtectors().getTransaction().tryPerformAction("buy")) {
            player.sendMessage("You are buying too fast.");
            return;
        }

        if (_items == null) {
            sendPacket(ActionFailed.STATIC_PACKET);
            return;
        }

        if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.hasKarma()) {
            sendPacket(ActionFailed.STATIC_PACKET);
            return;
        }


        final L2BuyList buyList = BuyListData.getInstance().getBuyList(_listId);
        if (buyList == null) {
            Util.handleIllegalPlayerAction(player, "Warning!! Character " + player.getName() + " of account " + player.getAccountName() + " sent a false BuyList list_id " + _listId, Config.DEFAULT_PUNISH);
            return;
        }

        L2Character merchant = null;
        boolean selfService;
        if (buyList.isNpcOnly()) {
            var optional = validateAndCastNpcBuyListConditions(player, buyList);
            if (optional.isPresent()) {
                merchant = optional.get();
                selfService = false;
            } else {
                sendPacket(ActionFailed.STATIC_PACKET);
                return;
            }
        } else {
            selfService = true;
        }

        long totalPrice = 0;
        for (UniqueItemHolder i : _items) {
            L2ItemInstance item = player.checkItemManipulation(i.getObjectId(), i.getCount(), "sell");
            if ((item == null) || (!item.isSellable())) {
                continue;
            }

            long price = item.getReferencePrice() / 2;
            totalPrice += price * i.getCount();
            if (((MAX_ADENA / i.getCount()) < price) || (totalPrice > MAX_ADENA)) {
                Util.handleIllegalPlayerAction(player, "Warning!! Character " + player.getName() + " of account " + player.getAccountName() + " tried to purchase over " + MAX_ADENA + " adena worth of goods.", Config.DEFAULT_PUNISH);
                return;
            }

            if (Config.ALLOW_REFUND && merchant != null) {
                player.getInventory().transferItem("Sell", i.getObjectId(), i.getCount(), player.getRefund(), player, merchant);
            } else {
                player.getInventory().destroyItem("Sell", i.getObjectId(), i.getCount(), player, selfService ? "NPC-Less Buylist" : merchant);
            }
        }
        player.addAdena("Sell", totalPrice, merchant, false);

        // Update current load as well
        StatusUpdate su = new StatusUpdate(player);
        su.addAttribute(StatusUpdate.CUR_LOAD, player.getCurrentLoad());
        player.sendPacket(su);
        player.sendPacket(new ExBuySellList(player, true));
    }

    @Override
    public String getType() {
        return _C__37_REQUESTSELLITEM;
    }
}
