/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver;

import com.l2jserver.Config;
import com.l2jserver.common.config.CommonConfig;
import com.l2jserver.common.util.StringUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 * This class is made to handle all the ThreadPools used in L2J.
 * </p>
 * <p>
 * Scheduled Tasks can either be sent to a {@link #generalScheduledThreadPool "general"} or {@link #effectsScheduledThreadPool "effects"} {@link ScheduledThreadPoolExecutor ScheduledThreadPool}: The "effects" one is used for every effects (skills, hp/mp regen ...) while the "general" one is used
 * for everything else that needs to be scheduled.<br>
 * There also is an {@link #aiScheduledThreadPool "ai"} {@link ScheduledThreadPoolExecutor ScheduledThreadPool} used for AI Tasks.
 * </p>
 * <p>
 * Tasks can be sent to {@link ScheduledThreadPoolExecutor ScheduledThreadPool} either with:
 * <ul>
 * <li>{@link #scheduleEffect(Runnable, long, TimeUnit)} and {@link #scheduleEffect(Runnable, long)} : for effects Tasks that needs to be executed only once.</li>
 * <li>{@link #scheduleGeneral(Runnable, long, TimeUnit)} and {@link #scheduleGeneral(Runnable, long)} : for scheduled Tasks that needs to be executed once.</li>
 * </ul>
 * or
 * <ul>
 * <li>{@link #scheduleEffectAtFixedRate(Runnable, long, long, TimeUnit)} and {@link #scheduleEffectAtFixedRate(Runnable, long, long)} : for effects Tasks that needs to be executed periodically.</li>
 * <li>{@link #scheduleGeneralAtFixedRate(Runnable, long, long, TimeUnit)} and {@link #scheduleGeneralAtFixedRate(Runnable, long, long)} : for scheduled Tasks that needs to be executed periodically.</li>
 * <li>{@link #scheduleAiAtFixedRate(Runnable, long, long, TimeUnit)} and {@link #scheduleAiAtFixedRate(Runnable, long, long)} : for AI Tasks that needs to be executed periodically</li>
 * </ul>
 * </p>
 * <p>
 * For all Tasks that should be executed with no delay asynchronously in a ThreadPool there also are usual {@link ThreadPoolExecutor ThreadPools} that can grow/shrink according to their load.:
 * <ul>
 * <li>{@link #generalPacketsThreadPool GeneralPackets} where most packets handler are executed.</li>
 * <li>{@link #ioPacketsThreadPool I/O Packets} where all the i/o packets are executed.</li>
 * <li>There will be an AI ThreadPool where AI events should be executed</li>
 * <li>A general ThreadPool where everything else that needs to run asynchronously with no delay should be executed ({@link com.l2jserver.gameserver.model.actor.knownlist KnownList} updates, SQL updates/inserts...)?</li>
 * </ul>
 * </p>
 *
 * @author -Wooden-
 */
public class ThreadPoolManager {
	protected static final Logger LOG = LogManager.getLogger();

	private static final class RunnableWrapper implements Runnable {
		private final Runnable _r;

		public RunnableWrapper(final Runnable r) {
			_r = r;
		}
		
		@Override
		public final void run()
		{
			try
			{
				_r.run();
			}
			catch (final Throwable e) {
                final Thread t = Thread.currentThread();
                final UncaughtExceptionHandler h = t.getUncaughtExceptionHandler();
                if (h != null) {
                    h.uncaughtException(t, e);
                }
            }
        }
    }

    private ScheduledThreadPoolExecutor effectsScheduledThreadPool;
    private ScheduledThreadPoolExecutor generalScheduledThreadPool;
	private ScheduledThreadPoolExecutor aiScheduledThreadPool;
	private ScheduledThreadPoolExecutor botAiScheduledThreadPool;
	private ScheduledThreadPoolExecutor eventScheduledThreadPool;
    private final ThreadPoolExecutor generalPacketsThreadPool;
    private final ThreadPoolExecutor ioPacketsThreadPool;
    private final ThreadPoolExecutor generalThreadPool;
    private final ThreadPoolExecutor eventThreadPool;

    private boolean _shutdown;

    public static ThreadPoolManager getInstance() {
        return SingletonHolder._instance;
    }

    protected ThreadPoolManager() {
        effectsScheduledThreadPool = new ScheduledThreadPoolExecutor(Config.THREAD_P_EFFECTS, new PriorityThreadFactory("EffectsSTPool", Thread.NORM_PRIORITY));
        generalScheduledThreadPool = new ScheduledThreadPoolExecutor(Config.THREAD_P_GENERAL, new PriorityThreadFactory("GeneralSTPool", Thread.NORM_PRIORITY));
        eventScheduledThreadPool = new ScheduledThreadPoolExecutor(Config.THREAD_E_EVENTS, new PriorityThreadFactory("EventSTPool", Thread.NORM_PRIORITY));
        ioPacketsThreadPool = new ThreadPoolExecutor(Config.IO_PACKET_THREAD_CORE_SIZE, Integer.MAX_VALUE, 5L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new PriorityThreadFactory("I/O Packet Pool", Thread.NORM_PRIORITY + 1));
        generalPacketsThreadPool = new ThreadPoolExecutor(Config.GENERAL_PACKET_THREAD_CORE_SIZE, Config.GENERAL_PACKET_THREAD_CORE_SIZE + 2, 15L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new PriorityThreadFactory("Normal Packet Pool", Thread.NORM_PRIORITY + 1));
        generalThreadPool = new ThreadPoolExecutor(Config.GENERAL_THREAD_CORE_SIZE, Config.GENERAL_THREAD_CORE_SIZE + 2, 5L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new PriorityThreadFactory("General Pool", Thread.NORM_PRIORITY));
		aiScheduledThreadPool = new ScheduledThreadPoolExecutor(Config.AI_MAX_THREAD, new PriorityThreadFactory("AISTPool", Thread.NORM_PRIORITY));
		botAiScheduledThreadPool = new ScheduledThreadPoolExecutor(Config.AI_MAX_THREAD, new PriorityThreadFactory("BAISTPool", Thread.NORM_PRIORITY));
		eventThreadPool = new ThreadPoolExecutor(Config.EVENT_MAX_THREAD, Config.EVENT_MAX_THREAD + 2, 5L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new PriorityThreadFactory("Event Pool", Thread.NORM_PRIORITY));

        scheduleGeneralAtFixedRate(new PurgeTask(), 10, 5, TimeUnit.MINUTES);
	}
	
	/**
	 * Schedules an effect task to be executed after the given delay.
	 * @param task the task to execute
	 * @param delay the delay in the given time unit
	 * @param unit the time unit of the delay parameter
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleEffect(Runnable task, long delay, TimeUnit unit)
	{
		try {
            return effectsScheduledThreadPool.schedule(new RunnableWrapper(task), delay, unit);
        }
		catch (RejectedExecutionException e)
		{
			return null;
		}
	}
	
	/**
	 * Schedules an effect task to be executed after the given delay.
	 * @param task the task to execute
	 * @param delay the delay in milliseconds
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleEffect(Runnable task, long delay)
	{
		return scheduleEffect(task, delay, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Schedules an effect task to be executed at fixed rate.
	 * @param task the task to execute
	 * @param initialDelay the initial delay in the given time unit
	 * @param period the period between executions in the given time unit
	 * @param unit the time unit of the initialDelay and period parameters
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleEffectAtFixedRate(Runnable task, long initialDelay, long period, TimeUnit unit)
	{
		try {
            return effectsScheduledThreadPool.scheduleAtFixedRate(new RunnableWrapper(task), initialDelay, period, unit);
        }
		catch (RejectedExecutionException e)
		{
			return null; /* shutdown, ignore */
		}
	}
	
	/**
	 * Schedules an effect task to be executed at fixed rate.
	 * @param task the task to execute
	 * @param initialDelay the initial delay in milliseconds
	 * @param period the period between executions in milliseconds
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleEffectAtFixedRate(Runnable task, long initialDelay, long period)
	{
		return scheduleEffectAtFixedRate(task, initialDelay, period, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Schedules a general task to be executed after the given delay.
	 * @param task the task to execute
	 * @param delay the delay in the given time unit
	 * @param unit the time unit of the delay parameter
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleGeneral(Runnable task, long delay, TimeUnit unit)
	{
		try {
            return generalScheduledThreadPool.schedule(new RunnableWrapper(task), delay, unit);
        }
		catch (RejectedExecutionException e)
		{
			return null; /* shutdown, ignore */
		}
	}
	
	/**
	 * Schedules a general task to be executed after the given delay.
	 * @param task the task to execute
	 * @param delay the delay in milliseconds
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleGeneral(Runnable task, long delay)
	{
		return scheduleGeneral(task, delay, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Schedules a general task to be executed at fixed rate.
	 * @param task the task to execute
	 * @param initialDelay the initial delay in the given time unit
	 * @param period the period between executions in the given time unit
	 * @param unit the time unit of the initialDelay and period parameters
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleGeneralAtFixedRate(Runnable task, long initialDelay, long period, TimeUnit unit)
	{
		try {
            return generalScheduledThreadPool.scheduleAtFixedRate(new RunnableWrapper(task), initialDelay, period, unit);
        }
		catch (RejectedExecutionException e)
		{
			return null; /* shutdown, ignore */
		}
	}
	
	/**
	 * Schedules a event task to be executed after the given delay.
	 * @param task the task to execute
	 * @param delay the delay in the given time unit
	 * @param unit the time unit of the delay parameter
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleEvent(Runnable task, long delay, TimeUnit unit)
	{
		try {
            return eventScheduledThreadPool.schedule(new RunnableWrapper(task), delay, unit);
        }
		catch (RejectedExecutionException e)
		{
			return null; /* shutdown, ignore */
		}
	}
	
	/**
	 * Schedules a event task to be executed after the given delay.
	 * @param task the task to execute
	 * @param delay the delay in milliseconds
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleEvent(Runnable task, long delay)
	{
		return scheduleEvent(task, delay, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Schedules a event task to be executed at fixed rate.
	 * @param task the task to execute
	 * @param initialDelay the initial delay in the given time unit
	 * @param period the period between executions in the given time unit
	 * @param unit the time unit of the initialDelay and period parameters
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleEventAtFixedRate(Runnable task, long initialDelay, long period, TimeUnit unit)
	{
		try {
            return eventScheduledThreadPool.scheduleAtFixedRate(new RunnableWrapper(task), initialDelay, period, unit);
        }
		catch (RejectedExecutionException e)
		{
			return null; /* shutdown, ignore */
		}
	}
	
	/**
	 * Schedules a general task to be executed at fixed rate.
	 * @param task the task to execute
	 * @param initialDelay the initial delay in milliseconds
	 * @param period the period between executions in milliseconds
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleGeneralAtFixedRate(Runnable task, long initialDelay, long period)
	{
		return scheduleGeneralAtFixedRate(task, initialDelay, period, TimeUnit.MILLISECONDS);
	}

	/**
	 * Schedules an AI task to be executed after the given delay.
	 * @param task the task to execute
	 * @param delay the delay in the given time unit
	 * @param unit the time unit of the delay parameter
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleAi(Runnable task, long delay, TimeUnit unit)
	{
		try {
            return aiScheduledThreadPool.schedule(new RunnableWrapper(task), delay, unit);
        }
		catch (RejectedExecutionException e)
		{
			return null; /* shutdown, ignore */
		}
	}
	
	/**
	 * Schedules an AI task to be executed after the given delay.
	 * @param task the task to execute
	 * @param delay the delay in milliseconds
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleAi(Runnable task, long delay)
	{
		return scheduleAi(task, delay, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Schedules a general task to be executed at fixed rate.
	 * @param task the task to execute
	 * @param initialDelay the initial delay in the given time unit
	 * @param period the period between executions in the given time unit
	 * @param unit the time unit of the initialDelay and period parameters
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleAiAtFixedRate(Runnable task, long initialDelay, long period, TimeUnit unit) {
		try {
			return aiScheduledThreadPool.scheduleAtFixedRate(new RunnableWrapper(task), initialDelay, period, unit);
		} catch (RejectedExecutionException e) {
			return null; /* shutdown, ignore */
		}
	}

	public ScheduledFuture<?> scheduleBotAiAtFixedRate(Runnable task, long initialDelay, long period) {
		try {
			return botAiScheduledThreadPool.scheduleAtFixedRate(new RunnableWrapper(task), initialDelay, period, TimeUnit.MILLISECONDS);
		} catch (RejectedExecutionException e) {
			return null; /* shutdown, ignore */
		}
	}

	public ScheduledFuture<?> scheduleBotAiAtFixedRate(Runnable task, long initialDelay, long period, TimeUnit unit) {
		try {
			return botAiScheduledThreadPool.scheduleAtFixedRate(new RunnableWrapper(task), initialDelay, period, unit);
		} catch (RejectedExecutionException e) {
			return null; /* shutdown, ignore */
		}
	}

	/**
	 * Schedules a general task to be executed at fixed rate.
	 *
	 * @param task         the task to execute
	 * @param initialDelay the initial delay in milliseconds
	 * @param period       the period between executions in milliseconds
	 * @return a ScheduledFuture representing pending completion of the task, and whose get() method will throw an exception upon cancellation
	 */
	public ScheduledFuture<?> scheduleAiAtFixedRate(Runnable task, long initialDelay, long period) {
		return scheduleAiAtFixedRate(task, initialDelay, period, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Executes a packet task sometime in future in another thread.
	 * @param task the task to execute
	 */
	public void executePacket(Runnable task)
	{
		try {
            generalPacketsThreadPool.execute(task);
        }
		catch (RejectedExecutionException e)
		{
			/* shutdown, ignore */
		}
	}
	
	/**
	 * Executes an IO packet task sometime in future in another thread.
	 * @param task the task to execute
	 */
	public void executeIOPacket(Runnable task)
	{
		try {
            ioPacketsThreadPool.execute(task);
        }
		catch (RejectedExecutionException e)
		{
			/* shutdown, ignore */
		}
	}
	
	/**
	 * Executes a general task sometime in future in another thread.
	 * @param task the task to execute
	 */
	public void executeGeneral(Runnable task)
	{
		try {
            generalThreadPool.execute(new RunnableWrapper(task));
        }
		catch (RejectedExecutionException e)
		{
			/* shutdown, ignore */
		}
	}
	
	/**
	 * Executes an AI task sometime in future in another thread.
	 * @param task the task to execute
	 */
	public void executeAi(Runnable task)
	{
		try {
            aiScheduledThreadPool.execute(new RunnableWrapper(task));
        }
		catch (RejectedExecutionException e)
		{
			/* shutdown, ignore */
		}
	}
	
	/**
	 * Executes an Event task sometime in future in another thread.
	 * @param task the task to execute
	 */
	public void executeEvent(Runnable task)
	{
		try {
            eventThreadPool.execute(new RunnableWrapper(task));
        }
		catch (RejectedExecutionException e)
		{
			/* shutdown, ignore */
		}
	}
	
	public String[] getStats()
	{
		return new String[]
                {
                        "STP:",
                        " + Effects:",
                        " |- ActiveThreads:   " + effectsScheduledThreadPool.getActiveCount(),
                        " |- getCorePoolSize: " + effectsScheduledThreadPool.getCorePoolSize(),
                        " |- PoolSize:        " + effectsScheduledThreadPool.getPoolSize(),
                        " |- MaximumPoolSize: " + effectsScheduledThreadPool.getMaximumPoolSize(),
                        " |- CompletedTasks:  " + effectsScheduledThreadPool.getCompletedTaskCount(),
                        " |- ScheduledTasks:  " + effectsScheduledThreadPool.getQueue().size(),
                        " | -------",
                        " + General:",
                        " |- ActiveThreads:   " + generalScheduledThreadPool.getActiveCount(),
                        " |- getCorePoolSize: " + generalScheduledThreadPool.getCorePoolSize(),
                        " |- PoolSize:        " + generalScheduledThreadPool.getPoolSize(),
                        " |- MaximumPoolSize: " + generalScheduledThreadPool.getMaximumPoolSize(),
                        " |- CompletedTasks:  " + generalScheduledThreadPool.getCompletedTaskCount(),
						" |- ScheduledTasks:  " + generalScheduledThreadPool.getQueue().size(),
						" | -------",
						" + AI:",
						" |- ActiveThreads:   " + aiScheduledThreadPool.getActiveCount(),
						" |- getCorePoolSize: " + aiScheduledThreadPool.getCorePoolSize(),
						" |- PoolSize:        " + aiScheduledThreadPool.getPoolSize(),
						" |- MaximumPoolSize: " + aiScheduledThreadPool.getMaximumPoolSize(),
						" |- CompletedTasks:  " + aiScheduledThreadPool.getCompletedTaskCount(),
						" |- ScheduledTasks:  " + aiScheduledThreadPool.getQueue().size(),
						" | -------",
						" + Bot AI:",
						" |- ActiveThreads:   " + botAiScheduledThreadPool.getActiveCount(),
						" |- getCorePoolSize: " + botAiScheduledThreadPool.getCorePoolSize(),
						" |- PoolSize:        " + botAiScheduledThreadPool.getPoolSize(),
						" |- MaximumPoolSize: " + botAiScheduledThreadPool.getMaximumPoolSize(),
						" |- CompletedTasks:  " + botAiScheduledThreadPool.getCompletedTaskCount(),
						" |- ScheduledTasks:  " + botAiScheduledThreadPool.getQueue().size(),
						" | -------",
						" + Event:",
						" |- ActiveThreads:   " + eventScheduledThreadPool.getActiveCount(),
						" |- getCorePoolSize: " + eventScheduledThreadPool.getCorePoolSize(),
						" |- PoolSize:        " + eventScheduledThreadPool.getPoolSize(),
						" |- MaximumPoolSize: " + eventScheduledThreadPool.getMaximumPoolSize(),
						" |- CompletedTasks:  " + eventScheduledThreadPool.getCompletedTaskCount(),
						" |- ScheduledTasks:  " + eventScheduledThreadPool.getQueue().size(),
						"TP:",
						" + Packets:",
						" |- ActiveThreads:   " + generalPacketsThreadPool.getActiveCount(),
                        " |- getCorePoolSize: " + generalPacketsThreadPool.getCorePoolSize(),
                        " |- MaximumPoolSize: " + generalPacketsThreadPool.getMaximumPoolSize(),
                        " |- LargestPoolSize: " + generalPacketsThreadPool.getLargestPoolSize(),
                        " |- PoolSize:        " + generalPacketsThreadPool.getPoolSize(),
                        " |- CompletedTasks:  " + generalPacketsThreadPool.getCompletedTaskCount(),
                        " |- QueuedTasks:     " + generalPacketsThreadPool.getQueue().size(),
                        " | -------",
                        " + I/O Packets:",
                        " |- ActiveThreads:   " + ioPacketsThreadPool.getActiveCount(),
                        " |- getCorePoolSize: " + ioPacketsThreadPool.getCorePoolSize(),
                        " |- MaximumPoolSize: " + ioPacketsThreadPool.getMaximumPoolSize(),
                        " |- LargestPoolSize: " + ioPacketsThreadPool.getLargestPoolSize(),
                        " |- PoolSize:        " + ioPacketsThreadPool.getPoolSize(),
                        " |- CompletedTasks:  " + ioPacketsThreadPool.getCompletedTaskCount(),
                        " |- QueuedTasks:     " + ioPacketsThreadPool.getQueue().size(),
                        " | -------",
                        " + General Tasks:",
                        " |- ActiveThreads:   " + generalThreadPool.getActiveCount(),
                        " |- getCorePoolSize: " + generalThreadPool.getCorePoolSize(),
                        " |- MaximumPoolSize: " + generalThreadPool.getMaximumPoolSize(),
                        " |- LargestPoolSize: " + generalThreadPool.getLargestPoolSize(),
                        " |- PoolSize:        " + generalThreadPool.getPoolSize(),
                        " |- CompletedTasks:  " + generalThreadPool.getCompletedTaskCount(),
                        " |- QueuedTasks:     " + generalThreadPool.getQueue().size(),
                        " | -------",
                        " + Event Tasks:",
                        " |- ActiveThreads:   " + eventThreadPool.getActiveCount(),
                        " |- getCorePoolSize: " + eventThreadPool.getCorePoolSize(),
                        " |- MaximumPoolSize: " + eventThreadPool.getMaximumPoolSize(),
                        " |- LargestPoolSize: " + eventThreadPool.getLargestPoolSize(),
                        " |- PoolSize:        " + eventThreadPool.getPoolSize(),
                        " |- CompletedTasks:  " + eventThreadPool.getCompletedTaskCount(),
                        " |- QueuedTasks:     " + eventThreadPool.getQueue().size(),
                        " | -------"
                };
	}
	
	private static class PriorityThreadFactory implements ThreadFactory
	{
		private final int _prio;
		private final String _name;
		private final AtomicInteger _threadNumber = new AtomicInteger(1);
		private final ThreadGroup _group;
		
		public PriorityThreadFactory(String name, int prio)
		{
			_prio = prio;
			_name = name;
			_group = new ThreadGroup(_name);
		}
		
		@Override
		public Thread newThread(Runnable r)
		{
			Thread t = new Thread(_group, r, _name + "-" + _threadNumber.getAndIncrement());
			t.setPriority(_prio);
			return t;
		}
		
		public ThreadGroup getGroup()
		{
			return _group;
		}
	}
	
	public void shutdown()
	{
		_shutdown = true;
		try {
			effectsScheduledThreadPool.shutdown();
			generalScheduledThreadPool.shutdown();
			generalPacketsThreadPool.shutdown();
			ioPacketsThreadPool.shutdown();
			generalThreadPool.shutdown();
			eventThreadPool.shutdown();
			aiScheduledThreadPool.shutdown();
			botAiScheduledThreadPool.shutdown();
			eventScheduledThreadPool.shutdown();
			effectsScheduledThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			generalScheduledThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			generalPacketsThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			ioPacketsThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			generalThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			eventThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			aiScheduledThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			botAiScheduledThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			eventScheduledThreadPool.awaitTermination(1, TimeUnit.SECONDS);
			LOG.info("All ThreadPools are now stopped");
		}
		catch (InterruptedException e)
		{
			LOG.warn("There has been a problem shutting down the thread pool manager!", e);
		}
	}
	
	public boolean isShutdown()
	{
		return _shutdown;
	}
	
	public void purge() {
        effectsScheduledThreadPool.purge();
        generalScheduledThreadPool.purge();
		aiScheduledThreadPool.purge();
		botAiScheduledThreadPool.purge();
		eventScheduledThreadPool.purge();
        ioPacketsThreadPool.purge();
        generalPacketsThreadPool.purge();
        generalThreadPool.purge();
        eventThreadPool.purge();
	}

	public String getPacketStats() {
        final StringBuilder sb = new StringBuilder(1000);
        ThreadFactory tf = generalPacketsThreadPool.getThreadFactory();
        if (tf instanceof PriorityThreadFactory) {
            PriorityThreadFactory ptf = (PriorityThreadFactory) tf;
            int count = ptf.getGroup().activeCount();
            Thread[] threads = new Thread[count + 2];
            ptf.getGroup().enumerate(threads);
            StringUtil.append(sb, "General Packet Thread Pool:" + CommonConfig.EOL + "Tasks in the queue: ", String.valueOf(generalPacketsThreadPool.getQueue().size()), CommonConfig.EOL + "Showing threads stack trace:" + CommonConfig.EOL + "There should be ", String.valueOf(count), " Threads" + CommonConfig.EOL);
            for (Thread t : threads) {
                if (t == null) {
                    continue;
                }

                StringUtil.append(sb, t.getName(), CommonConfig.EOL);
                for (StackTraceElement ste : t.getStackTrace()) {
                    StringUtil.append(sb, ste.toString(), CommonConfig.EOL);
                }
            }
        }

        sb.append("Packet Tp stack traces printed.");
        sb.append(CommonConfig.EOL);
        return sb.toString();
    }
	
	public String getIOPacketStats() {
        final StringBuilder sb = new StringBuilder(1000);
        ThreadFactory tf = ioPacketsThreadPool.getThreadFactory();

        if (tf instanceof PriorityThreadFactory) {
            PriorityThreadFactory ptf = (PriorityThreadFactory) tf;
            int count = ptf.getGroup().activeCount();
            Thread[] threads = new Thread[count + 2];
            ptf.getGroup().enumerate(threads);
            StringUtil.append(sb, "I/O Packet Thread Pool:" + CommonConfig.EOL + "Tasks in the queue: ", String.valueOf(ioPacketsThreadPool.getQueue().size()), CommonConfig.EOL + "Showing threads stack trace:" + CommonConfig.EOL + "There should be ", String.valueOf(count), " Threads" + CommonConfig.EOL);

            for (Thread t : threads) {
                if (t == null) {
                    continue;
                }

                StringUtil.append(sb, t.getName(), CommonConfig.EOL);

                for (StackTraceElement ste : t.getStackTrace()) {
                    StringUtil.append(sb, ste.toString(), CommonConfig.EOL);
                }
            }
        }

        sb.append("Packet Tp stack traces printed.");
        sb.append(CommonConfig.EOL);
        return sb.toString();
    }
	
	public String getGeneralStats() {
        final StringBuilder sb = new StringBuilder(1000);
        ThreadFactory tf = generalThreadPool.getThreadFactory();

        if (tf instanceof PriorityThreadFactory) {
            PriorityThreadFactory ptf = (PriorityThreadFactory) tf;
            int count = ptf.getGroup().activeCount();
            Thread[] threads = new Thread[count + 2];
            ptf.getGroup().enumerate(threads);
            StringUtil.append(sb, "General Thread Pool:" + CommonConfig.EOL + "Tasks in the queue: ", String.valueOf(generalThreadPool.getQueue().size()), CommonConfig.EOL + "Showing threads stack trace:" + CommonConfig.EOL + "There should be ", String.valueOf(count), " Threads" + CommonConfig.EOL);

            for (Thread t : threads) {
                if (t == null) {
                    continue;
                }

                StringUtil.append(sb, t.getName(), CommonConfig.EOL);

                for (StackTraceElement ste : t.getStackTrace()) {
                    StringUtil.append(sb, ste.toString(), CommonConfig.EOL);
                }
            }
        }

        sb.append("Packet Tp stack traces printed.");
        sb.append(CommonConfig.EOL);
        return sb.toString();
    }
	
	protected class PurgeTask implements Runnable
	{
		@Override
		public void run() {
            effectsScheduledThreadPool.purge();
            generalScheduledThreadPool.purge();
			aiScheduledThreadPool.purge();
			botAiScheduledThreadPool.purge();
			eventScheduledThreadPool.purge();
        }
    }

    private static class SingletonHolder {
        protected static final ThreadPoolManager _instance = new ThreadPoolManager();
    }

    public ScheduledThreadPoolExecutor getEffectsScheduledThreadPool() {
        return effectsScheduledThreadPool;
    }

    public ScheduledThreadPoolExecutor getGeneralScheduledThreadPool() {
        return generalScheduledThreadPool;
    }

    public ScheduledThreadPoolExecutor getAiScheduledThreadPool() {
        return aiScheduledThreadPool;
    }

    public ScheduledThreadPoolExecutor getEventScheduledThreadPool() {
        return eventScheduledThreadPool;
    }

    public ThreadPoolExecutor getGeneralPacketsThreadPool() {
        return generalPacketsThreadPool;
    }

    public ThreadPoolExecutor getIoPacketsThreadPool() {
        return ioPacketsThreadPool;
    }

    public ThreadPoolExecutor getGeneralThreadPool() {
        return generalThreadPool;
    }

    public ThreadPoolExecutor getEventThreadPool() {
        return eventThreadPool;
    }
}