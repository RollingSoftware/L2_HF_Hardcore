package com.l2jserver.gameserver.model.events.impl.character.player;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.events.EventType;
import com.l2jserver.gameserver.model.events.impl.IBaseEvent;

public class OnPlayerExitCombat implements IBaseEvent {

    private final L2PcInstance player;

    public OnPlayerExitCombat(L2PcInstance player) {
        this.player = player;
    }

    public L2PcInstance getPlayer() {
        return player;
    }

    @Override
    public EventType getType() {
        return EventType.ON_PLAYER_EXIT_COMBAT;
    }
}
