package com.l2jserver.gameserver.model.events.impl.server;

import com.l2jserver.gameserver.model.events.EventType;
import com.l2jserver.gameserver.model.events.impl.IBaseEvent;

public class OnServerStarted implements IBaseEvent {
    @Override
    public EventType getType() {
        return EventType.ON_SERVER_STARTED;
    }
}
