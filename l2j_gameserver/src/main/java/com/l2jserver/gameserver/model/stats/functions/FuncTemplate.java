/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.stats.functions;

import com.l2jserver.gameserver.enums.StatFunction;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.model.stats.Stats;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;

/**
 * Function template.
 *
 * @author mkizub, Zoey76
 */
public final class FuncTemplate {
    private static final Logger LOG = LogManager.getLogger();

    private final Condition _attachCond;
    private final Condition _applayCond;
    private final Constructor<?> _constructor;
    private final Stats _stat;
    private final int _order;
    private final double _value;

    public FuncTemplate(Condition attachCond, Condition applayCond, String functionName, int order, Stats stat, double value) {
		final StatFunction function = StatFunction.valueOf(functionName.toUpperCase());
		if (order >= 0)
		{
			_order = order;
		}
		else
		{
			_order = function.getOrder();
		}
		
		_attachCond = attachCond;
		_applayCond = applayCond;
		_stat = stat;
		_value = value;
		
		try
		{
			final Class<?> functionClass = Class.forName("com.l2jserver.gameserver.model.stats.functions.Func" + function.getName());
			_constructor = functionClass.getConstructor(Stats.class, // Stats to update
				Integer.TYPE, // Order of execution
				Object.class, // Owner
				Double.TYPE, // Value for function
				Condition.class // Condition
			);
		}
		catch (ClassNotFoundException | NoSuchMethodException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Gets the function stat.
	 * @return the stat.
	 */
	public Stats getStat()
	{
		return _stat;
	}
	
	/**
	 * Gets the function priority order.
	 * @return the order
	 */
	public int getOrder()
	{
		return _order;
	}
	
	/**
	 * Gets the function value.
	 * @return the value
	 */
	public double getValue()
	{
		return _value;
	}

	/**
	 * Gets the functions for items.
	 *
	 * @param caster the caster
	 * @param target the target
	 * @param owner  the owner
	 * @return the function if conditions are met, {@code null} otherwise
	 */
	public AbstractFunction getFunc(L2Character caster, L2Character target, Object owner) {
		return getFunc(caster, target, null, owner);
	}

	/**
	 * Gets the functions for skills and items.
	 *
	 * @param caster the caster
	 * @param target the target
	 * @param skill  the skill
	 * @param owner  the owner
	 * @return the function if conditions are met, {@code null} otherwise
	 */
	public AbstractFunction getFunc(L2Character caster, L2Character target, Skill skill, Object owner) {
		if ((_attachCond != null) && !_attachCond.test(caster, target, skill)) {
			return null;
		}
		try {
			return (AbstractFunction) _constructor.newInstance(_stat, _order, owner, _value, _applayCond);
		} catch (Exception e) {
			LOG.warn(FuncTemplate.class.getSimpleName() + ": " + e.getMessage());
		}
		return null;
	}
}
