package com.l2jserver.gameserver.model.events.impl.character.player;

import com.l2jserver.gameserver.gameplay.damage.CombatHistoryMemento;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.events.EventType;
import com.l2jserver.gameserver.model.events.impl.IBaseEvent;

public class OnPlayerDeath implements IBaseEvent {

    private final L2PcInstance player;
    private final CombatHistoryMemento combatHistory;

    public OnPlayerDeath(L2PcInstance player, CombatHistoryMemento combatHistory) {
        this.player = player;
        this.combatHistory = combatHistory;
    }

    public L2PcInstance getPlayer() {
        return player;
    }

    public CombatHistoryMemento getCombatHistory() {
        return combatHistory;
    }

    @Override
    public EventType getType() {
        return EventType.ON_PLAYER_DEATH;
    }
}
