package com.l2jserver.gameserver.model.target;

import com.l2jserver.common.target.Target;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

public class TargetHolder {

    private L2Character master;
    private L2Character summon;

    public TargetHolder(L2Character master, L2Character summon) {
        this.master = master;
        this.summon = summon;
    }

    public static TargetHolder of(L2PcInstance player) {
        return new TargetHolder(player, null);
    }

    public static TargetHolder of(L2PcInstance player, L2Character summon) {
        return new TargetHolder(player, summon);
    }

    public L2Character getMaster() {
        return master;
    }

    public L2Character getSummon() {
        return summon;
    }

    public boolean isSummonTarget() {
        return summon != null;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", TargetHolder.class.getSimpleName() + "[", "]")
                .add(Objects.toString(master))
                .add(Objects.toString(summon))
                .toString();
    }

    public static TargetHolder parseTarget(L2PcInstance player, String targetArg) {
        if (targetArg == null) {
            return TargetHolder.of(player);
        }

        Optional<Target> targetOption = Target.parse(targetArg);
        if (targetOption.isEmpty()) {
            return TargetHolder.of(player);
        }

        Target targetValue = targetOption.get();
        if (targetValue.equals(Target.SUMMON)) {
            L2Character summon = player.getSummon();
            if (summon != null) {
                return TargetHolder.of(player, summon);
            }
        }

        return TargetHolder.of(player);
    }

}
