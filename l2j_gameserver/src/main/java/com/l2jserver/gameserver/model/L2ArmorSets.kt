package com.l2jserver.gameserver.model

import com.l2jserver.gameserver.datatables.ItemTable
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance
import com.l2jserver.gameserver.model.itemcontainer.Inventory
import com.l2jserver.gameserver.model.itemcontainer.Inventory.getPaperdollIndex
import com.l2jserver.gameserver.model.items.L2Item
import com.l2jserver.gameserver.model.items.graded.Grade
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance

sealed class L2ArmorSets(open val armorSetId: Int) {
    sealed class Fighter(override val armorSetId: Int) : L2ArmorSets(armorSetId) {
        object Wooden : Fighter(1)
    }

    sealed class Mage(override val armorSetId: Int) : L2ArmorSets(armorSetId) {
        object Devotion : Mage(2)
        object Karmian : Mage(14)
        object AvadonRobe : Mage(30)
        object DarkCrystalRobe : Mage(44)
        object MajorArcana : Mage(58)
        object MoraiTunic : Mage(184)
        object VesperTunic : Mage(145)
        object VorpalTunic : Mage(187)
    }

    override fun toString(): String {
        return "${this::class.simpleName} [$armorSetId]"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as L2ArmorSets

        if (armorSetId != other.armorSetId) return false
        return true
    }

    override fun hashCode(): Int {
        return armorSetId
    }
}

data class GradedArmorSet(val grade: Grade, val armorSet: L2ArmorSet)


fun L2ArmorSet.getSetPaperdollIndexWithoutShield(): List<Int> {
    val mutableList = mutableListOf<Int>()
    if (chestId > 0) {
        mutableList.add(Inventory.PAPERDOLL_CHEST)
    }
    if (legs.isNotEmpty()) {
        mutableList.add(Inventory.PAPERDOLL_LEGS)
    }
    if (gloves.isNotEmpty()) {
        mutableList.add(Inventory.PAPERDOLL_GLOVES)
    }
    if (feet.isNotEmpty()) {
        mutableList.add(Inventory.PAPERDOLL_FEET)
    }
    if (head.isNotEmpty()) {
        mutableList.add(Inventory.PAPERDOLL_HEAD)
    }
    return mutableList
}

fun L2ArmorSet.getSetBodyPartWithoutShield(): List<Int> {
    val mutableList = mutableListOf<Int>()
    if (chestId > 0) {
        mutableList.add(ItemTable.getInstance().getTemplate(chestId).bodyPart)
    }
    if (legs.isNotEmpty()) {
        mutableList.add(L2Item.SLOT_LEGS)
    }
    if (gloves.isNotEmpty()) {
        mutableList.add(L2Item.SLOT_GLOVES)
    }
    if (feet.isNotEmpty()) {
        mutableList.add(L2Item.SLOT_FEET)
    }
    if (head.isNotEmpty()) {
        mutableList.add(L2Item.SLOT_HEAD)
    }
    return mutableList
}

fun L2ArmorSet.findSetItemsInInventoryWithoutShield(player: L2PcInstance): List<L2ItemInstance> {
    val inventorySetItems = player.inventory.allItems
        .filter {
            it.id == chestId ||
                    it.id in legs ||
                    it.id in gloves ||
                    it.id in feet ||
                    it.id in head
        }.associateBy {
            getPaperdollIndex(it.item.bodyPart)
        }

    return if (inventorySetItems.keys.containsAll(getSetPaperdollIndexWithoutShield())) {
        inventorySetItems.values.toList()
    } else {
        emptyList()
    }
}