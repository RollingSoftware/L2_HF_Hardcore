package com.l2jserver.gameserver.model.base

fun ClassId.nextClassId(targetClassId: ClassId): ClassId {
    if (this.level() == ClassId.MAX_LEVEL) {
        return this
    }

    if (this == targetClassId) {
        return this
    }

    if (!targetClassId.childOf(this)) {
        throw IllegalArgumentException("Target Class Id $targetClassId is not a child of current class Id $this branch")
    }

    var classIdSearch = targetClassId
    while (classIdSearch.parent != this) {
        classIdSearch = classIdSearch.parent
    }
    return classIdSearch
}

fun ClassId.levelRequirement(): Int =
    when(this.level()) {
        0 -> 0
        1 -> 20
        2 -> 40
        3 -> 76
        else -> Int.MAX_VALUE
    }