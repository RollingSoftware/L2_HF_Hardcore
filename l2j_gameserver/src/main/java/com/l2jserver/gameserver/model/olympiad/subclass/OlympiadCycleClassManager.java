package com.l2jserver.gameserver.model.olympiad.subclass;

import com.google.common.collect.Lists;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.xml.impl.ClassListData;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.base.ClassLevel;
import com.l2jserver.gameserver.model.base.PlayerClass;
import com.l2jserver.gameserver.model.olympiad.Olympiad;
import com.l2jserver.gameserver.util.Util;

import java.util.Optional;
import java.util.stream.Collectors;

public class OlympiadCycleClassManager {

    private final OlympiadCycleClassRepository olympiadCycleClassRepository = new OlympiadCycleClassRepository();

    public boolean managerSubclassRegistration(L2PcInstance player) {
        var cycleClassId = olympiadCycleClassRepository.findCycleClassId(player.getObjectId(), Olympiad.getInstance().getCurrentCycle());
        return cycleClassId
                .map(player::checkCurrentClassIs)
                .orElseGet(() -> showSelectCycleClassIdHtml(player));
    }

    private boolean showSelectCycleClassIdHtml(L2PcInstance player) {
        var rowTemplate = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), "data/html/olympiad/cycleclass/cycle_class_row.html");

        var baseClass = ClassListData.getInstance().getClass(player.getBaseClass());
        var baseClassIndex = "0";
        var baseClassRow = rowTemplate
                .replace("{{CLASS_INDEX}}", baseClassIndex)
                .replace("{{CLASS_NAME}}", baseClass.getClassName());

        var cycleClassItems = Lists.newArrayList(baseClassRow);

        var subclassRows = player.getSubClasses()
                .values()
                .stream()
                .filter(it -> PlayerClass.values()[it.getClassId()].getLevel() == ClassLevel.Fourth)
                .map(it -> rowTemplate
                        .replace("{{CLASS_INDEX}}", String.valueOf(it.getClassIndex()))
                        .replace("{{CLASS_NAME}}", ClassListData.getInstance().getClass(it.getClassId()).getClassName())
                ).collect(Collectors.toList());

        cycleClassItems.addAll(subclassRows);

        var seasonClassList = HtmCache.getInstance().getHtm(player.getHtmlPrefix(), "data/html/olympiad/cycleclass/choose_cycle_class.html");
        Util.sendHtml(player, seasonClassList.replace("{{CYCLE_SUBCLASSES}}", String.join("\n", cycleClassItems)));
        return false;
    }

    public boolean isCycleClassActive(L2PcInstance player) {
        var cycleClassId = olympiadCycleClassRepository.findCycleClassId(
                player.getObjectId(),
                Olympiad.getInstance().getCurrentCycle()
        );
        return cycleClassId.map(player::checkCurrentClassIs).orElse(false);
    }

    public static OlympiadCycleClassManager with() {
        return SingletonHolder.INSTANCE;
    }

    public boolean registerCycleClass(L2PcInstance player, int currentCycle, int classId) {
        var existingClass = olympiadCycleClassRepository.findCycleClassId(player.getObjectId(), currentCycle);
        if (existingClass.isPresent()) {
            player.sendScreenMessage(player.string("olympiad_cycle_class_already_registered"), 4000);
            return false;
        } else {
            olympiadCycleClassRepository.storeCycleClassId(player.getObjectId(), currentCycle, classId);
            return true;
        }
    }

    public Optional<Integer> getCycleClassId(L2PcInstance player) {
        return olympiadCycleClassRepository.findCycleClassId(player.getObjectId(), Olympiad.getInstance().getCurrentCycle());
    }

    private static class SingletonHolder {
        protected static final OlympiadCycleClassManager INSTANCE = new OlympiadCycleClassManager();
    }
}
