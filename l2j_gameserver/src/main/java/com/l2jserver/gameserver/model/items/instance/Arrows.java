package com.l2jserver.gameserver.model.items.instance;

import com.l2jserver.gameserver.model.items.type.CrystalType;

import java.util.List;
import java.util.Map;

public class Arrows {

    public static final Map<CrystalType, Integer> BOLTS_BY_GRADE = Map.of(
            CrystalType.NONE, 9632, // Wooden Bolt
            CrystalType.D, 9633, // Bone Bolt
            CrystalType.C, 9634, // Steel Bolt
            CrystalType.B, 9635, // Silver Bolt
            CrystalType.A, 9636, // Mithril Bolt
            CrystalType.S, 9637); // Shining Bolt

    public static final Map<CrystalType, Integer> ARROWS_BY_GRADE = Map.of(
            CrystalType.NONE, 17, // Wooden Arrow
            CrystalType.D, 1341, // Bone Arrow
            CrystalType.C, 1342, // Steel Arrow
            CrystalType.B, 1343, // Silver Arrow
            CrystalType.A, 1344, // Mithril Arrow
            CrystalType.S, 1345); // Shining Arrow

    public static final List<Integer> ALL_PROJECTILE_IDS = List.of(
            9632, // Wooden Bolt
            9633, // Bone Bolt
            9634, // Steel Bolt
            9635, // Silver Bolt
            9636, // Mithril Bolt
            9637, // Shining Bolt
            17,   // Wooden Arrow
            1341, // Bone Arrow
            1342, // Steel Arrow
            1343, // Silver Arrow
            1344, // Mithril Arrow
            1345); // Shining Arrow

}
