package com.l2jserver.gameserver.model.interfaces;

public interface Reloadable {
    boolean reload();
}
