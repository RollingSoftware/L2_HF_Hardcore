/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.stats.functions

import com.l2jserver.gameserver.data.xml.impl.EnchantItemHPBonusData
import com.l2jserver.gameserver.model.actor.L2Character
import com.l2jserver.gameserver.model.conditions.Condition
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance
import com.l2jserver.gameserver.model.skills.Skill
import com.l2jserver.gameserver.model.stats.Stats


class FuncEnchantHp(stat: Stats?, order: Int, owner: Any?, value: Double, applyCond: Condition?) :

    AbstractFunction(stat, order, owner, value, applyCond) {
    override fun calc(effector: L2Character?, effected: L2Character?, skill: Skill?, initVal: Double): Double {
        if (applyCond != null && !applyCond.test(effector, effected, skill)) {
            return initVal
        }

        if (funcOwner is L2ItemInstance) {
            val item = funcOwner as L2ItemInstance
            if (item.enchantLevel > 0) {
                return initVal + EnchantItemHPBonusData.getInstance().getHPBonus(item)
            }
        }
        return initVal
    }
}