package com.l2jserver.gameserver.model.olympiad;

import com.google.common.collect.Lists;

import java.util.List;

public class OlympiadPeriod {

    public static final OlympiadPeriod COMPETITION = new OlympiadPeriod(0);
    public static final OlympiadPeriod VALIDATION = new OlympiadPeriod(1);

    private static final List<OlympiadPeriod> VALUES = Lists.newArrayList(
            COMPETITION,
            VALIDATION
    );

    private final int id;

    public OlympiadPeriod(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "OlympiadPeriod=" + id;
    }

    public static OlympiadPeriod byId(int id) {
        return VALUES
                .stream()
                .filter(it -> it.id == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Incorrect period ID: " + id));
    }
}
