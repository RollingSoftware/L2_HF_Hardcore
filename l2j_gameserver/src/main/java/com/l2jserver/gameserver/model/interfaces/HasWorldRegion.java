package com.l2jserver.gameserver.model.interfaces;

import com.l2jserver.gameserver.model.L2WorldRegion;

public interface HasWorldRegion {
    L2WorldRegion getWorldRegion();
}
