package com.l2jserver.gameserver.model.interfaces;

public interface Unloadable {
    boolean unload();
}
