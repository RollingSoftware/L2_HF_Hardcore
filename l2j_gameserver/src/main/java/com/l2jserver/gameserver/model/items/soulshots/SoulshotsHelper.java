package com.l2jserver.gameserver.model.items.soulshots;

import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.items.type.CrystalType;
import com.l2jserver.gameserver.model.skills.SkillBrief;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jserver.gameserver.util.Broadcast;

import java.util.HashMap;
import java.util.Map;

public class SoulshotsHelper {

    public static Map<CrystalType, SkillBrief> FIGHTER_SHOTS_SKILLS = fighterShotsSkills();

    private static Map<CrystalType, SkillBrief> fighterShotsSkills() {
        var map = new HashMap<CrystalType, SkillBrief>();
        map.put(CrystalType.NONE, new SkillBrief(2039));
        map.put(CrystalType.D, new SkillBrief(2150));
        map.put(CrystalType.C, new SkillBrief(2151));
        map.put(CrystalType.B, new SkillBrief(2152));
        map.put(CrystalType.A, new SkillBrief(2153));
        map.put(CrystalType.S, new SkillBrief(2154));
        return map;
    }

    public static Map<CrystalType, SkillBrief> MAGE_SHOTS_SKILLS = mageShotSkills();

    private static Map<CrystalType, SkillBrief> mageShotSkills() {
        var map = new HashMap<CrystalType, SkillBrief>();
        map.put(CrystalType.NONE, new SkillBrief(2061));
        map.put(CrystalType.D, new SkillBrief(2160));
        map.put(CrystalType.C, new SkillBrief(2161));
        map.put(CrystalType.B, new SkillBrief(2162));
        map.put(CrystalType.A, new SkillBrief(2163));
        map.put(CrystalType.S, new SkillBrief(2163));
        return map;
    }

    public static Map<CrystalType, SkillBrief> FISHING_SHOTS_SKILLS = fishingShotSkills();

    private static Map<CrystalType, SkillBrief> fishingShotSkills() {
        var map = new HashMap<CrystalType, SkillBrief>();
        map.put(CrystalType.NONE, new SkillBrief(2181));
        map.put(CrystalType.D, new SkillBrief(2182));
        map.put(CrystalType.C, new SkillBrief(2183));
        map.put(CrystalType.B, new SkillBrief(2184));
        map.put(CrystalType.A, new SkillBrief(2185));
        map.put(CrystalType.S, new SkillBrief(2186));
        return map;
    }

    public static final SkillBrief PET_FIGHTER_SHOT_SKILL = new SkillBrief(2033);
    public static final SkillBrief PET_MAGE_SHOT_SKILL = new SkillBrief(2009);


    public static void fakeSoulshot(L2Character character) {
        var activeWeapon = character.getActiveWeaponItem();
        if (activeWeapon == null) {
            return;
        }
        var activeWeaponCrystalType = activeWeapon.getItemGradeSPlus();
        var shots = SoulshotsHelper.FIGHTER_SHOTS_SKILLS.get(activeWeaponCrystalType);

        Broadcast.toSelfAndKnownPlayersInRadius(
                character,
                new MagicSkillUse(character, character, shots.getId(), shots.getLevel(), 0, 0),
                600
        );
    }

    public static void fakeSpiritshot(L2Character character) {
        var activeWeapon = character.getActiveWeaponItem();
        if (activeWeapon == null) {
            return;
        }
        var activeWeaponCrystalType = activeWeapon.getItemGradeSPlus();
        var shots = SoulshotsHelper.MAGE_SHOTS_SKILLS.get(activeWeaponCrystalType);

        Broadcast.toSelfAndKnownPlayersInRadius(
                character,
                new MagicSkillUse(character, character, shots.getId(), shots.getLevel(), 0, 0),
                600
        );
    }

    public static void fakeFishingshot(L2Character character) {
        var activeWeapon = character.getActiveWeaponItem();
        if (activeWeapon == null) {
            return;
        }
        var activeWeaponCrystalType = activeWeapon.getItemGradeSPlus();
        var shots = SoulshotsHelper.FISHING_SHOTS_SKILLS.get(activeWeaponCrystalType);

        Broadcast.toSelfAndKnownPlayersInRadius(
                character,
                new MagicSkillUse(character, character, shots.getId(), shots.getLevel(), 0, 0),
                600
        );
    }

    public static void fakePetSoulshot(L2Character character) {
        Broadcast.toSelfAndKnownPlayersInRadius(
                character,
                new MagicSkillUse(
                        character,
                        character,
                        SoulshotsHelper.PET_FIGHTER_SHOT_SKILL.getId(),
                        SoulshotsHelper.PET_FIGHTER_SHOT_SKILL.getLevel(),
                        0,
                        0
                ),
                600
        );
    }

    public static void fakePetSpiritshot(L2Character character) {
        Broadcast.toSelfAndKnownPlayersInRadius(
                character,
                new MagicSkillUse(
                        character,
                        character,
                        SoulshotsHelper.PET_MAGE_SHOT_SKILL.getId(),
                        SoulshotsHelper.PET_MAGE_SHOT_SKILL.getLevel(),
                        0,
                        0
                ),
                600
        );
    }

}
