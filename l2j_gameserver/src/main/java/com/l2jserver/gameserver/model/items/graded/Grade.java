package com.l2jserver.gameserver.model.items.graded;

import com.l2jserver.gameserver.model.items.type.CrystalType;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public enum Grade {

    UNSET, NG, D, C, B, A, S, S80, S84;

    public static Grade fromString(String text) {
        if (text == null || text.isEmpty()) {
            return UNSET;
        }

        return valueOf(text.toUpperCase());
    }

    public static Grade fromCrystalType(CrystalType crystalType) {
        if (crystalType.equals(CrystalType.NONE)) {
            return NG;
        }

        try {
            return valueOf(crystalType.name());
        } catch (RuntimeException e) {
            return UNSET;
        }
    }

    private static final NavigableMap<Integer, Grade> gradeByRange = new TreeMap<>();

    static {
        gradeByRange.put(1, NG);
        gradeByRange.put(20, D);
        gradeByRange.put(40, C);
        gradeByRange.put(52, B);
        gradeByRange.put(61, A);
        gradeByRange.put(76, S);
        gradeByRange.put(80, S80);
        gradeByRange.put(84, S84);
    }

    public static Set<Grade> gradesByLevel(int level) {
        return gradeByRange
                .entrySet()
                .stream()
                .filter(it -> level >= it.getKey())
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }

    public static Grade resolveGrade(int level) {
        var value = gradeByRange.floorEntry(level);
        if (value != null) {
            return value.getValue();
        }
        return Grade.UNSET;
    }
}
