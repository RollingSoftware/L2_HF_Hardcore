package com.l2jserver.gameserver.model.olympiad.subclass;

import com.l2jserver.common.pool.impl.ConnectionFactory;
import org.jdbi.v3.core.Jdbi;

import java.util.Optional;

public class OlympiadCycleClassRepository {

    private static final String SELECT = """
            SELECT class_id FROM olympiad_cycle_class 
            WHERE character_id=:characterId AND olympiad_cycle=:olympiadCycle""";

    private static final String INSERT = """
            INSERT INTO olympiad_cycle_class (class_id, character_id, olympiad_cycle) 
            VALUES (:classId, :characterId, :olympiadCycle)""";

    private final Jdbi jdbi;

    public OlympiadCycleClassRepository() {
        this.jdbi = Jdbi.create(ConnectionFactory.getInstance().getDataSource());
    }

    public Optional<Integer> findCycleClassId(int playerObjectId, int olympiadCycle) {
        return jdbi.withHandle(h -> h.createQuery(SELECT)
                .bind("characterId", playerObjectId)
                .bind("olympiadCycle", olympiadCycle)
                .mapTo(Integer.class)
                .findOne());
    }

    public void storeCycleClassId(int playerObjectId, int olympiadCycle, int classId) {
        jdbi.withHandle(h -> h.createUpdate(INSERT)
                .bind("characterId", playerObjectId)
                .bind("olympiadCycle", olympiadCycle)
                .bind("classId", classId)
                .execute());
    }

}


