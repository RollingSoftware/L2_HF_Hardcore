package com.l2jserver.gameserver.model.events.impl.server

import com.l2jserver.gameserver.model.events.EventType
import com.l2jserver.gameserver.model.events.impl.IBaseEvent

class OnServerShutdown : IBaseEvent {
    override fun getType(): EventType {
        return EventType.ON_SERVER_STARTED
    }
}
