package com.l2jserver.gameserver.model.skills;

public class SkillBrief {
    private final int id;
    private final int level;

    public SkillBrief(int id) {
        this.id = id;
        this.level = 1;
    }

    public SkillBrief(int id, int level) {
        this.id = id;
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public int getLevel() {
        return level;
    }
}
