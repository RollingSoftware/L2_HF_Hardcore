package com.l2jserver.gameserver.datatables.categorized;

import com.l2jserver.common.config.CommonConfig;
import com.l2jserver.common.mappers.Json;
import com.l2jserver.common.util.CollectionUtil;
import com.l2jserver.gameserver.model.items.scrolls.CategorizedScrolls;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ScrollDropDataTable {


    private static final Logger LOG = LogManager.getLogger();

    private Set<Integer> scrollIds;
    private CategorizedScrolls categorizedScrolls;

    public ScrollDropDataTable() {
        load();
    }

    public void load() {
        try {
            File itemPartsFile = new File(CommonConfig.DATAPACK_ROOT, "./data/stats/categorized/scrolls.json");
            categorizedScrolls = Json.INSTANCE.getJson().readValue(itemPartsFile, CategorizedScrolls.class);

            scrollIds = new HashSet<>();
            scrollIds.addAll(CollectionUtil.extractIds(categorizedScrolls.getNormalWeaponScrolls()));
            scrollIds.addAll(CollectionUtil.extractIds(categorizedScrolls.getBlessedWeaponScrolls()));
            scrollIds.addAll(CollectionUtil.extractIds(categorizedScrolls.getNormalArmorScrolls()));
            scrollIds.addAll(CollectionUtil.extractIds(categorizedScrolls.getBlessedArmorScrolls()));
            scrollIds.addAll(CollectionUtil.extractIds(categorizedScrolls.getNormalMiscScrolls()));
            scrollIds.addAll(CollectionUtil.extractIds(categorizedScrolls.getBlessedMiscScrolls()));

            LOG.info("Loaded {} scrolls", scrollIds.size());
        } catch (IOException e) {
            throw new IllegalStateException("Could not read scroll drop data: " + e.getMessage());
        }
    }

    public CategorizedScrolls getCategorizedScrolls() {
        return categorizedScrolls;
    }

    public Set<Integer> getScrollIds() {
        return scrollIds;
    }

    public static ScrollDropDataTable getInstance() {
        return ScrollDropDataTable.SingletonHolder._instance;
    }

    private static class SingletonHolder {
        protected static final ScrollDropDataTable _instance = new ScrollDropDataTable();
    }

}
