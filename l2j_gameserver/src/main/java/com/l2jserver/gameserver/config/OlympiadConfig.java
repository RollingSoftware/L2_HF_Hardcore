package com.l2jserver.gameserver.config;

import com.l2jserver.common.config.PropertiesParser;
import com.l2jserver.common.resources.ResourceResolver;
import com.l2jserver.util.ConfigHelper;
import java.util.ArrayList;
import java.util.List;

public class OlympiadConfig {

    public static final String OLYMPIAD_CONFIG_FILE = ResourceResolver.resolveConfig("./gameplay/Olympiad.properties");

    public static int ALT_OLY_START_TIME;
    public static int ALT_OLY_MIN;
    public static int ALT_OLY_MAX_BUFFS;
    public static boolean OLYMPIAD_WIN_REWARDS_FAME;
    public static int OLYMPIAD_WIN_FAME_AMOUNT;
    public static long ALT_OLY_CPERIOD;
    public static long ALT_OLY_BATTLE;
    public static long ALT_OLY_WPERIOD;
    public static long ALT_OLY_VPERIOD;
    public static int ALT_OLY_START_POINTS;
    public static int ALT_OLY_WEEKLY_POINTS;
    public static int ALT_OLY_CLASSED;
    public static int ALT_OLY_NONCLASSED;
    public static int ALT_OLY_TEAMS;
    public static int ALT_OLY_REG_DISPLAY;
    public static int[][] ALT_OLY_CLASSED_REWARD;
    public static int[][] ALT_OLY_NONCLASSED_REWARD;
    public static int[][] ALT_OLY_TEAM_REWARD;
    public static int OLYPMIAD_CYCLE_IN_WEEKS;
    public static int ALT_OLY_COMP_RITEM;
    public static int ALT_OLY_MIN_MATCHES;
    public static int ALT_OLY_GP_PER_POINT;
    public static int ALT_OLY_HERO_POINTS;
    public static int ALT_OLY_RANK1_POINTS;
    public static int ALT_OLY_RANK2_POINTS;
    public static int ALT_OLY_RANK3_POINTS;
    public static int ALT_OLY_RANK4_POINTS;
    public static int ALT_OLY_RANK5_POINTS;
    public static int ALT_OLY_MAX_POINTS;
    public static int ALT_OLY_DIVIDER_CLASSED;
    public static int ALT_OLY_DIVIDER_NON_CLASSED;
    public static int ALT_OLY_MAX_WEEKLY_MATCHES;
    public static int ALT_OLY_MAX_WEEKLY_MATCHES_NON_CLASSED;
    public static int ALT_OLY_MAX_WEEKLY_MATCHES_CLASSED;
    public static int ALT_OLY_MAX_WEEKLY_MATCHES_TEAM;
    public static boolean ALT_OLY_LOG_FIGHTS;
    public static boolean ALT_OLY_SHOW_MONTHLY_WINNERS;
    public static boolean ALT_OLY_ANNOUNCE_GAMES;
    public static List<Integer> LIST_OLY_RESTRICTED_ITEMS;
    public static int ALT_OLY_ENCHANT_LIMIT;
    public static int ALT_OLY_WAIT_TIME;

    public static boolean SUBCLASS_REGISTRATION;

    public static void load() {
        final PropertiesParser Olympiad = new PropertiesParser(OLYMPIAD_CONFIG_FILE);

        ALT_OLY_START_TIME = Olympiad.getInt("AltOlyStartTime", 18);
        ALT_OLY_MIN = Olympiad.getInt("AltOlyMin", 0);

        OLYMPIAD_WIN_REWARDS_FAME = Olympiad.getBoolean("AltOlyWinFameReward", true);
        OLYMPIAD_WIN_FAME_AMOUNT = Olympiad.getInt("AltOlyWinFameAmount", 100);

        ALT_OLY_MAX_BUFFS = Olympiad.getInt("AltOlyMaxBuffs", 5);
        ALT_OLY_CPERIOD = Olympiad.getLong("AltOlyCPeriod", 21600000);
        ALT_OLY_BATTLE = Olympiad.getLong("AltOlyBattle", 300000);
        ALT_OLY_WPERIOD = Olympiad.getLong("AltOlyWPeriod", 604800000);
        ALT_OLY_VPERIOD = Olympiad.getLong("AltOlyVPeriod", 86400000);
        ALT_OLY_START_POINTS = Olympiad.getInt("AltOlyStartPoints", 10);
        ALT_OLY_WEEKLY_POINTS = Olympiad.getInt("AltOlyWeeklyPoints", 10);
        ALT_OLY_CLASSED = Olympiad.getInt("AltOlyClassedParticipants", 11);
        ALT_OLY_NONCLASSED = Olympiad.getInt("AltOlyNonClassedParticipants", 11);
        ALT_OLY_TEAMS = Olympiad.getInt("AltOlyTeamsParticipants", 6);
        ALT_OLY_REG_DISPLAY = Olympiad.getInt("AltOlyRegistrationDisplayNumber", 100);
        ALT_OLY_CLASSED_REWARD = ConfigHelper.parseItemsList(Olympiad.getString("AltOlyClassedReward", "13722,50"));
        ALT_OLY_NONCLASSED_REWARD = ConfigHelper.parseItemsList(Olympiad.getString("AltOlyNonClassedReward", "13722,40"));
        ALT_OLY_TEAM_REWARD = ConfigHelper.parseItemsList(Olympiad.getString("AltOlyTeamReward", "13722,85"));
        ALT_OLY_COMP_RITEM = Olympiad.getInt("AltOlyCompRewItem", 13722);
        OLYPMIAD_CYCLE_IN_WEEKS = Olympiad.getInt("OlympiadCycleInWeeks", 2);
        ALT_OLY_MIN_MATCHES = Olympiad.getInt("AltOlyMinMatchesForPoints", 15);
        ALT_OLY_GP_PER_POINT = Olympiad.getInt("AltOlyGPPerPoint", 1000);
        ALT_OLY_HERO_POINTS = Olympiad.getInt("AltOlyHeroPoints", 200);
        ALT_OLY_RANK1_POINTS = Olympiad.getInt("AltOlyRank1Points", 100);
        ALT_OLY_RANK2_POINTS = Olympiad.getInt("AltOlyRank2Points", 75);
        ALT_OLY_RANK3_POINTS = Olympiad.getInt("AltOlyRank3Points", 55);
        ALT_OLY_RANK4_POINTS = Olympiad.getInt("AltOlyRank4Points", 40);
        ALT_OLY_RANK5_POINTS = Olympiad.getInt("AltOlyRank5Points", 30);
        ALT_OLY_MAX_POINTS = Olympiad.getInt("AltOlyMaxPoints", 10);
        ALT_OLY_DIVIDER_CLASSED = Olympiad.getInt("AltOlyDividerClassed", 5);
        ALT_OLY_DIVIDER_NON_CLASSED = Olympiad.getInt("AltOlyDividerNonClassed", 5);
        ALT_OLY_MAX_WEEKLY_MATCHES = Olympiad.getInt("AltOlyMaxWeeklyMatches", 70);
        ALT_OLY_MAX_WEEKLY_MATCHES_NON_CLASSED = Olympiad.getInt("AltOlyMaxWeeklyMatchesNonClassed", 60);
        ALT_OLY_MAX_WEEKLY_MATCHES_CLASSED = Olympiad.getInt("AltOlyMaxWeeklyMatchesClassed", 30);
        ALT_OLY_MAX_WEEKLY_MATCHES_TEAM = Olympiad.getInt("AltOlyMaxWeeklyMatchesTeam", 10);
        ALT_OLY_LOG_FIGHTS = Olympiad.getBoolean("AltOlyLogFights", false);
        ALT_OLY_SHOW_MONTHLY_WINNERS = Olympiad.getBoolean("AltOlyShowMonthlyWinners", true);
        ALT_OLY_ANNOUNCE_GAMES = Olympiad.getBoolean("AltOlyAnnounceGames", true);
        String[] olyRestrictedItems = Olympiad.getString("AltOlyRestrictedItems", "6611,6612,6613,6614,6615,6616,6617,6618,6619,6620,6621,9388,9389,9390,17049,17050,17051,17052,17053,17054,17055,17056,17057,17058,17059,17060,17061,20759,20775,20776,20777,20778,14774").split(",");
        LIST_OLY_RESTRICTED_ITEMS = new ArrayList<>(olyRestrictedItems.length);
        for (String id : olyRestrictedItems) {
            LIST_OLY_RESTRICTED_ITEMS.add(Integer.parseInt(id));
        }
        ALT_OLY_ENCHANT_LIMIT = Olympiad.getInt("AltOlyEnchantLimit", -1);
        ALT_OLY_WAIT_TIME = Olympiad.getInt("AltOlyWaitTime", 120);
        SUBCLASS_REGISTRATION = Olympiad.getBoolean("SubclassRegistration", true);
    }

}
