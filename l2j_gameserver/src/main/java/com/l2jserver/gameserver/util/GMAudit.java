/*
 * Copyright (C) 2004-2016 L2J Server
 *
 * This file is part of L2J Server.
 *
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.util;

import com.l2jserver.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Audits Game Master's actions.
 */
public class GMAudit {
    private static final Logger LOG = LogManager.getLogger("GmAudit");

    /**
     * Logs a Game Master's action into a file.
     *
     * @param gmName the Game Master's name
     * @param action the performed action
     * @param target the target's name
     * @param params the parameters
     */
    public static void auditGMAction(String gmName, String action, String target, String params) {
        if (Config.GMAUDIT) {
            LOG.info("{} > {} > {} > {}", gmName, action, target, params);
        }
    }

    /**
     * Wrapper method.
     *
     * @param gmName the Game Master's name
     * @param action the performed action
     * @param target the target's name
     */
    public static void auditGMAction(String gmName, String action, String target) {
        if (Config.GMAUDIT) {
            LOG.info("{} > {} > {}", gmName, action, target);
        }
    }
}