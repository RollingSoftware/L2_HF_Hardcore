package com.l2jserver.gameserver.gameplay.damage;

import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.skills.Skill;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CombatHistory {

    private Queue<DamageRecord> damageRecords = new ConcurrentLinkedQueue<>();

    public void recordDamage(L2Character attacker, double amount, Skill skill) {
        damageRecords.add(DamageRecord.of(attacker, amount, skill));
    }

    public void reset() {
        damageRecords = new ConcurrentLinkedQueue<>();
    }

    public CombatHistoryMemento asMemento() {
        return new CombatHistoryMemento(new LinkedList<>(damageRecords));
    }

}
