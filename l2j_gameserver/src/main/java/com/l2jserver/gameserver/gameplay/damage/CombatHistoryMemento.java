package com.l2jserver.gameserver.gameplay.damage;

import java.util.Queue;

public class CombatHistoryMemento {

    private final Queue<DamageRecord> damageRecords;

    public CombatHistoryMemento(Queue<DamageRecord> damageRecords) {
        this.damageRecords = damageRecords;
    }

    public Queue<DamageRecord> getDamageRecords() {
        return damageRecords;
    }

    @Override
    public String toString() {
        return "CombatHistoryMemento{" +
                "damageRecords=" + damageRecords +
                '}';
    }
}
