package com.l2jserver.gameserver.gameplay.damage;

import java.util.Objects;

public class SkillHistory {
    private String name;
    private String icon;
    private int level;

    public SkillHistory(String name, String icon, int level) {
        this.name = name;
        this.icon = icon;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "SkillHistory{" +
                "name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", level=" + level +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SkillHistory that = (SkillHistory) o;

        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
