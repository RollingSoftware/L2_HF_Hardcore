package com.l2jserver.gameserver.gameplay.damage;

import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.skills.Skill;

public class DamageRecord {

    private final L2Character attacker;
    private final double amount;
    private final SkillHistory skill;

    private DamageRecord(L2Character attacker, double amount, SkillHistory skill) {
        this.attacker = attacker;
        this.amount = amount;
        this.skill = skill;
    }

    public L2Character getAttacker() {
        return attacker;
    }

    public double getAmount() {
        return amount;
    }

    public SkillHistory getSkill() {
        return skill;
    }

    public static DamageRecord of(L2Character attacker, double amount, Skill skill) {
        SkillHistory skillHistory;
        if (skill == null) {
            skillHistory = new SkillHistory("Attack", "icon.action003", 1);
        } else {
            skillHistory = new SkillHistory(skill.getName(), skill.getIcon(), skill.getLevel());
        }
        return new DamageRecord(attacker, amount, skillHistory);
    }

    @Override
    public String toString() {
        return "DamageRecord{" +
                "attacker=" + attacker +
                ", amount=" + amount +
                ", skill=" + skill +
                '}';
    }
}
