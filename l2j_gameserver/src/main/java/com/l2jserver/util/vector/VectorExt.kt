package com.l2jserver.util.vector

import com.l2jserver.common.primitives.Vector2
import com.l2jserver.common.primitives.Vector3
import com.l2jserver.gameserver.model.Location

fun Location.toVec2d() =
    Vector2(this.x.toDouble(), this.y.toDouble())

fun Location.toVec3d() =
    Vector3(this.x.toDouble(), this.y.toDouble(), this.z.toDouble())