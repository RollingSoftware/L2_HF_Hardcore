package com.l2jserver.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConfigHelper {
    private static final Logger LOG = LogManager.getLogger();

    /**
     * Parse a config value from its string representation to a two-dimensional int array.<br>
     * The format of the value to be parsed should be as follows: "item1Id,item1Amount;item2Id,item2Amount;...itemNId,itemNAmount".
     *
     * @param line the value of the parameter to parse
     * @return the parsed list or {@code null} if nothing was parsed
     */
    public static int[][] parseItemsList(String line) {
        final String[] propertySplit = line.split(";");
        if (propertySplit.length == 0) {
            // nothing to do here
            return null;
        }

        int i = 0;
        String[] valueSplit;
        final int[][] result = new int[propertySplit.length][];
        int[] tmp;
        for (String value : propertySplit) {
            valueSplit = value.split(",");
            if (valueSplit.length != 2) {
                LOG.warn("parseItemsList[Config.load()]: invalid entry -> {}, should be itemId,itemNumber. Skipping to the next entry in the list.", valueSplit[0]);
                continue;
            }

            tmp = new int[2];
            try {
                tmp[0] = Integer.parseInt(valueSplit[0]);
            } catch (NumberFormatException e) {
                LOG.warn("parseItemsList[Config.load()]: invalid itemId -> {}, value must be an integer. Skipping to the next entry in the list.", valueSplit[0]);
                continue;
            }
            try {
                tmp[1] = Integer.parseInt(valueSplit[1]);
            } catch (NumberFormatException e) {
                LOG.warn("parseItemsList[Config.load()]: invalid item number -> {}, value must be an integer. Skipping to the next entry in the list.", valueSplit[1]);
                continue;
            }
            result[i++] = tmp;
        }
        return result;
    }
}
