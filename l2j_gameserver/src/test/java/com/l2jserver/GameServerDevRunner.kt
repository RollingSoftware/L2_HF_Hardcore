package com.l2jserver

import com.l2jserver.common.config.CommonConfig
import com.l2jserver.gameserver.GameServer
import com.l2jserver.gameserver.scripting.ScriptEngineManager
import java.io.File
import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
    val gradleExecutable = if (getOS() == OS.WINDOWS) "gradlew.bat" else "./gradlew"
    "$gradleExecutable :l2j_datapack:assemble".runCommand(File("./"))
    CommonConfig.DEFAULT_DATAPACK_ROOT = File("./l2j_datapack/dist/game").canonicalPath
    CommonConfig.DEFAULT_CONFIG_ROOT = File("./l2j_gameserver/dist/game/config").canonicalPath
    ScriptEngineManager.SCRIPT_ROOTS = listOf(
        File("./l2j_datapack/build/classes/java/main"),
        File("./l2j_datapack/build/classes/kotlin/main")
    )
    ScriptEngineManager.SCRIPT_RESOURCE_ROOT = File("./l2j_datapack/dist/game/data/scripts")
    Config.HTM_CACHE_ENABLED = false
    GameServer.main(args)
}

enum class OS {
    WINDOWS, OTHER
}

fun getOS(): OS {
    val os = System.getProperty("os.name").lowercase()
    return when {
        os.contains("win") -> {
            OS.WINDOWS
        }
        else -> {
            OS.OTHER
        }
    }
}

fun String.runCommand(workingDir: File) {
    ProcessBuilder(*split(" ").toTypedArray())
        .directory(workingDir)
        .redirectOutput(ProcessBuilder.Redirect.INHERIT)
        .redirectError(ProcessBuilder.Redirect.INHERIT)
        .start()
        .waitFor(60, TimeUnit.MINUTES)
}