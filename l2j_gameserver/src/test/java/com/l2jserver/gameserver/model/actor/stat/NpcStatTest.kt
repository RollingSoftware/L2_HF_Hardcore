package com.l2jserver.gameserver.model.actor.stat;

import com.l2jserver.Config
import com.l2jserver.common.config.CommonConfig
import com.l2jserver.gameserver.GameTimeController
import com.l2jserver.gameserver.data.sql.impl.AnnouncementsTable
import com.l2jserver.gameserver.data.sql.impl.SummonSkillsTable
import com.l2jserver.gameserver.data.xml.impl.CategoryData
import com.l2jserver.gameserver.data.xml.impl.EnchantSkillGroupsData
import com.l2jserver.gameserver.data.xml.impl.NpcData
import com.l2jserver.gameserver.data.xml.impl.SecondaryAuthData
import com.l2jserver.gameserver.data.xml.impl.SkillTreesData
import com.l2jserver.gameserver.datatables.SkillData
import com.l2jserver.gameserver.handler.EffectHandler
import com.l2jserver.gameserver.instancemanager.GlobalVariablesManager
import com.l2jserver.gameserver.instancemanager.InstanceManager
import com.l2jserver.gameserver.instancemanager.MapRegionManager
import com.l2jserver.gameserver.model.L2World
import com.l2jserver.gameserver.model.actor.instance.L2MonsterInstance
import com.l2jserver.gameserver.scripting.ScriptEngineManager
import org.assertj.core.api.Assertions
import java.io.File

class NpcStatTest {

    private val depriveNpcId = 20664

    fun getPDef() {
        CommonConfig.DEFAULT_DATAPACK_ROOT = File("./../l2j_datapack/dist/game").canonicalPath
        CommonConfig.DEFAULT_CONFIG_ROOT = File("./dist/game/config").canonicalPath
        ScriptEngineManager.SCRIPT_ROOTS = listOf(
            File("./../l2j_datapack/build/classes/java/main"),
            File("./../l2j_datapack/build/classes/kotlin/main")
        )
        ScriptEngineManager.SCRIPT_RESOURCE_ROOT = File("./../l2j_datapack/dist/game/data/scripts")

        CommonConfig.load()
        Config.load()

        ScriptEngineManager.getInstance()

        GameTimeController.init()
        InstanceManager.getInstance()
        L2World.getInstance()
        MapRegionManager.getInstance()
        AnnouncementsTable.getInstance()
        GlobalVariablesManager.getInstance()

        CategoryData.getInstance()
        SecondaryAuthData.getInstance()

        EffectHandler.getInstance().executeScript()
        EnchantSkillGroupsData.getInstance()
        SkillTreesData.getInstance()

        SkillData.getInstance()
        SummonSkillsTable.getInstance()

        val depriveTemplate = NpcData.getInstance().getTemplate(depriveNpcId)
        val depriveNpc = L2MonsterInstance(depriveTemplate)

        Assertions.assertThat(depriveNpc.stat.getPDef(null)).isEqualTo(252.0)
    }
}